<?php

// [Task#55]

class PlenarySessionsHelper {

    public static function removeRowsWithCurrentDate($content) {
        $content_without_info_about_current_date = '';
        $current_date = date('m/d/Y', time());

        // remove first row-header with name of columns
        $pos = strpos($content, "\n");
        $content = substr($content, $pos + 1);

        while (true) {
            $date_agenda = PlenarySessionsHelper::getFirstDateAgendaFromContent($content);
            if ( strcmp($date_agenda, $current_date) == 0 ) {
                $content = PlenarySessionsHelper::removeFirstRowFromContent($content);
            } else {
                $content_without_info_about_current_date = $content;
                break;
            }
        }

        return $content_without_info_about_current_date;
    }

    private static function getFirstDateAgendaFromContent($content) {
        $pos = strpos($content, ",");
        $date_agenda = substr($content, 0, $pos);
        return $date_agenda;
    }

    private static function removeFirstRowFromContent($content) {
        $pos = strpos($content, "\n");
        $content_without_first_row = substr($content, $pos + 1);

        return $content_without_first_row;
    }

}