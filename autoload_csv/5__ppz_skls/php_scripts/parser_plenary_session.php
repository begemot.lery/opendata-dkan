<?php

require_once './php_scripts/utils/curl_helper.php';
require_once './php_scripts/utils/plenary_session_parse_helper.php';

class PlenarySessionParser {

	const PATH_CSV_DIR = './parsed_csv';

	private $convocation_number;
	private $api_url;
	private $curlHelper;
	private $xml_obj;

	private $csv_file__event_question;
	private $csv_file__result_event;
	private $csv_file__result_by_name;

	public function __construct( $convocation_number, $api_url ) {
		$this->convocation_number = $convocation_number;
		$this->api_url            = $api_url;
		$this->curlHelper         = new CurlHelper();

		$PATH_CSV_FILE__EVENT_QUESTION = self::PATH_CSV_DIR . '/' . $convocation_number . '/' . 'event_question_' . $convocation_number . '.csv';
		$PATH_CSV_FILE__RESULT_EVENT   = self::PATH_CSV_DIR . '/' . $convocation_number . '/' . 'result_event_'   . $convocation_number . '.csv';
		$PATH_CSV_FILE__RESULT_BY_NAME = self::PATH_CSV_DIR . '/' . $convocation_number . '/' . 'result_by_name_' . $convocation_number . '.csv';
		
		
		$this->csv_file__event_question = fopen( $PATH_CSV_FILE__EVENT_QUESTION, 'a' );
		$this->csv_file__result_event   = fopen( $PATH_CSV_FILE__RESULT_EVENT,   'a' );
		$this->csv_file__result_by_name = fopen( $PATH_CSV_FILE__RESULT_BY_NAME, 'a' );
	
	}

	public function __destruct() {
		fclose ( $this->csv_file__event_question );
		fclose ( $this->csv_file__result_event   );
		fclose ( $this->csv_file__result_by_name );
	}

	public function parse() {
		$xml_content    = $this->curlHelper->getXmlContent( $this->api_url );
		$this->xml_obj  = simplexml_load_string( $xml_content );

		$parseHelper = new PlenarySessionParse_Helper( $this->xml_obj,
							       $this->csv_file__event_question,
							       $this->csv_file__result_event,
							       $this->csv_file__result_by_name );
 
		$parseHelper->parseXmlToCsv();
	}
}

?>