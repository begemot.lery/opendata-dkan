<?php

require_once './php_scripts/parser_plenary_session.php';

$CONVOCATION_NUMBER = $argv[1];
$API_URL            = $argv[2];

$plenaryParser = new PlenarySessionParser( $CONVOCATION_NUMBER, $API_URL );
set_time_limit(10);
$plenaryParser->parse();

?>