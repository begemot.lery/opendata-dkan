<?php

require_once './php_scripts/directories.php';

$CONVOCATION_NUMBER     = $argv[1];
$API_URL_MPS            = $argv[2];
$API_URL_FRACTIONS      = $argv[3];
$API_URL_COMMENTS       = $argv[4];
$API_URL_AGENDA         = $argv[5];

$DirectoriesParser = new Directories(   $CONVOCATION_NUMBER, 
                                        $API_URL_MPS , 
                                        $API_URL_FRACTIONS , 
                                        $API_URL_AGENDA, 
                                        $API_URL_COMMENTS );
$DirectoriesParser->parse(); 


?>