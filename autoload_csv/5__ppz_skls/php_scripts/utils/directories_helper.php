<?php

class DirectoriesHelper {

    private $csv_file__list_of_issues_agenda;
    private $csv_file__comments_chronology;
    private $csv_file__directory_fractions_chronology;
    private $csv_file__directory_deputies_chronology;
    
    private $xml_obj_agenda;
    private $xml_obj_fractions;
    private $xml_obj_comments;
    private $xml_obj_mps;
    
    public function __construct( &$xml_obj_agenda,
                                 &$xml_obj_fractions,
                                 &$xml_obj_comments,
                                 &$xml_obj_mps,
                                 &$csv_file__list_of_issues_agenda,
                                 &$csv_file__comments_chronology,
                                 &$csv_file__directory_fractions_chronology,
                                 &$csv_file__directory_deputies_chronology)
    {
        $this->xml_obj_agenda                           = $xml_obj_agenda;
        $this->xml_obj_fractions                        = $xml_obj_fractions;
        $this->xml_obj_comments                         = $xml_obj_comments;
        $this->xml_obj_mps                              = $xml_obj_mps;
        $this->csv_file__list_of_issues_agenda          = $csv_file__list_of_issues_agenda;
        $this->csv_file__comments_chronology            = $csv_file__comments_chronology;
        $this->csv_file__directory_fractions_chronology = $csv_file__directory_fractions_chronology;
        $this->csv_file__directory_deputies_chronology  = $csv_file__directory_deputies_chronology;
    }

    public function parseAgendaXmlToCsv() {
        foreach ($this->xml_obj_agenda->agenda as $agenda){
            foreach($agenda->question as $question ){
                $id = $question->attributes()['id_question'];
                fputcsv( $this->csv_file__list_of_issues_agenda, [$agenda->date_agenda->day_agenda."/".$agenda->date_agenda->month_agenda."/".$agenda->date_agenda->year_agenda,$id, $question->number_question, $question->init_question,$question->name_question], ',', '"' );

            }
        }
    }
    public function parseMpsXmlToCsv() {
        foreach ($this->xml_obj_mps->mp as $mp){
            $id = $mp->attributes()['id_mp'];

            fputcsv( $this->csv_file__directory_deputies_chronology, [$id,$mp->sex,$mp->name], ',', '"' );

        }
    }
    public function parseCommentsXmlToCsv() {
        foreach ($this->xml_obj_comments->komment as $komment){
            $id = $komment->attributes()['id'];

            fputcsv( $this->csv_file__comments_chronology, [$id, $komment->komm], ',', '"' );
        }

    }
    public function parseFractionXmlToCsv() {
        foreach ($this->xml_obj_fractions->faction as $faction){
            $id = $faction->attributes()['id_faction'];

            fputcsv( $this->csv_file__directory_fractions_chronology, [$id,$faction->type,$faction->name], ',', '"' );

        }
    }



}

