<?php

class PlenarySessionParse_Helper {

	private $xml_obj;

	private $csv_file__event_question;
	private $csv_file__result_event;
	private $csv_file__result_by_name;
	
	public function __construct( &$xml_obj,
								 &$csv_file__event_question,
							     &$csv_file__result_event,
								 &$csv_file__result_by_name ) 
	{
		$this->xml_obj = $xml_obj;
		$this->csv_file__event_question = $csv_file__event_question;
		$this->csv_file__result_event   = $csv_file__result_event;							 
		$this->csv_file__result_by_name = $csv_file__result_by_name;							 
	}
	
	public function parseXmlToCsv() {
		// 1. date_agenda
		$date_agenda = $this->getDateAgenda();

		foreach ( $this->xml_obj->question as $question ) {
			
			// 2. id_question
			$id_question = $question->attributes()->id_question;
			$id_question = "'$id_question'";
			
			// 3. date_question
			$date_question = $this->getDateQuestion($question->date_question);
			
			foreach( $question->event_question as $event_question ) {
				// 4. date_event
				$date_event = $this->getDateEvent($event_question->date_event);

				// 5. name_event
				$name_event = $event_question->name_event;
								
				$result_event = $event_question->result_event;
				
				// 6. id_event
				$id_event = $result_event->id_event;
				
				if ( $id_event ) {					
					// 7. for
					$for = $result_event->for;
					
					// 8. against
					$against = $result_event->against;

					// 9. abstain
					$abstain = $result_event->abstain;

					// 10. not_voting
					$not_voting = $result_event->not_voting;

					// 11. total
					$total = $result_event->total;

					// 12. presence
					$presence = $result_event->presence;
					
					// 13. absent
					$absent = $result_event->absent;

					// CSV_FILE__RESULT_EVENT
					$arr_result_event = [$date_agenda, $id_question, $id_event, $for, $against, $abstain, $not_voting, $total, $presence, $absent];				
					fputcsv( $this->csv_file__result_event, $arr_result_event, ',', '"' );

					$result_by_name = $result_event->result_by_name;					
					foreach ( $result_by_name->mp as $mp ) {
						// 14. id_mp
						$id_mp = $mp->attributes()->id_mp;
						
						// 15. faction
						$faction = $mp->faction;
						
						// 16. result
						$result = $mp->result;
						
						// 17. komment
						$komment = $mp->komment;					

						// CSV_FILE__RESULT_BY_NAME
						$arr_result_by_name = [$date_agenda, $id_question, $id_event, $id_mp, $faction, $result, $komment];				
						fputcsv( $this->csv_file__result_by_name, $arr_result_by_name, ',', '"' );
					}
				}
				
				// CSV_FILE__EVENT_QUESTION
				$arr_event_question = [$date_agenda, $id_question, $date_question, $date_event, $name_event, $id_event];				
				fputcsv( $this->csv_file__event_question, $arr_event_question, ',', '"' );
			}				
		}
	}
	
	private function getDateAgenda() {
		$day_agenda   = $this->xml_obj->date_agenda->day_agenda;
		$month_agenda = $this->xml_obj->date_agenda->month_agenda;
		$year_agenda  = $this->xml_obj->date_agenda->year_agenda;
		
		return "$day_agenda/$month_agenda/$year_agenda";		
	}
	
	private function getDateQuestion( $date_question ) {
		$day   = $date_question->day_question;
		$month = $date_question->month_question;
		$year  = $date_question->year_question;		

		$hour = $date_question->hour_question;
		$min  = $date_question->min_question;
		$sec  = $date_question->sec_question;
		
		return "$day/$month/$year $hour:$min:$sec";
	}

	private function getDateEvent( $date_event ) {
		$day   = $date_event->day_event;
		$month = $date_event->month_event;
		$year  = $date_event->year_event;
		
		$hour = $date_event->hour_event;
		$min  = $date_event->min_event;
		$sec  = $date_event->sec_event;
		
		return "$day/$month/$year $hour:$min:$sec";
	}

}

?>