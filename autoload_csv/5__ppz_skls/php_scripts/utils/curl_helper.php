<?php

class CurlHelper {	

	public function getXmlContent( $api_url ) {		
		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_URL, $api_url );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		$xmlContent = curl_exec($curl);
		curl_close( $curl );

		return $xmlContent;
	}
}

?>