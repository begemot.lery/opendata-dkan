<?php

require_once './php_scripts/utils/curl_helper.php';
require_once './php_scripts/utils/directories_helper.php';


class Directories{

    const PATH_CSV_DIR = './parsed_csv';

    private $convocation_number;
    private $curlHelper;
    
    private $api_url_mps;
    private $api_url_fractions;
    private $api_url_agenda;
    private $api_url_comments;
    
    
    private $xml_obj_agenda;
    private $xml_obj_fractions;
    private $xml_obj_comments;
    private $xml_obj_mps;

    private $csv_file__list_of_issues_agenda;
    private $csv_file__comments_chronology;
    private $csv_file__directory_fractions_chronology;
    private $csv_file__directory_deputies_chronology;



    public function __construct( $convocation_number,
                                 $api_url_mps,
                                 $api_url_fractions,
                                 $api_url_agenda,
                                 $api_url_comments ) {

        $this->convocation_number = $convocation_number;

        $this->api_url_mps        = $api_url_mps;
        $this->api_url_fractions  = $api_url_fractions;
        $this->api_url_agenda     = $api_url_agenda;
        $this->api_url_comments   = $api_url_comments;

        $this->curlHelper         = new CurlHelper();
        //Формирование пути для записи 
        $PATH_CSV_FILE__LIST_OF_ISSUES_AGENDA           = self::PATH_CSV_DIR . '/' . $convocation_number . '/' . 'list_of_issues_agenda__' . $convocation_number . '.csv';
        $PATH_CSV_FILE__COMMENTS_CHRONOLOGY             = self::PATH_CSV_DIR . '/' . $convocation_number . '/' . 'comments_chronology_'	. $convocation_number . '.csv';
        $PATH_CSV_FILE__DIRECTORY_FRACTIONS_CHRONOLOGY  = self::PATH_CSV_DIR . '/' . $convocation_number . '/' . 'directory_fractions_chronology_' . $convocation_number . '.csv';
        $PATH_CSV_FILE__DIRECTORY_DEPUTIES_CHRONOLOGY   = self::PATH_CSV_DIR . '/' . $convocation_number . '/' . 'directory_deputies_chronology_'	. $convocation_number . '.csv';
        //Открытие файлов для записи
        $this->csv_file__list_of_issues_agenda          = fopen( $PATH_CSV_FILE__LIST_OF_ISSUES_AGENDA, 'a'         );
        $this->csv_file__comments_chronology            = fopen( $PATH_CSV_FILE__COMMENTS_CHRONOLOGY, 'a'           );
        $this->csv_file__directory_fractions_chronology = fopen( $PATH_CSV_FILE__DIRECTORY_FRACTIONS_CHRONOLOGY, 'a');
        $this->csv_file__directory_deputies_chronology  = fopen( $PATH_CSV_FILE__DIRECTORY_DEPUTIES_CHRONOLOGY, 'a' );
    }

    public function __destruct() {

        fclose ( $this->csv_file__list_of_issues_agenda 	        );
        fclose ( $this->csv_file__comments_chronology 	            );
        fclose ( $this->csv_file__directory_fractions_chronology 	);
        fclose ( $this->csv_file__directory_deputies_chronology 	);
    }

    public function parse() {

        //Загрузка XML контента по API

        $this->xmlLoader(   $this->api_url_agenda,
                            $this->api_url_comments,
                            $this->api_url_fractions,
                            $this->api_url_mps);

        //Инициализация об'єкта с данными справочников

        $parseDirectoriesHelper = new DirectoriesHelper(    $this->xml_obj_agenda,
                                                            $this->xml_obj_fractions,
                                                            $this->xml_obj_comments,
                                                            $this->xml_obj_mps, 
                                                            $this->csv_file__list_of_issues_agenda,
                                                            $this->csv_file__comments_chronology,
                                                            $this->csv_file__directory_fractions_chronology,
                                                            $this->csv_file__directory_deputies_chronology);

        // Парсинг каждого справочника по отдельности

        $parseDirectoriesHelper->parseAgendaXmlToCsv();
        $parseDirectoriesHelper->parseCommentsXmlToCsv();
        $parseDirectoriesHelper->parseMpsXmlToCsv();
        $parseDirectoriesHelper->parseFractionXmlToCsv();
        
    }

    //Загрузка XML контента по API

    public function xmlLoader($api_url_agenda,$api_url_comments,$api_url_fractions,$api_url_mps){
        $xml_content_agenda     = $this->curlHelper->getXmlContent( $api_url_agenda     );
        $xml_content_comments   = $this->curlHelper->getXmlContent( $api_url_comments   );
        $xml_content_fractions  = $this->curlHelper->getXmlContent( $api_url_fractions  );
        $xml_content_mps        = $this->curlHelper->getXmlContent( $api_url_mps        );
        $this->xml_obj_agenda       = simplexml_load_string( $xml_content_agenda        );
        $this->xml_obj_comments     = simplexml_load_string( $xml_content_comments      );
        $this->xml_obj_fractions    = simplexml_load_string( $xml_content_fractions     );
        $this->xml_obj_mps          = simplexml_load_string( $xml_content_mps           );

    }
}

?>