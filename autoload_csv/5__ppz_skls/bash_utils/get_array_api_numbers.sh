#!/bin/bash

FILE_DATES=$1

function date_to_number() {
    DATE="$1"
    day=${DATE:0:2}
    month=${DATE:3:2}
    year=${DATE:6}
    echo "$day$month$year"
}

declare -a array_api_numbers

array_dates=( $(cat $FILE_DATES) )

index=0
for date in ${array_dates[@]}
do
    number=$(date_to_number $date)
    array_api_numbers[$index]=$number
    ((index++))
done

echo ${array_api_numbers[@]}
