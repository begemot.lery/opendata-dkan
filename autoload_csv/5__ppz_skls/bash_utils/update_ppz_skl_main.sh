#!/bin/bash

CONVOCATION_NUMBER=$1

DATASTORE_RESOURCE_ID__EVENT_QUESTION=$2
DATASTORE_RESOURCE_ID__RESULT_EVENT=$3
DATASTORE_RESOURCE_ID__RESULT_BY_NAME=$4
DATASTORE_RESOURCE_ID__DIRECTORY_FRACTIONS_CHRONOLOGY_8=$5
DATASTORE_RESOURCE_ID__COMMENTS_CHRONOLOGY_8=$6
DATASTORE_RESOURCE_ID__LIST_OF_ISSUES_AGENDA_8=$7
DATASTORE_RESOURCE_ID__DIRECTORY_DEPUTIES_CHRONOLOGY_8=$8

path_csv_dir="./parsed_csv/$CONVOCATION_NUMBER"
csv_file__event_question="$path_csv_dir/event_question_$CONVOCATION_NUMBER.csv"
csv_file__result_event="$path_csv_dir/result_event_$CONVOCATION_NUMBER.csv"
csv_file__result_by_name="$path_csv_dir/result_by_name_$CONVOCATION_NUMBER.csv"
csv_file__directory_fractions_chronology="$path_csv_dir/directory_fractions_chronology_$CONVOCATION_NUMBER.csv"
csv_file__comments_chronology="$path_csv_dir/comments_chronology_$CONVOCATION_NUMBER.csv"
csv_file__list_of_issues_agenda="$path_csv_dir/list_of_issues_agenda__$CONVOCATION_NUMBER.csv"
csv_file__directory_deputies_chronology="$path_csv_dir/directory_deputies_chronology_$CONVOCATION_NUMBER.csv"

./bash_utils/init_csv_files.sh \
                                $csv_file__event_question \
                                $csv_file__result_event \
                                $csv_file__result_by_name \
                                $csv_file__directory_fractions_chronology \
                                $csv_file__comments_chronology \
                                $csv_file__list_of_issues_agenda \
                                $csv_file__directory_deputies_chronology


./bash_utils/download_dates_ppz_skl.sh $CONVOCATION_NUMBER

file_dates="./dates_ppz_skl/dates_ppz_skl_$CONVOCATION_NUMBER.txt"
array_api_numbers=( $(./bash_utils/get_array_api_numbers.sh $file_dates) )

lenght_array_api_numbers=${#array_api_numbers[*]}

time=$(date +%s)
position_current_api_number=0
for api_number in ${array_api_numbers[*]}
do
	((position_current_api_number++))
	api_url="http://data.rada.gov.ua/ogd/zal/ppz/skl$CONVOCATION_NUMBER/xml/$api_number.xml"
	echo
	echo "[$position_current_api_number from $lenght_array_api_numbers]"
	echo "DOWNLOAD & PARSING: $api_url"
	php ./php_scripts/parser_plenary_session_main.php $CONVOCATION_NUMBER $api_url
	echo $(($(date +%s)-$time))
done
path_csv_dir="./autoload_csv/5__ppz_skls/parsed_csv/$CONVOCATION_NUMBER"
csv_file__event_question="$path_csv_dir/event_question_$CONVOCATION_NUMBER.csv"
csv_file__result_event="$path_csv_dir/result_event_$CONVOCATION_NUMBER.csv"
csv_file__result_by_name="$path_csv_dir/result_by_name_$CONVOCATION_NUMBER.csv"
csv_file__directory_fractions_chronology="$path_csv_dir/directory_fractions_chronology_$CONVOCATION_NUMBER.csv"
csv_file__comments_chronology="$path_csv_dir/comments_chronology_$CONVOCATION_NUMBER.csv"
csv_file__list_of_issues_agenda="$path_csv_dir/list_of_issues_agenda__$CONVOCATION_NUMBER.csv"
csv_file__directory_deputies_chronology="$path_csv_dir/directory_deputies_chronology_$CONVOCATION_NUMBER.csv"

api_url_mps="http://data.rada.gov.ua/ogd/zal/ppz/skl$CONVOCATION_NUMBER/dict/mps.xml"
api_url_fraction="http://data.rada.gov.ua/ogd/zal/ppz/skl$CONVOCATION_NUMBER/dict/factions.xml"
api_url_comments="http://data.rada.gov.ua/ogd/zal/ppz/skl$CONVOCATION_NUMBER/dict/komments.xml"
api_url_agendas="http://data.rada.gov.ua/ogd/zal/ppz/skl$CONVOCATION_NUMBER/dict/agendas_8_skl.xml"
	echo
	echo "[$position_current_api_number from $lenght_array_api_numbers]"
	echo "DOWNLOAD & PARSING: directories"
	php ./php_scripts/directories_main.php $CONVOCATION_NUMBER $api_url_mps $api_url_fraction $api_url_comments $api_url_agendas

/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__EVENT_QUESTION $csv_file__event_question
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__RESULT_EVENT   $csv_file__result_event
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__RESULT_BY_NAME $csv_file__result_by_name
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__DIRECTORY_FRACTIONS_CHRONOLOGY_8 $csv_file__directory_fractions_chronology
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__COMMENTS_CHRONOLOGY_8 $csv_file__comments_chronology
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__LIST_OF_ISSUES_AGENDA_8 $csv_file__list_of_issues_agenda
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__DIRECTORY_DEPUTIES_CHRONOLOGY_8 $csv_file__directory_deputies_chronology
