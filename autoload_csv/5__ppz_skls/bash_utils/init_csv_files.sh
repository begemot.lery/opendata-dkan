#!/bin/bash

CSV_FILE__EVENT_QUESTION=$1
CSV_FILE__RESULT_EVENT=$2
CSV_FILE__RESULT_BY_NAME=$3
CSV_FILE__DIRECTORY_FRACTIONS_CHRONOLOGY=$4
CSV_FILE__COMMENTS_CHRONOLOGY=$5
CSV_FILE__LIST_OF_ISSUES_AGENDA=$6
CSV_FILE__DIRECTORY_DEPUTIES_CHRONOLOGY=$7


echo "date_agenda, id_question, date_question, date_event, name_event, id_event" > $CSV_FILE__EVENT_QUESTION
echo "date_agenda, id_question, id_event, for, against, abstain, not_voting, total, presence, absent" > $CSV_FILE__RESULT_EVENT
echo "date_agenda, id_question, id_event, id_mp, fraction, result, comment" > $CSV_FILE__RESULT_BY_NAME
echo "id_fraction, type, name" > $CSV_FILE__DIRECTORY_FRACTIONS_CHRONOLOGY
echo "id, comment" > $CSV_FILE__COMMENTS_CHRONOLOGY
echo "date_agenda, id_question, number_question, init_question, question" > $CSV_FILE__LIST_OF_ISSUES_AGENDA
echo "id_mp, sex, name" > $CSV_FILE__DIRECTORY_DEPUTIES_CHRONOLOGY

