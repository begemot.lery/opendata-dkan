#!/bin/bash

cd /var/www/dkan/public_html/autoload_csv/5__ppz_skls

CONVOCATION_NUMBER_7=7

DATASTORE_RESOURCE_ID__EVENT_QUESTION_7=b47bed47-4021-453a-87b8-2b444e4b52d2
DATASTORE_RESOURCE_ID__RESULT_EVENT_7=9b7adeba-57a5-4da3-9b20-6aeb3e5488e9
DATASTORE_RESOURCE_ID__RESULT_BY_NAME_7=4c5aba3e-91d8-4b48-8d6b-c49aa193e2b2

./bash_utils/update_ppz_skl_main.sh \
							$CONVOCATION_NUMBER_7 \
							$DATASTORE_RESOURCE_ID__EVENT_QUESTION_7 \
							$DATASTORE_RESOURCE_ID__RESULT_EVENT_7 \
							$DATASTORE_RESOURCE_ID__RESULT_BY_NAME_7
