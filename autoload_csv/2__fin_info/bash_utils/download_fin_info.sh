#!/bin/bash

DATASTORE_RESOURCE_ID__JAN=$1
DATASTORE_RESOURCE_ID__FEB=$2
DATASTORE_RESOURCE_ID__MAR=$3
DATASTORE_RESOURCE_ID__APR=$4
DATASTORE_RESOURCE_ID__MAY=$5
DATASTORE_RESOURCE_ID__JUN=$6
DATASTORE_RESOURCE_ID__JUL=$7
DATASTORE_RESOURCE_ID__AUG=$8
DATASTORE_RESOURCE_ID__SEP=$9
DATASTORE_RESOURCE_ID__OCT=$10

path_csv_dir="./parsed_csv"

curl "http://data.rada.gov.ua/ogd/fin/komp/meta.xml" -o "./downloaded_xml/compensation_list.xml"
php ./php_scripts/parser_compensation_list_main.php

/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__JAN                         "$path_csv_dir/komp_mps_2016-01.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__FEB                         "$path_csv_dir/komp_mps_2016-02.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__MAR                         "$path_csv_dir/komp_mps_2016-03.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__APR                         "$path_csv_dir/komp_mps_2016-04.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__MAY                         "$path_csv_dir/komp_mps_2016-05.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__JUN                         "$path_csv_dir/komp_mps_2016-06.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__JUL                         "$path_csv_dir/komp_mps_2016-07.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__AUG                         "$path_csv_dir/komp_mps_2016-08.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__SEP                         "$path_csv_dir/komp_mps_2016-09.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__OCT                         "$path_csv_dir/komp_mps_2016-10.csv"
