<?php

class CurlHelper {
	const USER_AGENT = "OpenData";

	public function getCSVContent( $api_url ) {
		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_USERAGENT, self::USER_AGENT );
		curl_setopt( $curl, CURLOPT_URL, $api_url );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		$htmlContent = curl_exec( $curl );
		curl_close( $curl );

		return $htmlContent;
	}
}

?>