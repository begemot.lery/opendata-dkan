<?php
require_once './php_scripts/utils/curl_helper.php';

class ParserCompensationList
{
    const DIR_CSV = './parsed_csv';
    const DIR_XML = './downloaded_xml';

    private $xml;

    private $csv_file;
    private $curlHelper;


    public function __construct()
    {
        $xml_file = self::DIR_XML . '/' . 'compensation_list.xml';

        if (file_exists($xml_file)) {
            $this->xml = simplexml_load_file($xml_file);
            $this->curlHelper = new CurlHelper();

        }
    }

    public function __destruct()
    {

    }

    //Вытягивает из xml список нужных для скачивания имен файлов
    public function parseXmlToCsv()
    {
        $xml = $this->xml;

        foreach ($xml->meta->item as $type) {
            if($type->name == 'komp_mps_list') {
                self::downloadXLS('http://data.rada.gov.ua' . $type->path . $type->name . '.xls', $type->name);
            }else{
                self::downloadCSV('http://data.rada.gov.ua' . $type->path . $type->name . '.csv', $type->name);
            }

        }

    }
    
    //Скачивает файлы,перекодирует в utf-8 и сохраняет их в csv
    public function downloadCSV($url, $name)
    {
        $csv = file_get_contents($url);
        $csv = iconv("WINDOWS-1251", "UTF-8", $csv);
        
        file_put_contents(self::DIR_CSV . '/' . $name . '.csv', $csv);
    }
    //Скачивает файл и сохраняет их в xls
    public function downloadXLS($url, $name)
    {
        $xls = file_get_contents($url);
        file_put_contents(self::DIR_CSV . '/' . $name . '.xls', $xls);
    }


}

?>