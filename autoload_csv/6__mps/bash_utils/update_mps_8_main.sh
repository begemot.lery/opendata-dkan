#!/bin/bash

ARGV=( $@ )

CONVOCATION_NUMBER=${ARGV[0]}

DATASTORE_RESOURCE_ID___ASSISTENT_TYPES=${ARGV[1]}
DATASTORE_RESOURCE_ID___DECL_ROW_NAMES=${ARGV[2]}
DATASTORE_RESOURCE_ID___FR_ASSOCIATIONS=${ARGV[3]}
DATASTORE_RESOURCE_ID___FR_POSTS=${ARGV[4]}
DATASTORE_RESOURCE_ID___MP_POSTS=${ARGV[5]}
DATASTORE_RESOURCE_ID___MPS__ASSISTANTS=${ARGV[6]}
DATASTORE_RESOURCE_ID___MPS__DECLARATION__DEPOSITS=${ARGV[7]}
DATASTORE_RESOURCE_ID___MPS__DECLARATION__FAMILY_MEMBERS=${ARGV[8]}
DATASTORE_RESOURCE_ID___MPS__DECLARATION__FINANCIAL_LIABILITIES=${ARGV[9]}
DATASTORE_RESOURCE_ID___MPS__DECLARATION__INCOMES=${ARGV[10]}
DATASTORE_RESOURCE_ID___MPS__DECLARATION__INCOMES_ABROAD=${ARGV[11]}
DATASTORE_RESOURCE_ID___MPS__DECLARATION__REALTIES=${ARGV[12]}
DATASTORE_RESOURCE_ID___MPS__DECLARATION__TRANSPORTS=${ARGV[13]}
DATASTORE_RESOURCE_ID___MPS__POST_BFDAS=${ARGV[14]}
DATASTORE_RESOURCE_ID___MPS__POST_FRS=${ARGV[15]}
DATASTORE_RESOURCE_ID___MPS__POST_MPS=${ARGV[16]}
DATASTORE_RESOURCE_ID___MPS__RESIGNATION=${ARGV[17]}
DATASTORE_RESOURCE_ID___MPS=${ARGV[18]}
DATASTORE_RESOURCE_ID___PARTIES=${ARGV[19]}
DATASTORE_RESOURCE_ID___REGIONS=${ARGV[20]}


curl "http://data.rada.gov.ua/ogd/mps/skl8/mps-data.xml" -o "./downloaded_xml/mps_$CONVOCATION_NUMBER.xml"
cp "./downloaded_xml/mps_$CONVOCATION_NUMBER.xml" "../../sites/default/files/links/mps/mps_$CONVOCATION_NUMBER.xml"
curl "http://data.rada.gov.ua/ogd/mps/skl8/mps-data.json" -o "../../sites/default/files/links/mps/mps_$CONVOCATION_NUMBER.json"


php ./php_scripts/parser_mps_8_main.php $CONVOCATION_NUMBER


path_csv_dir="./autoload_csv/6__mps/parsed_csv/$CONVOCATION_NUMBER"

/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___ASSISTENT_TYPES                         "$path_csv_dir/assistant_types_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___DECL_ROW_NAMES                          "$path_csv_dir/decl_row_names_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___FR_ASSOCIATIONS                         "$path_csv_dir/fr_associations_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___FR_POSTS                                "$path_csv_dir/fr_posts_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MP_POSTS                                "$path_csv_dir/mp_posts_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__ASSISTANTS                         "$path_csv_dir/mps__assistants_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__DECLARATION__DEPOSITS              "$path_csv_dir/mps__declaration__deposits_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__DECLARATION__FAMILY_MEMBERS        "$path_csv_dir/mps__declaration__family_members_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__DECLARATION__FINANCIAL_LIABILITIES "$path_csv_dir/mps__declaration__financial_liabilities_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__DECLARATION__INCOMES               "$path_csv_dir/mps__declaration__incomes_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__DECLARATION__INCOMES_ABROAD        "$path_csv_dir/mps__declaration__incomes_abroad_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__DECLARATION__REALTIES              "$path_csv_dir/mps__declaration__realties_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__DECLARATION__TRANSPORTS            "$path_csv_dir/mps_declaration_transports_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__POST_BFDAS                         "$path_csv_dir/mps__post_bfdas_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__POST_FRS                           "$path_csv_dir/mps__post_frs_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__POST_MPS                           "$path_csv_dir/mps__post_mps_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__RESIGNATION                        "$path_csv_dir/mps__resignation_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS                                     "$path_csv_dir/mps_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___PARTIES                                 "$path_csv_dir/parties_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___REGIONS                                 "$path_csv_dir/regions_$CONVOCATION_NUMBER.csv"