#!/bin/bash

CONVOCATION_NUMBER=$1

DATASTORE_RESOURCE_ID___MPS=$2
DATASTORE_RESOURCE_ID___MPS__POSTS=$3
DATASTORE_RESOURCE_ID___MPS__POSTS_MP=$4

curl "http://itdl.rada.gov.ua/apps_ad/opendata/Temp_files/exmps$CONVOCATION_NUMBER""_xml.xml" -o "./downloaded_xml/mps_$CONVOCATION_NUMBER.xml"

cp "./downloaded_xml/mps_$CONVOCATION_NUMBER.xml" "../../sites/default/files/links/mps/mps_$CONVOCATION_NUMBER.xml"
curl "http://itdl.rada.gov.ua/apps_ad/opendata/Temp_files/exmps$CONVOCATION_NUMBER""_json.json" -o "../../sites/default/files/links/mps/mps_$CONVOCATION_NUMBER.json"


php ./php_scripts/parser_mps_4-7_main.php $CONVOCATION_NUMBER


path_dir_csv="./autoload_csv/6__mps/parsed_csv/$CONVOCATION_NUMBER"

/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS           "$path_dir_csv/mps_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__POSTS    "$path_dir_csv/mps__posts_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___MPS__POSTS_MP "$path_dir_csv/mps__posts_mp_$CONVOCATION_NUMBER.csv"

