#!/bin/bash

cd /var/www/dkan/public_html/autoload_csv/6__mps

CONVOCATION_NUMBER_7=7

DATASTORE_RESOURCE_ID___MPS_7=3e0a32e9-8d05-470b-9499-f4dc69b57e1f
DATASTORE_RESOURCE_ID___MPS__POSTS_7=8411ea7d-cb65-458b-9605-e361fc282441
DATASTORE_RESOURCE_ID___MPS__POSTS_MP_7=45b09942-dc08-4edf-b47c-31a06cebba43


./bash_utils/update_mps_4-7_main.sh \
							$CONVOCATION_NUMBER_7 \
							$DATASTORE_RESOURCE_ID___MPS_7 \
							$DATASTORE_RESOURCE_ID___MPS__POSTS_7 \
							$DATASTORE_RESOURCE_ID___MPS__POSTS_MP_7
