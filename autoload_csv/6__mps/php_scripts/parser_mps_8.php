<?php

$convocation_number = $argv[1];

class ParserMps8 {
	const DIR_CSV = './parsed_csv';
	const DIR_XML = './downloaded_xml';

	private $xml;
	
	private $csv_file_assistant_types;
	private $csv_file_decl_row_names;
	private $csv_file_fr_associations;
	private $csv_file_fr_posts;
	private $csv_file_mp_posts;
	private $csv_file_mps;
	private $csv_file_mps__assistants;
	private $csv_file_mps__declaration;
	private $csv_file_mps__declaration__family_members;
	private $csv_file_mps__declaration__financial_liabilities;
	private $csv_file_mps__declaration__incomes;
	private $csv_file_mps__declaration__incomes_abroad;
	private $csv_file_mps__declaration__realties;
	private $csv_file_mps__declaration__transports;
	private $csv_file_mps__post_bfdas;
	private $csv_file_mps__post_frs;
	private $csv_file_mps__post_mps;
	private $csv_file_mps__resignation;
	private $csv_file_parties;
	private $csv_file_regions;
	
	public function __construct( $convocation_number ) {		
		$xml_file = self::DIR_XML . '/' . 'mps_' . $convocation_number . '.xml';

		if ( file_exists( $xml_file ) ) {
			$this->xml = simplexml_load_file( $xml_file );			
						
			$this->csv_file_assistant_types = $this->_fopen( 'assistant_types_', $convocation_number );
			fputcsv( $this->csv_file_assistant_types, ['id', 'name'], ',', '"' );
			
			$this->csv_file_decl_row_names = $this->_fopen( 'decl_row_names_', $convocation_number );
			fputcsv( $this->csv_file_decl_row_names, ['id', 'row_name', 'row_num'], ',', '"' );
			
			$this->csv_file_fr_associations = $this->_fopen( 'fr_associations_', $convocation_number );
			fputcsv( $this->csv_file_fr_associations, ['id', 'is_fr', 'name', 'type'], ',', '"' );
			
			$this->csv_file_fr_posts = $this->_fopen( 'fr_posts_', $convocation_number );
			fputcsv( $this->csv_file_fr_posts, ['id', 'name'], ',', '"' );
			
			$this->csv_file_mp_posts = $this->_fopen( 'mp_posts_', $convocation_number );
			fputcsv( $this->csv_file_mp_posts, ['organization_id', 'post_id', 'post_name'], ',', '"' );
			
			$this->csv_file_mps = $this->_fopen( 'mps_', $convocation_number );
			fputcsv( $this->csv_file_mps, ['fullname', 'id', 'birthday', 'convocation', 'date_oath', 'district_num', 'emails', 'gender', 'num_in_party', 'party_id', 'phones', 'photo', 'photo_ext', 'presentAuto_absent', 'presentAuto_present', 'presentManualy_absent', 'presentManualy_confirmAbsent', 'presentManualy_present', 'rada_id', 'region_id', 'resignation_date', 'resignation_reason', 'short_info', 'socials'], ',', '"' );
			
			$this->csv_file_mps__assistants = $this->_fopen( 'mps__assistants_', $convocation_number );
			fputcsv( $this->csv_file_mps__assistants, ['mp_id', 'assistant_fullname', 'assistant_type_id'], ',', '"' );

			$this->csv_file_mps__declaration__deposits = $this->_fopen( 'mps__declaration__deposits_', $convocation_number );
			fputcsv( $this->csv_file_mps__declaration__deposits, ['mp_fullname', 'mp_id', 'post', 'declarate_date', 'year', 'decl_row_name_id', 'sum_abroad', 'sum_all'], ',', '"' );

			$this->csv_file_mps__declaration__family_members = $this->_fopen( 'mps__declaration__family_members_', $convocation_number );
			fputcsv( $this->csv_file_mps__declaration__family_members, ['mp_fullname', 'mp_id', 'post', 'declarate_date', 'year', 'decl_row_name_id', 'member_fullname', 'relationship'], ',', '"' );

			$this->csv_file_mps__declaration__financial_liabilities = $this->_fopen( 'mps__declaration__financial_liabilities_', $convocation_number );
			fputcsv( $this->csv_file_mps__declaration__financial_liabilities, ['mp_fullname', 'mp_id', 'post', 'declarate_date', 'year', 'decl_row_name_id', 'sum_abroad', 'sum_all'], ',', '"' );
			
			$this->csv_file_mps__declaration__incomes = $this->_fopen( 'mps__declaration__incomes_', $convocation_number );
			fputcsv( $this->csv_file_mps__declaration__incomes, ['mp_fullname', 'mp_id', 'post', 'declarate_date', 'year', 'decl_row_name_id', 'organizations', 'sum_declarant', 'sum_family'], ',', '"' );
			
			$this->csv_file_mps__declaration__incomes_abroad = $this->_fopen( 'mps__declaration__incomes_abroad_', $convocation_number );
			fputcsv( $this->csv_file_mps__declaration__incomes_abroad, ['mp_fullname', 'mp_id', 'post', 'declarate_date', 'year', 'country', 'decl_row_name_id', 'sum_abroad', 'sum_our'], ',', '"' );
		
			$this->csv_file_mps__declaration__realties = $this->_fopen( 'mps__declaration__realties_', $convocation_number );
			fputcsv( $this->csv_file_mps__declaration__realties, ['mp_fullname', 'mp_id', 'post', 'declarate_date', 'year', 'area', 'decl_row_name_id', 'sum_other', 'sum_own'], ',', '"' );
		
			$this->csv_file_mps__declaration__transports = $this->_fopen( 'mps_declaration_transports_', $convocation_number );
			fputcsv( $this->csv_file_mps__declaration__transports, ['mp_fullname', 'mp_id', 'post', 'declarate_date', 'year', 'decl_row_name_id', 'model', 'sum_other', 'sum_own', 'year_create'], ',', '"' );
		
			$this->csv_file_mps__post_bfdas = $this->_fopen( 'mps__post_bfdas_', $convocation_number );
			fputcsv( $this->csv_file_mps__post_bfdas, ['mp_id', 'association_name', 'post_name'], ',', '"' );
		
			$this->csv_file_mps__post_frs = $this->_fopen( 'mps__post_frs_', $convocation_number );
			fputcsv( $this->csv_file_mps__post_frs, ['mp_id', 'fr_association_id', 'fr_post_id'], ',', '"' );
		
			$this->csv_file_mps__post_mps = $this->_fopen( 'mps__post_mps_', $convocation_number );
			fputcsv( $this->csv_file_mps__post_mps, ['mp_id', 'organization_id', 'post_id'], ',', '"' );
		
			$this->csv_file_mps__resignation = $this->_fopen( 'mps__resignation_', $convocation_number );
			fputcsv( $this->csv_file_mps__resignation, ['mp_id', 'resignation_date', 'resignation_reason'], ',', '"' );
		
			$this->csv_file_parties = $this->_fopen( 'parties_', $convocation_number );
			fputcsv( $this->csv_file_parties, ['id', 'name'], ',', '"' );

			$this->csv_file_regions = $this->_fopen( 'regions_', $convocation_number );
			fputcsv( $this->csv_file_regions, ['id', 'name'], ',', '"' );			
		}
	}
	
	public function __destruct() {
		fclose( $this->csv_file_assistant_types                         );
		fclose( $this->csv_file_decl_row_names                          );
		fclose( $this->csv_file_fr_associations                         );
		fclose( $this->csv_file_fr_posts                                );
		fclose( $this->csv_file_mp_posts                                );
		fclose( $this->csv_file_mps                                     );
		fclose( $this->csv_file_mps__assistants                         );
		fclose( $this->csv_file_mps__declaration__deposits              );
		fclose( $this->csv_file_mps__declaration__family_members        );
		fclose( $this->csv_file_mps__declaration__financial_liabilities );
		fclose( $this->csv_file_mps__declaration__incomes               );
		fclose( $this->csv_file_mps__declaration__incomes_abroad        );
		fclose( $this->csv_file_mps__declaration__realties              );
		fclose( $this->csv_file_mps__declaration__transports            );
		fclose( $this->csv_file_mps__post_bfdas                         );
		fclose( $this->csv_file_mps__post_frs                           );
		fclose( $this->csv_file_mps__post_mps                           );
		fclose( $this->csv_file_mps__resignation                        );
		fclose( $this->csv_file_parties                                 );
		fclose( $this->csv_file_regions                                 );
	}
	
	public function parseXmlToCsv() {
		$xml = $this->xml;
		
		/* 1. ASSISTANT_TYPES */
		foreach ( $xml->assistant_types->type as $type ) {
			fputcsv( $this->csv_file_assistant_types, get_object_vars($type), ',', '"' );
		}		
		
		/* 2. DECL_ROW_NAMES */	
		foreach ( $xml->decl_row_names->row as $row ) {
			fputcsv( $this->csv_file_decl_row_names, get_object_vars($row), ',', '"' );
		}

		/* 3. FR_ASSOCIATIONS */	
		foreach ( $xml->fr_associations->association as $association ) {
			fputcsv( $this->csv_file_fr_associations, get_object_vars($association), ',', '"' );
		}
			
		/* 4. FR_POSTS */	
		foreach ( $xml->fr_posts->post as $post ) {
			fputcsv( $this->csv_file_fr_posts, get_object_vars($post), ',', '"' );
		}
			
		/* 5. MP_POSTS */
		foreach ( $xml->mp_posts->mp_post as $mp_post ) {
			fputcsv( $this->csv_file_mp_posts, get_object_vars($mp_post), ',', '"' );
		}
			
		/* 6. MPS */
		foreach ( $xml->mps->mp as $mp ) {
			$mp_fullname = $mp->surname . ' ' . $mp->firstname . ' ' . $mp->patronymic;
			$mp_id       = $mp->id;
			
			$array_mps[0]  = $mp_fullname;
			$array_mps[1]  = $mp_id;
			$array_mps[2]  = $mp->birthday;
			$array_mps[3]  = $mp->convocation;
			$array_mps[4]  = $mp->date_oath;
			$array_mps[5]  = $mp->district_num;
			$array_mps[6]  = $this->getMpEmails( $mp->emails );
			$array_mps[7]  = $mp->gender;
			$array_mps[8]  = $mp->num_in_party;
			$array_mps[9]  = $mp->party_id;
			$array_mps[10] = $this->getMpPhones( $mp->phones );
			$array_mps[11] = $mp->photo;
			$array_mps[12] = $mp->photo_ext;
			$array_mps[13] = $mp->presentAuto_absent;
			$array_mps[14] = $mp->presentAuto_present;
			$array_mps[15] = $mp->presentManualy_absent;
			$array_mps[16] = $mp->presentManualy_confirmAbsent;
			$array_mps[17] = $mp->presentManualy_present;
			$array_mps[18] = $mp->rada_id;
			$array_mps[19] = $mp->region_id;
			$array_mps[20] = $mp->resignation_date;
			$array_mps[21] = $mp->resignation_reason;
			$array_mps[22] = $mp->short_info;
			$array_mps[23] = $this->getMpSocials( $mp->socials );
			fputcsv( $this->csv_file_mps, $array_mps, ',', '"' );			
			
			/* 7. MPS__ASSISTANTS (6_1) */
			foreach ( $mp->assistants->assistant as $assistant ) {
				$array_mps__assistant = array_merge( array('mp_id' => $mp_id), get_object_vars($assistant) );
				fputcsv( $this->csv_file_mps__assistants, $array_mps__assistant, ',', '"' );
			}

			foreach ( $mp->declarations->declaration as $declaration ) {
				$declarate_date   = $declaration->declarate_date;
				$mp_decl_fullname = $declaration->fullname;
				$mp_id            = $mp_id;
				$post             = $declaration->post;
				$year             = $declaration->year;
				
				/* 8. MPS__DECLARATION__DEPOSITS (6_2) */
				foreach ( $declaration->deposits->deposit as $deposit ) {
					//$array_mps__declaration__deposits = array_merge( array('mp_fullname' => $mp_decl_fullname, 'mp_id' => $mp_id, 'post' => $post, 'declarate_date' => $declarate_date, 'year' => $year), get_object_vars($deposit) );					

					$array_mps__declaration__deposits[0] = $mp_decl_fullname;
					$array_mps__declaration__deposits[1] = $mp_id;
					$array_mps__declaration__deposits[2] = $post;
					$array_mps__declaration__deposits[3] = $declarate_date;
					$array_mps__declaration__deposits[4] = $year;
					$array_mps__declaration__deposits[5] = $deposit->decl_row_name_id;
					$array_mps__declaration__deposits[6] = $deposit->sum_abroad . " ";
					$array_mps__declaration__deposits[7] = $deposit->sum_all . " ";

					fputcsv( $this->csv_file_mps__declaration__deposits, $array_mps__declaration__deposits, ',', '"');			
				}
				
				/* 9. MPS__DECLARATION__FAMILY_MEMBERS (6_3) */
				foreach ( $declaration->family_members->member as $member ) {
					$array_mps__declaration__family_members = array_merge( array('mp_fullname' => $mp_decl_fullname, 'mp_id' => $mp_id, 'post' => $post, 'declarate_date' => $declarate_date, 'year' => $year), get_object_vars($member) );
					fputcsv( $this->csv_file_mps__declaration__family_members, $array_mps__declaration__family_members, ',', '"' );
				}
				
				/* 10. MPS__DECLARATION__FINANCIAL_LIABILITIES (6_4) */
				foreach ( $declaration->financial_liabilities->financial_liabilities as $financial_liabilities ) {
					//$array_mps__declaration__financial_liabilities = array_merge( array('mp_fullname' => $mp_decl_fullname, 'mp_id' => $mp_id, 'post' => $post, 'declarate_date' => $declarate_date, 'year' => $year), get_object_vars($financial_liabilities) );

					$array_mps__declaration__financial_liabilities[0] = $mp_decl_fullname;
					$array_mps__declaration__financial_liabilities[1] = $mp_id;
					$array_mps__declaration__financial_liabilities[2] = $post;
					$array_mps__declaration__financial_liabilities[3] = $declarate_date;
					$array_mps__declaration__financial_liabilities[4] = $year;
					$array_mps__declaration__financial_liabilities[5] = $financial_liabilities->decl_row_name_id;
					$array_mps__declaration__financial_liabilities[6] = $financial_liabilities->sum_abroad . " ";
					$array_mps__declaration__financial_liabilities[7] = $financial_liabilities->sum_all . " ";

					fputcsv( $this->csv_file_mps__declaration__financial_liabilities, $array_mps__declaration__financial_liabilities, ',', '"' );
				}
				
				/* 11. MPS__DECLARATION__INCOMES (6_5) */
				foreach ( $declaration->incomes->income as $income ) {
					//$array_mps__declaration__incomes = array_merge( array('mp_fullname' => $mp_decl_fullname, 'mp_id' => $mp_id, 'post' => $post, 'declarate_date' => $declarate_date, 'year' => $year), get_object_vars($income) );

					$array_mps__declaration__incomes[0] = $mp_decl_fullname;
					$array_mps__declaration__incomes[1] = $mp_id;
					$array_mps__declaration__incomes[2] = $post;
					$array_mps__declaration__incomes[3] = $declarate_date;
					$array_mps__declaration__incomes[4] = $year;
					$array_mps__declaration__incomes[5] = $income->decl_row_name_id;
					$array_mps__declaration__incomes[6] = $income->organizations . " ";
					$array_mps__declaration__incomes[7] = $income->sum_declarant . " ";
					$array_mps__declaration__incomes[8] = $income->sum_family . " ";

					fputcsv( $this->csv_file_mps__declaration__incomes, $array_mps__declaration__incomes, ',', '"' );
				}
				
				/* 12. MPS__DECLARATION__INCOMES_ABROAD (6_6) */
				foreach ( $declaration->incomes_abroad->incomes_abroad as $incomes_abroad ) {
					//$array_mps__declaration__incomes_abroad = array_merge( array('mp_fullname' => $mp_decl_fullname, 'mp_id' => $mp_id, 'post' => $post, 'declarate_date' => $declarate_date, 'year' => $year), get_object_vars($incomes_abroad) );
					
					$array_mps__declaration__incomes_abroad[0] = $mp_decl_fullname;
					$array_mps__declaration__incomes_abroad[1] = $mp_id;
					$array_mps__declaration__incomes_abroad[2] = $post;
					$array_mps__declaration__incomes_abroad[3] = $declarate_date;
					$array_mps__declaration__incomes_abroad[4] = $year;
					$array_mps__declaration__incomes_abroad[5] = $incomes_abroad->country;
					$array_mps__declaration__incomes_abroad[6] = $incomes_abroad->decl_row_name_id;
					$array_mps__declaration__incomes_abroad[7] = $incomes_abroad->sum_abroad . " ";
					$array_mps__declaration__incomes_abroad[8] = $incomes_abroad->sum_our . " ";

					fputcsv( $this->csv_file_mps__declaration__incomes_abroad, $array_mps__declaration__incomes_abroad, ',', '"' );
				}
				
				/* 13. MPS__DECLARATION__REALTIES (6_7) */
				foreach ( $declaration->realties->realty as $realty ) {
					//$array_mps__declaration__realties = array_merge( array('mp_fullname' => $mp_decl_fullname, 'mp_id' => $mp_id, 'post' => $post, 'declarate_date' => $declarate_date, 'year' => $year), get_object_vars($realty) );
					
					$array_mps__declaration__realties[0] = $mp_decl_fullname;
					$array_mps__declaration__realties[1] = $mp_id;
					$array_mps__declaration__realties[2] = $post;
					$array_mps__declaration__realties[3] = $declarate_date;
					$array_mps__declaration__realties[4] = $year;
					$array_mps__declaration__realties[5] = $realty->area . " ";
					$array_mps__declaration__realties[6] = $realty->decl_row_name_id;
					$array_mps__declaration__realties[7] = $realty->sum_other . " ";
					$array_mps__declaration__realties[8] = $realty->sum_own . " ";

					fputcsv( $this->csv_file_mps__declaration__realties, $array_mps__declaration__realties, ',', '"' );
				}
				
				/* 14. MPS__DECLARATION__TRANSPORTS (6_8) */
				foreach ( $declaration->transports->transport as $transport ) {
					//$array_mps__declaration__transports = array_merge( array('mp_fullname' => $mp_decl_fullname, 'mp_id' => $mp_id, 'post' => $post, 'declarate_date' => $declarate_date, 'year' => $year), get_object_vars($transport) );

					$array_mps__declaration__transports[0] = $mp_decl_fullname;
					$array_mps__declaration__transports[1] = $mp_id;
					$array_mps__declaration__transports[2] = $post;
					$array_mps__declaration__transports[3] = $declarate_date;
					$array_mps__declaration__transports[4] = $year;
					$array_mps__declaration__transports[5] = $transport->decl_row_name_id;
					$array_mps__declaration__transports[6] = $transport->model;
					$array_mps__declaration__transports[7] = $transport->sum_other . " ";
					$array_mps__declaration__transports[8] = $transport->sum_own . " ";
					$array_mps__declaration__transports[9] = $transport->year_create;

					fputcsv( $this->csv_file_mps__declaration__transports, $array_mps__declaration__transports, ',', '"' );
				}
			}
			
			/* 15. MPS__POST_BFDAS (6_9) */
			foreach ( $mp->post_bfdas->post_bfda as $post_bfda ) {
				$array_mps__post_bfda = array_merge( array('mp_id' => $mp_id), get_object_vars($post_bfda) );
				fputcsv( $this->csv_file_mps__post_bfdas, $array_mps__post_bfda, ',', '"' );
			}
			
			/* 16. MPS__POST_FRS (6_10) */
			foreach ( $mp->post_frs->post_fr as $post_fr ) {
				$array_mps__post_fr = array_merge( array('mp_id' => $mp_id), get_object_vars($post_fr) );
				fputcsv( $this->csv_file_mps__post_frs, $array_mps__post_fr, ',', '"' );
			}
		
			/* 17. MPS__POST_MPS (6_11) */
			foreach ( $mp->post_mps->post_mp as $post_mp ) {
				$array_mps__post_mp = array_merge( array('mp_id' => $mp_id), get_object_vars($post_mp) );
				fputcsv( $this->csv_file_mps__post_mps, $array_mps__post_mp, ',', '"' );
			}
		
			/* 18. MPS__RESIGNATION (6_12) */
			$resignation_date   = $mp->resignation_date;
			$resignation_reason = $mp->resignation_reason;
			if ( $resignation_date != '' ) {
				fputcsv( $this->csv_file_mps__resignation, [$mp_id, $resignation_date, $resignation_reason], ',', '"' );
			}
		}
		
		/* 19. PARTIES */				
		foreach ( $xml->parties->party as $party ) {
			fputcsv( $this->csv_file_parties, get_object_vars($party), ',', '"' );
		}

		/* 20. REGIONS */
		foreach ( $xml->regions->region as $region ) {
			fputcsv( $this->csv_file_regions, get_object_vars($region), ',', '"' );
		}
	}
	
	private function getMpEmails( $emails ) {
		$result = '';
		foreach ( $emails->email as $email ) {
			$result .= $email->value . '   ';
		}
		return $result;
	}

	private function getMpPhones( $phones ) {
		$result = '';
		foreach ( $phones->phone as $phone ) {
			$result .= $phone->value . '   ';
		}
		return $result;
	}

	private function getMpSocials( $socials ) {
		$result = '';
		foreach ( $socials->social as $social ) {
			$result .= $social->url . '   ';
		}
		return $result;
	}	

	private function _fopen( $token_name, $convocation_number ) {
		$path_file = self::DIR_CSV . '/' . $convocation_number . '/' . $token_name . $convocation_number . '.csv';
	    return fopen( $path_file, 'w' );
	}	
}

?>