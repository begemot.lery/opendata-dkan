<?php

require_once './php_scripts/parser_mps_4-7.php';

$convocation_number = $argv[1];

$mps47Parser = new ParserMps47( $convocation_number );
$mps47Parser->parseXmlToCsv();

?>