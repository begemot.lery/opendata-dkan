<?php

class ParserMps47 {
	const DIR_CSV = './parsed_csv';
	const DIR_XML = './downloaded_xml';

	private $xml;
	
	private $csv_file_mps;
	private $csv_file_mps__posts;
	private $csv_file_mps__posts_mp;
	
	public function __construct( $convocation_number ) {
		$xml_file = self::DIR_XML . '/' . 'mps_' . $convocation_number . '.xml';

		if ( file_exists( $xml_file ) ) {
			$this->xml = simplexml_load_file( $xml_file );
						
			$this->csv_file_mps = $this->_fopen( 'mps_', $convocation_number );
			fputcsv( $this->csv_file_mps, ['fullname', 'id', 'birthday', 'convocation', 'date_finish', 'date_oath', 'district_name', 'district_num', 'education', 'gender', 'mps_info', 'obr_info', 'obr_info_full', 'old_post', 'old_profession', 'party_name', 'party_num', 'photo', 'photo_ext', 'rada_id'], ',', '"' );
					
			$this->csv_file_mps__posts = $this->_fopen( 'mps__posts_', $convocation_number );
			fputcsv( $this->csv_file_mps__posts, ['mp_id', 'department_name', 'is_fraction', 'post_name'], ',', '"' );

			$this->csv_file_mps__posts_mp = $this->_fopen( 'mps__posts_mp_', $convocation_number );
			fputcsv( $this->csv_file_mps__posts_mp, ['mp_id', 'name', 'post_type'], ',', '"' );
		}
	}
	
	public function __destruct() {
		fclose( $this->csv_file_mps           );
		fclose( $this->csv_file_mps__posts    );
		fclose( $this->csv_file_mps__posts_mp );
	}
	
	public function parseXmlToCsv() {
		$xml = $this->xml;
		
		foreach ( $xml->ex_mps->ex_mp as $ex_mp ) {
			$mp_fullname = $ex_mp->firstname . ' ' . $ex_mp->patronymic . ' ' . $ex_mp->surname;
			$mp_id       = $ex_mp->id;

			$array_mps[0]  = $mp_fullname;
			$array_mps[1]  = $mp_id;
			$array_mps[2]  = $ex_mp->birthday;
			$array_mps[3]  = $ex_mp->convocation;
			$array_mps[4]  = $ex_mp->date_finish;
			$array_mps[5]  = $ex_mp->date_oath;
			$array_mps[6]  = $ex_mp->district_name;
			$array_mps[7]  = $ex_mp->district_num;
			$array_mps[8]  = $ex_mp->education;
			$array_mps[9]  = $ex_mp->gender;
			$array_mps[10] = $ex_mp->mps_info_full;
			$array_mps[11] = $ex_mp->obr_info;
			$array_mps[12] = $ex_mp->obr_info_full;
			$array_mps[13] = $ex_mp->old_post;
			$array_mps[14] = $ex_mp->old_profession;
			$array_mps[15] = $ex_mp->party_name;
			$array_mps[16] = $ex_mp->party_num;
			$array_mps[17] = $ex_mp->photo;
			$array_mps[18] = $ex_mp->photo_ext;
			$array_mps[19] = $ex_mp->rada_id;			
			fputcsv( $this->csv_file_mps, $array_mps, ',', '"' );			

			foreach ( $ex_mp->posts->post as $post ) {
				$array_mps__posts = array_merge( array('mp_id' => $mp_id), get_object_vars($post) );			
				fputcsv( $this->csv_file_mps__posts, $array_mps__posts, ',', '"' );
			}

			foreach ( $ex_mp->posts_mp->post_mp as $post_mp ) {
				$array_mps__posts_mp = array_merge( array('mp_id' => $mp_id), get_object_vars($post_mp) );			
				fputcsv( $this->csv_file_mps__posts_mp, $array_mps__posts_mp, ',', '"' );
			}					
		}		
	}	

	private function _fopen( $token_name, $convocation_number ) {
		$path_file = self::DIR_CSV . '/' . $convocation_number . '/' . $token_name . $convocation_number . '.csv';
	    return fopen( $path_file, 'w' );
	}	
}

?>