<?php

require_once './php_scripts/parser_mps_8.php';

$convocation_number = $argv[1];

$mps8Parser = new ParserMps8( $convocation_number );
$mps8Parser->parseXmlToCsv();

?>