#!/bin/bash

CSV_FILE__ORGANIZATION=$1
CSV_FILE__DEPARTMENTS=$2
CSV_FILE__POSITIONS=$3


echo "id_org, name_org" > $CSV_FILE__ORGANIZATION

echo "id_org, id_dep, name_dep" > $CSV_FILE__DEPARTMENTS

echo "id_org, id_dep, post, category, count" > $CSV_FILE__POSITIONS