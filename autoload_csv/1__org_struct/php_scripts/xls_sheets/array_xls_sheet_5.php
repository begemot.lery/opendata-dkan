<?php

//************************************//
//*  Фракції (РГВРУ №914)            *//
//************************************//

$array_xls_sheet_5 = array(

	// Секретаріат депутатської фракції ПАРТІЇ "БЛОК ПЕТРА ПОРОШЕНКА" 
	'1'  => array(
				'name' => 'A16',   
				'info' => array(
					'1' => array(
						'post'     => 'A17',
						'category' => 'B17',
						'count'    => 'C17',
					),
					'2' => array(
						'post'     => 'A18',
						'category' => 'B18',
						'count'    => 'C18',
					),
					'3' => array(
						'post'     => 'A19',
						'category' => 'B19',
						'count'    => 'C19',
					),
					'4' => array(
						'post'     => 'A20',
						'category' => 'B20',
						'count'    => 'C20',
					),
					'5' => array(
						'post'     => 'A21',
						'category' => 'B21',
						'count'    => 'C21',
					),
					'6' => array(
						'post'     => 'A22',
						'category' => 'B22',
						'count'    => 'C22',
					),
					'7' => array(
						'post'     => 'A23',
						'category' => 'B23',
						'count'    => 'C23',
					),
				),
			),
			
	// Секретаріат депутатської фракції Політичної партії "НАРОДНИЙ ФРОНТ"
	'2'  => array(
				'name' => 'A25',
				'info' => array(
					'1' => array(
						'post'     => 'A26',
						'category' => 'B26',
						'count'    => 'C26',
					),
					'2' => array(
						'post'     => 'A27',
						'category' => 'B27',
						'count'    => 'C27',
					),
					'3' => array(
						'post'     => 'A28',
						'category' => 'B28',
						'count'    => 'C28',
					),
					'4' => array(
						'post'     => 'A29',
						'category' => 'B29',
						'count'    => 'C29',
					),
					'5' => array(
						'post'     => 'A30',
						'category' => 'B30',
						'count'    => 'C30',
					),
					'6' => array(
						'post'     => 'A31',
						'category' => 'B31',
						'count'    => 'C31',
					),
					'7' => array(
						'post'     => 'A32',
						'category' => 'B32',
						'count'    => 'C32',
					),
				),
			),  

	// Секретаріат депутатської фракції Політичної партії "Об'єднання "САМОПОМІЧ" 
	'3'  => array(
				'name' => 'A34',
				'info' => array(
					'1' => array(
						'post'     => 'A35',
						'category' => 'B35',
						'count'    => 'C35',
					),
					'2' => array(
						'post'     => 'A36',
						'category' => 'B36',
						'count'    => 'C36',
					),
					'3' => array(
						'post'     => 'A37',
						'category' => 'B37',
						'count'    => 'C37',
					),
					'4' => array(
						'post'     => 'A38',
						'category' => 'B38',
						'count'    => 'C38',
					),
					'5' => array(
						'post'     => 'A39',
						'category' => 'B39',
						'count'    => 'C39',
					),
				),
			),  
	
	// Секретаріат депутатської фракції Політичної партії "Опозиційний блок" 
	'4'  => array(
				'name' => 'A41',
				'info' => array(
					'1' => array(
						'post'     => 'A42',
						'category' => 'B42',
						'count'    => 'C42',
					),
					'2' => array(
						'post'     => 'A43',
						'category' => 'B43',
						'count'    => 'C43',
					),
					'3' => array(
						'post'     => 'A44',
						'category' => 'B44',
						'count'    => 'C44',
					),
					'4' => array(
						'post'     => 'A45',
						'category' => 'B45',
						'count'    => 'C45',
					),
					'5' => array(
						'post'     => 'A46',
						'category' => 'B46',
						'count'    => 'C46',
					),
				),
			),  
	
	// Секретаріат депутатської фракції Радикальної партії Олега Ляшка
	'5'  => array(
				'name' => 'A49',
				'info' => array(
					'1' => array(
						'post'     => 'A50',
						'category' => 'B50',
						'count'    => 'C50',
					),
					'2' => array(
						'post'     => 'A51',
						'category' => 'B51',
						'count'    => 'C51',
					),
					'3' => array(
						'post'     => 'A52',
						'category' => 'B52',
						'count'    => 'C52',
					),
					'4' => array(
						'post'     => 'A53',
						'category' => 'B53',
						'count'    => 'C53',
					),
					'5' => array(
						'post'     => 'A54',
						'category' => 'B54',
						'count'	   => 'C54',
					),
				),
			),  

	// Секретаріат депутатської фракції політичної партії Всеукраїнське об'єднання "Батьківщина" 
	'6'  => array(
				'name' => 'A56',
				'info' => array(
					'1' => array(
						'post'     => 'A57',
						'category' => 'B57',
						'count'    => 'C57',
					),
					'2' => array(
						'post'     => 'A59',
						'category' => 'B59',
						'count'    => 'C59',
					),
					'3' => array(
						'post'     => 'A60',
						'category' => 'B60',
						'count'    => 'C60',
					),
				),
			),  

	// Секретаріат депутатської групи "Партія "Відродження" 
	'7'  => array(
				'name' => 'A66',
				'info' => array(
					'1' => array(
						'post'     => 'A67',
						'category' => 'B67',
						'count'    => 'C67',
					),
					'2' => array(
						'post'     => 'A68',
						'category' => 'B68',
						'count'    => 'C68',
					),
					'3' => array(
						'post'     => 'A69',
						'category' => 'B69',
						'count'    => 'C69',
					),
					'4' => array(
						'post'     => 'A70',
						'category' => 'B70',
						'count'    => 'C70',
					),
				),
			),  

	// Секретаріат депутатської групи "Воля народу" 
	'8'  => array(
				'name' => 'A73',
				'info' => array(
					'1' => array(
						'post'     => 'A74',
						'category' => 'B74',
						'count'    => 'C74',
					),
					'2' => array(
						'post'     => 'A75',
						'category' => 'B75',
						'count'    => 'C75',
					),
					'3' => array(
						'post'     => 'A76',
						'category' => 'B76',
						'count'    => 'C76',
					),
					'4' => array(
						'post'     => 'A77',
						'category' => 'B77',
						'count'    => 'C77',
					),
				),
			),
);

?>