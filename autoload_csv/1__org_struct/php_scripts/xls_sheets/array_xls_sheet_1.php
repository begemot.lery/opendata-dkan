<?php

//************************************//
//*  Керівництво апарату (РВРУ№928)  *//
//************************************//

$array_xls_sheet_1 = array(
	// ???????
	'1'  => array(
				'name' => 'A11',   
				'info' => array(
					'1' => array(
						'post' => 'B15',
						'category' => 'C15',
						'count' => 'D15',
					),
					'2' => array(
						'post' => 'B17',
						'category' => 'C17',
						'count' => 'D17',
					),
					'3' => array(
						'post' => 'B18',
						'category' => 'C18',
						'count' => 'D18',
					),
					'4' => array(
						'post' => 'B19',
						'category' => 'C19',
						'count' => 'D19',
					),
					'5' => array(
						'post' => 'B20',
						'category' => 'C20',
						'count' => 'D20',
					),
					'6' => array(
						'post' => 'B21',
						'category' => 'C21',
						'count' => 'D21',
					),					
				),
			),
			
	// Група радників, помічників Голів Верховної Ради України, які припинили свої повноваження		
	'2'  => array(
				'name' => 'A28',
				'info' => array(
					'1' => array(
						'post' => 'B29',
						'category' => 'C29',
						'count' => 'D29',
					),
				),
			),  
);

?>