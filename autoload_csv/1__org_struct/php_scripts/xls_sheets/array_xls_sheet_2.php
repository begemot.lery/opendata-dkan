<?php

//******************************//
//*  Апарат(РГВРУ №1278,1279)  *//
//******************************//

$array_xls_sheet_2 = array(

	// СЕКРЕТАРІАТ ГОЛОВИ ВЕРХОВНОЇ РАДИ УКРАЇНИ
	'1'  => array(
				'name' => 'A16',
				'info' => array(
					'1' => array(
						'post'     => 'B17',
						'category' => 'C17',
						'count'    => 'D17',
					),
					'2' => array(
						'post'     => 'B18',
						'category' => 'C18',
						'count'    => 'D18',
					),
					'3' => array(
						'post'     => 'B19',
						'category' => 'C19',
						'count'    => 'D19',
					),
				),
			),  
			
    // Група радників, помічників Голови Верховної Ради України  
	'2'  => array(
				'name' => 'A21',
				'info' => array(
					'1' => array(
						'post'     => 'B22',
						'category' => 'C22',
						'count'    => 'D22',
					),
				),
			),  

    // Відділ документального забезпечення Голови Верховної Ради України  
	'3'  => array(
				'name' => 'A24',
				'info' => array(
					'1' => array(
						'post'     => 'B25',
						'category' => 'C25',
						'count'    => 'D25',
					),
					'2' => array(
						'post'     => 'B26',
						'category' => 'C26',
						'count'    => 'D26',
					),
					'3' => array(
						'post'     => 'B27',
						'category' => 'C27',
						'count'    => 'D27',
					),
				),
			),  

    // Приймальня Голови Верховної Ради України 
	'4'  => array(
				'name' => 'A31',
				'info' => array(
					'1' => array(
						'post'     => 'B32',
						'category' => 'C32',
						'count'    => 'D32',
					),
				),
			),  

    // СЕКРЕТАРІАТ ПЕРШОГО ЗАСТУПНИКА ГОЛОВИ ВЕРХОВНОЇ РАДИ УКРАЇНИ 
	'5'  => array(
				'name' => 'A36',
				'info' => array(
					'1' => array(
						'post'     => 'B37',
						'category' => 'C37',
						'count'    => 'D37',
					),
					'2' => array(
						'post'     => 'B38',
						'category' => 'C38',
						'count'    => 'D38',
					),
					'3' => array(
						'post'     => 'B40',
						'category' => 'C40',
						'count'    => 'D40',
					),
					'4' => array(
						'post'     => 'B41',
						'category' => 'C41',
						'count'    => 'D41',
					),
				),
			),  

    // СЕКРЕТАРІАТ ЗАСТУПНИКА ГОЛОВИ ВЕРХОВНОЇ РАДИ  УКРАЇНИ
	'6'  => array(
				'name' => 'A44',
				'info' => array(
					'1' => array(
						'post'     => 'B45',
						'category' => 'C45',
						'count'    => 'D45',
					),
					'2' => array(
						'post'     => 'B46',
						'category' => 'C46',
						'count'    => 'D46',
					),
					'3' => array(
						'post'     => 'B47',
						'category' => 'C47',
						'count'    => 'D47',
					),
					'4' => array(
						'post'     => 'B48',
						'category' => 'C48',
						'count'    => 'D48',
					),
					'5' => array(
						'post'     => 'B49',
						'category' => 'C49',
						'count'    => 'D49',
					),
					'6' => array(
						'post'     => 'B50',
						'category' => 'C50',
						'count'    => 'D50',
					),
				),
			),  

    // ??????????????
	'7'  => array(
				'name' => 'A53',
				'info' => array(
					'1' => array(
						'post'     => 'B54',
						'category' => 'C54',
						'count'    => 'D54',
					),
					'2' => array(
						'post'     => 'B55',
						'category' => 'C55',
						'count'    => 'D55',
					),
					'3' => array(
						'post'     => 'B56',
						'category' => 'C56',
						'count'    => 'D56',
					),
					'4' => array(
						'post'     => 'B58',
						'category' => 'C58',
						'count'    => 'D58',
					),
				),
			),  

    // ГОЛОВНЕ НАУКОВО-ЕКСПЕРТНЕ УПРАВЛІННЯ 
	'8'  => array(
				'name' => 'A61',
				'info' => array(
					'1' => array(
						'post'     => 'B62',
						'category' => 'C62',
						'count'    => 'D62',
					),
					'2' => array(
						'post'     => 'B63',
						'category' => 'C63',
						'count'    => 'D63',
					),
					'3' => array(
						'post'     => 'B64',
						'category' => 'C64',
						'count'    => 'D64',
					),
					'4' => array(
						'post'     => 'B65',
						'category' => 'C65',
						'count'    => 'D65',
					),
					'5' => array(
						'post'     => 'B66',
						'category' => 'C66',
						'count'    => 'D66',
					),
				),
			),  
//--------------------------------------------------------------------------------------//
    // Відділ з конституційних питань та державного будівництва 
	'9'  => array(
				'name' => 'A69',
				'info' => array(
					'1' => array(
						'post'     => 'B70',
						'category' => 'C70',
						'count'    => 'D70',
					),
					'2' => array(
						'post'     => 'B71',
						'category' => 'C71',
						'count'    => 'D71',
					),
				),
			),  

    // Відділ з гуманітарних і міжнародних питань, гармонізації законодавства з країнами ЄС та захисту прав людини
	'10'  => array(
				'name' => 'A74',
				'info' => array(
					'1' => array(
						'post'     => 'B75',
						'category' => 'C75',
						'count'    => 'D75',
					),
					'2' => array(
						'post'     => 'B76',
						'category' => 'C76',
						'count'    => 'D76',
					),
					'3' => array(
						'post'     => 'B77',
						'category' => 'C77',
						'count'    => 'D77',
					),
				),
			),  

    // Відділ з соціальних питань та праці
	'11'  => array(
				'name' => 'A80',
				'info' => array(
					'1' => array(
						'post'     => 'B81',
						'category' => 'C81',
						'count'    => 'D81',
					),
					'2' => array(
						'post'     => 'B82',
						'category' => 'C82',
						'count'    => 'D82',
					),
					'3' => array(
						'post'     => 'B83',
						'category' => 'C83',
						'count'    => 'D83',
					),
				),
			),  

    // Відділ з питань цивільних і господарських відносин та підприємництва
	'12'  => array(
				'name' => 'A86',
				'info' => array(
					'1' => array(
						'post'     => 'B87',
						'category' => 'C87',
						'count'    => 'D87',
					),
					'2' => array(
						'post'     => 'B88',
						'category' => 'C88',
						'count'    => 'D88',
					),
					'3' => array(
						'post'     => 'B89',
						'category' => 'C89',
						'count'    => 'D89',
					),
					'4' => array(
						'post'     => 'B90',
						'category' => 'C90',
						'count'    => 'D90',
					),
				),
			),  

    // Відділ з аграрних і екологічних питань та природокористування
	'13'  => array(
				'name' => 'A93',
				'info' => array(
					'1' => array(
						'post'     => 'B94',
						'category' => 'C94',
						'count'    => 'D94',
					),
					'2' => array(
						'post'     => 'B95',
						'category' => 'C95',
						'count'    => 'D95',
					),
					'3' => array(
						'post'     => 'B96',
						'category' => 'C96',
						'count'    => 'D96',
					),
				),
			),  

    // Відділ з питань бюджету, оподаткування та банківської діяльності
	'14'  => array(
				'name' => 'A99',
				'info' => array(
					'1' => array(
						'post'     => 'B100',
						'category' => 'C100',
						'count'    => 'D100',
					),
					'2' => array(
						'post'     => 'B101',
						'category' => 'C101',
						'count'    => 'D101',
					),
					'3' => array(
						'post'     => 'B102',
						'category' => 'C102',
						'count'    => 'D102',
					),
				),
			),  

    // Відділ з питань макроекономіки та промислового розвитку
	'15'  => array(
				'name' => 'A105',
				'info' => array(
					'1' => array(
						'post'     => 'B106',
						'category' => 'C106',
						'count'    => 'D106',
					),
					'2' => array(
						'post'     => 'B107',
						'category' => 'C107',
						'count'    => 'D107',
					),
				),
			),  

    // Відділ з питань національної безпеки, оборони, правоохоронної діяльності та боротьби із злочинністю
	'16'  => array(
				'name' => 'A110',
				'info' => array(
					'1' => array(
						'post'     => 'B111',
						'category' => 'C111',
						'count'    => 'D111',
					),
					'2' => array(
						'post'     => 'B112',
						'category' => 'C112',
						'count'    => 'D112',
					),
				),
			),  

    // ГОЛОВНЕ ЮРИДИЧНЕ УПРАВЛІННЯ
	'17'  => array(
				'name' => 'A116',
				'info' => array(
					'1' => array(
						'post'     => 'B117',
						'category' => 'C117',
						'count'    => 'D117',
					),
					'2' => array(
						'post'     => 'B118',
						'category' => 'C118',
						'count'    => 'D118',
					),
					'3' => array(
						'post'     => 'B119',
						'category' => 'C119',
						'count'    => 'D119',
					),
					'4' => array(
						'post'     => 'B120',
						'category' => 'C120',
						'count'    => 'D120',
					),
					'5' => array(
						'post'     => 'B121',
						'category' => 'C121',
						'count'    => 'D121',
					),
				),
			),  

    // УПРАВЛІННЯ КОНСТИТУЦІЙНОГО ЗАКОНОДАВСТВА ТА ДЕРЖАВНОГО БУДІВНИЦТВА
	'18'  => array(
				'name' => 'A124',
				'info' => array(
					'1' => array(
						'post'     => 'B125',
						'category' => 'C125',
						'count'    => 'D125',
					),
					'2' => array(
						'post'     => 'B126',
						'category' => 'C126',
						'count'    => 'D126',
					),
				),
			),  

    // Відділ з питань конституційного, кримінального, адміністративного законодавства та судово-правової реформи
	'19'  => array(
				'name' => 'A129',
				'info' => array(
					'1' => array(
						'post'     => 'B130',
						'category' => 'C130',
						'count'    => 'D130',
					),
					'2' => array(
						'post'     => 'B131',
						'category' => 'C131',
						'count'    => 'D131',
					),
					'3' => array(
						'post'     => 'B132',
						'category' => 'C132',
						'count'    => 'D132',
					),
				),
			),  			

    // Відділ з міжнародно-правових питань
	'20'  => array(
				'name' => 'A135',
				'info' => array(
					'1' => array(
						'post'     => 'B136',
						'category' => 'C136',
						'count'    => 'D136',
					),
					'2' => array(
						'post'     => 'B137',
						'category' => 'C137',
						'count'    => 'D137',
					),
					'3' => array(
						'post'     => 'B138',
						'category' => 'C138',
						'count'    => 'D138',
					),
					'4' => array(
						'post'     => 'B139',
						'category' => 'C139',
						'count'    => 'D139',
					),
				),
			),  

    // Відділ обліку та систематизації законодавства
	'21'  => array(
				'name' => 'A142',
				'info' => array(
					'1' => array(
						'post'     => 'B143',
						'category' => 'C143',
						'count'    => 'D143',
					),
					'2' => array(
						'post'     => 'B144',
						'category' => 'C144',
						'count'    => 'D144',
					),
					'3' => array(
						'post'     => 'B145',
						'category' => 'C145',
						'count'    => 'D145',
					),
					'4' => array(
						'post'     => 'B146',
						'category' => 'C146',
						'count'    => 'D146',
					),
				),
			),  

    // УПРАВЛІННЯ ЦИВІЛЬНОГО, ФІНАНСОВОГО ТА СОЦІАЛЬНОГО ЗАКОНОДАВСТВА
	'22'  => array(
				'name' => 'A150',
				'info' => array(
					'1' => array(
						'post'     => 'B151',
						'category' => 'C151',
						'count'    => 'D151',
					),
					'2' => array(
						'post'     => 'B152',
						'category' => 'C152',
						'count'    => 'D152',
					),
				),
			),  

    // Відділ з питань цивільного, податкового, бюджетного законодавства та підприємництва
	'23'  => array(
				'name' => 'A155',
				'info' => array(
					'1' => array(
						'post'     => 'B156',
						'category' => 'C156',
						'count'    => 'D156',
					),
					'2' => array(
						'post'     => 'B157',
						'category' => 'C157',
						'count'    => 'D157',
					),
					'3' => array(
						'post'     => 'B158',
						'category' => 'C158',
						'count'    => 'D158',
					),
					'4' => array(
						'post'     => 'B159',
						'category' => 'C159',
						'count'    => 'D159',
					),
				),
			),  

    // Відділ з питань екологічного, соціального та гуманітарного законодавства
	'24'  => array(
				'name' => 'A162',
				'info' => array(
					'1' => array(
						'post'     => 'B163',
						'category' => 'C163',
						'count'    => 'D163',
					),
					'2' => array(
						'post'     => 'B164',
						'category' => 'C164',
						'count'    => 'D164',
					),
					'3' => array(
						'post'     => 'B165',
						'category' => 'C165',
						'count'    => 'D165',
					),
				),
			),  

    // ГОЛОВНЕ УПРАВЛІННЯ ДОКУМЕНТАЛЬНОГО ЗАБЕЗПЕЧЕННЯ
	'25'  => array(
				'name' => 'A170',
				'info' => array(
					'1' => array(
						'post'     => 'B171',
						'category' => 'C171',
						'count'    => 'D171',
					),
					'2' => array(
						'post'     => 'B172',
						'category' => 'C172',
						'count'    => 'D172',
					),
					'3' => array(
						'post'     => 'B173',
						'category' => 'C173',
						'count'    => 'D173',
					),
				),
			),  

    // Відділ опрацювання та випуску законодавчих актів
	'26'  => array(
				'name' => 'A177',
				'info' => array(
					'1' => array(
						'post'     => 'B178',
						'category' => 'C178',
						'count'    => 'D178',
					),
					'2' => array(
						'post'     => 'B179',
						'category' => 'C179',
						'count'    => 'D179',
					),
				),
			),  

    // Сектор оформлення законодавчих актів
	'27'  => array(
				'name' => 'A182',
				'info' => array(
					'1' => array(
						'post'     => 'B183',
						'category' => 'C183',
						'count'    => 'D183',
					),
					'2' => array(
						'post'     => 'B184',
						'category' => 'C184',
						'count'    => 'D184',
					),
					'3' => array(
						'post'     => 'B185',
						'category' => 'C185',
						'count'    => 'D185',
					),
					'4' => array(
						'post'     => 'B186',
						'category' => 'C186',
						'count'    => 'D186',
					),
				),
			),  

    // Сектор випуску законодавчих актів
	'28'  => array(
				'name' => 'A189',
				'info' => array(
					'1' => array(
						'post'     => 'B190',
						'category' => 'C190',
						'count'    => 'D190',
					),
					'2' => array(
						'post'     => 'B191',
						'category' => 'C191',
						'count'    => 'D191',
					),
					'3' => array(
						'post'     => 'B192',
						'category' => 'C192',
						'count'    => 'D192',
					),
					'4' => array(
						'post'     => 'B193',
						'category' => 'C193',
						'count'    => 'D193',
					),
				),
			),  

    // Відділ організації  діловодства в Апараті
	'29'  => array(
				'name' => 'A197',
				'info' => array(
					'1' => array(
						'post'     => 'B198',
						'category' => 'C198',
						'count'    => 'D198',
					),
					'2' => array(
						'post'     => 'B199',
						'category' => 'C199',
						'count'    => 'D199',
					),
				),
			),  

    // Сектор організації діловодства та оформлення відряджень 
	'30'  => array(
				'name' => 'A202',
				'info' => array(
					'1' => array(
						'post'     => 'B203',
						'category' => 'C203',
						'count'    => 'D203',
					),
					'2' => array(
						'post'     => 'B204',
						'category' => 'C204',
						'count'    => 'D204',
					),
					'3' => array(
						'post'     => 'B205',
						'category' => 'C205',
						'count'    => 'D205',
					),
				),
			),  

    // Сектор з організаційних питань
	'31'  => array(
				'name' => 'A208',
				'info' => array(
					'1' => array(
						'post'     => 'B209',
						'category' => 'C209',
						'count'    => 'D209',
					),
					'2' => array(
						'post'     => 'B210',
						'category' => 'C210',
						'count'    => 'D210',
					),
					'3' => array(
						'post'     => 'B211',
						'category' => 'C211',
						'count'    => 'D211',
					),
				),
			),  

    // Сектор архівного діловодства
	'32'  => array(
				'name' => 'A214',
				'info' => array(
					'1' => array(
						'post'     => 'B215',
						'category' => 'C215',
						'count'    => 'D215',
					),
					'2' => array(
						'post'     => 'B216',
						'category' => 'C216',
						'count'    => 'D216',
					),
					'3' => array(
						'post'     => 'B217',
						'category' => 'C217',
						'count'    => 'D217',
					),
				),
			),  

    // Відділ службової кореспонденції
	'33'  => array(
				'name' => 'A221',
				'info' => array(
					'1' => array(
						'post'     => 'B222',
						'category' => 'C222',
						'count'    => 'D222',
					),
					'2' => array(
						'post'     => 'B223',
						'category' => 'C223',
						'count'    => 'D223',
					),
				),
			),  

    // Сектор прийому і відправлення кореспонденції
	'34'  => array(
				'name' => 'A226',
				'info' => array(
					'1' => array(
						'post'     => 'B227',
						'category' => 'C227',
						'count'    => 'D227',
					),
					'2' => array(
						'post'     => 'B228',
						'category' => 'C228',
						'count'    => 'D228',
					),
					'3' => array(
						'post'     => 'B209',
						'category' => 'C209',
						'count'    => 'D209',
					),
					'4' => array(
						'post'     => 'B230',
						'category' => 'C230',
						'count'    => 'D230',
					),
				),
			),  

    // Сектор аналізу кореспонденції
	'35'  => array(
				'name' => 'A233',
				'info' => array(
					'1' => array(
						'post'     => 'B234',
						'category' => 'C234',
						'count'    => 'D234',
					),
					'2' => array(
						'post'     => 'B235',
						'category' => 'C235',
						'count'    => 'D235',
					),
					'3' => array(
						'post'     => 'B236',
						'category' => 'C236',
						'count'    => 'D236',
					),
				),
			),  

    // Сектор реєстрації кореспонденції
	'36'  => array(
				'name' => 'A239',
				'info' => array(
					'1' => array(
						'post'     => 'B240',
						'category' => 'C240',
						'count'    => 'D240',
					),
					'2' => array(
						'post'     => 'B241',
						'category' => 'C241',
						'count'    => 'D241',
					),
					'3' => array(
						'post'     => 'B242',
						'category' => 'C242',
						'count'    => 'D242',
					),
					'4' => array(
						'post'     => 'B243',
						'category' => 'C243',
						'count'    => 'D243',
					),
				),
			),  

    // УПРАВЛІННЯ АВТОМАТИЗОВАНОЇ ОБРОБКИ ДОКУМЕНТІВ 
	'37'  => array(
				'name' => 'A247',
				'info' => array(
					'1' => array(
						'post'     => 'B248',
						'category' => 'C248',
						'count'    => 'D248',
					),
					'2' => array(
						'post'     => 'B249',
						'category' => 'C249',
						'count'    => 'D249',
					),
					'3' => array(
						'post'     => 'B250',
						'category' => 'C250',
						'count'    => 'D250',
					),
				),
			),  

    // Відділ автоматизованих комплексів
	'38'  => array(
				'name' => 'A253',
				'info' => array(
					'1' => array(
						'post'     => 'B254',
						'category' => 'C254',
						'count'    => 'D254',
					),
				),
			),  

    // Сектор супроводження автоматизованих комплексів та прикладного обладнання
	'39'  => array(
				'name' => 'A256',
				'info' => array(
					'1' => array(
						'post'     => 'B257',
						'category' => 'C257',
						'count'    => 'D257',
					),
					'2' => array(
						'post'     => 'B258',
						'category' => 'C258',
						'count'    => 'D258',
					),
					'3' => array(
						'post'     => 'B259',
						'category' => 'C259',
						'count'    => 'D259',
					),
				),
			),  

    // Сектор автоматизованої обробки законопроектів та супровідних матеріалів
	'40'  => array(
				'name' => 'A262',
				'info' => array(
					'1' => array(
						'post'     => 'B263',
						'category' => 'C263',
						'count'    => 'D263',
					),
					'2' => array(
						'post'     => 'B264',
						'category' => 'C264',
						'count'    => 'D264',
					),
					'3' => array(
						'post'     => 'B265',
						'category' => 'C265',
						'count'    => 'D265',
					),
					'4' => array(
						'post'     => 'B266',
						'category' => 'C266',
						'count'    => 'D266',
					),
					'5' => array(
						'post'     => 'B267',
						'category' => 'C267',
						'count'    => 'D267',
					),
				),
			),  

    // Відділ спеціалізованих інформаційних систем запису і обробки мовної інформації 
	'41'  => array(
				'name' => 'A271',
				'info' => array(
					'1' => array(
						'post'     => 'B272',
						'category' => 'C272',
						'count'    => 'D272',
					),
					'2' => array(
						'post'     => 'B273',
						'category' => 'C273',
						'count'    => 'D273',
					),
				),
			),  

    // Сектор комп'ютерної обробки мовної інформації пленарних засідань
	'42'  => array(
				'name' => 'A276',
				'info' => array(
					'1' => array(
						'post'     => 'B277',
						'category' => 'C277',
						'count'    => 'D277',
					),
					'2' => array(
						'post'     => 'B278',
						'category' => 'C278',
						'count'    => 'D278',
					),
					'3' => array(
						'post'     => 'B279',
						'category' => 'C279',
						'count'    => 'D279',
					),
					'4' => array(
						'post'     => 'B280',
						'category' => 'C280',
						'count'    => 'D280',
					),
					'5' => array(
						'post'     => 'B281',
						'category' => 'C281',
						'count'    => 'D281',
					),
				),
			),  

    // Сектор стенографічного запису і редагування текстових звітів
	'43'  => array(
				'name' => 'A284',
				'info' => array(
					'1' => array(
						'post'     => 'B285',
						'category' => 'C285',
						'count'    => 'D285',
					),
					'2' => array(
						'post'     => 'B286',
						'category' => 'C286',
						'count'    => 'D286',
					),
					'3' => array(
						'post'     => 'B287',
						'category' => 'C287',
						'count'    => 'D287',
					),
				),
			),  

    // Сектор комп'ютерної обробки мовної інформації парламентських заходів
	'44'  => array(
				'name' => 'A290',
				'info' => array(
					'1' => array(
						'post'     => 'B291',
						'category' => 'C291',
						'count'    => 'D291',
					),
					'2' => array(
						'post'     => 'B292',
						'category' => 'C292',
						'count'    => 'D292',
					),
					'3' => array(
						'post'     => 'B293',
						'category' => 'C293',
						'count'    => 'D293',
					),
					'4' => array(
						'post'     => 'B294',
						'category' => 'C294',
						'count'    => 'D294',
					),
					'5' => array(
						'post'     => 'B295',
						'category' => 'C295',
						'count'    => 'D295',
					),
				),
			),  

    // Відділ комп'ютерної обробки законодавчих актів
	'45'  => array(
				'name' => 'A299',
				'info' => array(
					'1' => array(
						'post'     => 'B300',
						'category' => 'C300',
						'count'    => 'D300',
					),
					'2' => array(
						'post'     => 'B301',
						'category' => 'C301',
						'count'    => 'D301',
					),
					'3' => array(
						'post'     => 'B302',
						'category' => 'C302',
						'count'    => 'D302',
					),
					'4' => array(
						'post'     => 'B303',
						'category' => 'C303',
						'count'    => 'D303',
					),
				),
			),  

    // Редакційний відділ
	'46'  => array(
				'name' => 'A307',
				'info' => array(
					'1' => array(
						'post'     => 'B308',
						'category' => 'C308',
						'count'    => 'D308',
					),
					'2' => array(
						'post'     => 'B309',
						'category' => 'C309',
						'count'    => 'D309',
					),
				),
			),  

    // Сектор редагування стенографічних матеріалів
	'47'  => array(
				'name' => 'A312',
				'info' => array(
					'1' => array(
						'post'     => 'B313',
						'category' => 'C313',
						'count'    => 'D313',
					),
					'2' => array(
						'post'     => 'B314',
						'category' => 'C314',
						'count'    => 'D314',
					),
					'3' => array(
						'post'     => 'B315',
						'category' => 'C315',
						'count'    => 'D315',
					),
					'4' => array(
						'post'     => 'B316',
						'category' => 'C316',
						'count'    => 'D316',
					),
				),
			),  

    // Сектор редагування законопроектів
	'48'  => array(
				'name' => 'A319',
				'info' => array(
					'1' => array(
						'post'     => 'B320',
						'category' => 'C320',
						'count'    => 'D320',
					),
					'2' => array(
						'post'     => 'B321',
						'category' => 'C321',
						'count'    => 'D321',
					),
					'3' => array(
						'post'     => 'B322',
						'category' => 'C322',
						'count'    => 'D322',
					),
					'4' => array(
						'post'     => 'B323',
						'category' => 'C323',
						'count'    => 'D323',
					),
				),
			),  

    // Сектор редакційного  опрацювання документів
	'49'  => array(
				'name' => 'A326',
				'info' => array(
					'1' => array(
						'post'     => 'B327',
						'category' => 'C327',
						'count'    => 'D327',
					),
					'2' => array(
						'post'     => 'B328',
						'category' => 'C328',
						'count'    => 'D328',
					),
				),
			),  

    // Сектор автоматизації редакційного процесу
	'50'  => array(
				'name' => 'A331',
				'info' => array(
					'1' => array(
						'post'     => 'B332',
						'category' => 'C332',
						'count'    => 'D332',
					),
					'2' => array(
						'post'     => 'B333',
						'category' => 'C333',
						'count'    => 'D333',
					),
					'3' => array(
						'post'     => 'B334',
						'category' => 'C334',
						'count'    => 'D334',
					),
					'4' => array(
						'post'     => 'B335',
						'category' => 'C335',
						'count'    => 'D335',
					),
				),
			),  

    // ГОЛОВНЕ ОРГАНІЗАЦІЙНЕ УПРАВЛІННЯ
	'51'  => array(
				'name' => 'A340',
				'info' => array(
					'1' => array(
						'post'     => 'B341',
						'category' => 'C341',
						'count'    => 'D341',
					),
					'2' => array(
						'post'     => 'B342',
						'category' => 'C342',
						'count'    => 'D342',
					),
					'3' => array(
						'post'     => 'B343',
						'category' => 'C343',
						'count'    => 'D343',
					),
				),
			),  

    // УПРАВЛІННЯ З ПИТАНЬ ПРОХОДЖЕННЯ ЗАКОНОПРОЕКТІВ ТА РОБОТИ З КОМІТЕТАМИ І ДЕПУТАТСЬКИМИ ФРАКЦІЯМИ
	'52'  => array(
				'name' => 'A346',
				'info' => array(
					'1' => array(
						'post'     => 'B347',
						'category' => 'C347',
						'count'    => 'D347',
					),
				),
			),  

    // Відділ по роботі з комітетами, депутатськими фракціями і групами
	'53'  => array(
				'name' => 'A349',
				'info' => array(
					'1' => array(
						'post'     => 'B350',
						'category' => 'C350',
						'count'    => 'D350',
					),
					'2' => array(
						'post'     => 'B351',
						'category' => 'C351',
						'count'    => 'D351',
					),
					'3' => array(
						'post'     => 'B352',
						'category' => 'C352',
						'count'    => 'D352',
					),
					'4' => array(
						'post'     => 'B353',
						'category' => 'C353',
						'count'    => 'D353',
					),
				),
			),  

    // Відділ реєстрації та  обліку проходження законопроектів
	'54'  => array(
				'name' => 'A358',
				'info' => array(
					'1' => array(
						'post'     => 'B359',
						'category' => 'C359',
						'count'    => 'D359',
					),
					'2' => array(
						'post'     => 'B360',
						'category' => 'C360',
						'count'    => 'D360',
					),
					'3' => array(
						'post'     => 'B361',
						'category' => 'C361',
						'count'    => 'D361',
					),
				),
			),  

    // Відділ організаційного забезпечення пленарних засідань Верховної Ради України 
	'55'  => array(
				'name' => 'A366',
				'info' => array(
					'1' => array(
						'post'     => 'B367',
						'category' => 'C367',
						'count'    => 'D367',
					),
				),
			),  

    // Сектор по забезпеченню пленарних засідань Верховної Ради України
	'56'  => array(
				'name' => 'A368',
				'info' => array(
					'1' => array(
						'post'     => 'B369',
						'category' => 'C369',
						'count'    => 'D369',
					),
					'2' => array(
						'post'     => 'B370',
						'category' => 'C370',
						'count'    => 'D370',
					),
					'3' => array(
						'post'     => 'B371',
						'category' => 'C371',
						'count'    => 'D371',
					),
				),
			),  

    // Сектор документального забезпечення та реєстрації учасників пленарних засідань
	'57'  => array(
				'name' => 'A375',
				'info' => array(
					'1' => array(
						'post'     => 'B376',
						'category' => 'C376',
						'count'    => 'D376',
					),
					'2' => array(
						'post'     => 'B377',
						'category' => 'C377',
						'count'    => 'D377',
					),
					'3' => array(
						'post'     => 'B378',
						'category' => 'C378',
						'count'    => 'D378',
					),
					'4' => array(
						'post'     => 'B379',
						'category' => 'C379',
						'count'    => 'D379',
					),
				),
			),  

    // ІНФОРМАЦІЙНЕ УПРАВЛІННЯ
	'58'  => array(
				'name' => 'A384',
				'info' => array(
					'1' => array(
						'post'     => 'B385',
						'category' => 'C385',
						'count'    => 'D385',
					),
					'2' => array(
						'post'     => 'B386',
						'category' => 'C386',
						'count'    => 'D386',
					),
					'3' => array(
						'post'     => 'B388',
						'category' => 'C388',
						'count'    => 'D388',
					),
				),
			),  

    // Відділ висвітлення діяльності Верховної Ради України 
	'59'  => array(
				'name' => 'A391',
				'info' => array(
					'1' => array(
						'post'     => 'B392',
						'category' => 'C392',
						'count'    => 'D392',
					),
					'2' => array(
						'post'     => 'B393',
						'category' => 'C393',
						'count'    => 'D393',
					),
					'3' => array(
						'post'     => 'B394',
						'category' => 'C394',
						'count'    => 'D394',
					),
				),
			),  

    // Відділ моніторингу
	'60'  => array(
				'name' => 'A397',
				'info' => array(
					'1' => array(
						'post'     => 'B398',
						'category' => 'C398',
						'count'    => 'D398',
					),
					'2' => array(
						'post'     => 'B399',
						'category' => 'C399',
						'count'    => 'D399',
					),
				),
			),  

    // Відділ роботи з публічною інформацією
	'61'  => array(
				'name' => 'A402',
				'info' => array(
					'1' => array(
						'post'     => 'B403',
						'category' => 'C403',
						'count'    => 'D403',
					),
					'2' => array(
						'post'     => 'B404',
						'category' => 'C404',
						'count'    => 'D404',
					),
					'3' => array(
						'post'     => 'B404',
						'category' => 'C404',
						'count'    => 'D404',
					),
					'4' => array(
						'post'     => 'B406',
						'category' => 'C406',
						'count'    => 'D406',
					),
				),
			),  

    // Відділ інформаційно-бібліотечного забезпечення
	'62'  => array(
				'name' => 'A409',
				'info' => array(
					'1' => array(
						'post'     => 'B410',
						'category' => 'C410',
						'count'    => 'D410',
					),
					'2' => array(
						'post'     => 'B411',
						'category' => 'C411',
						'count'    => 'D411',
					),
					'3' => array(
						'post'     => 'B412',
						'category' => 'C412',
						'count'    => 'D412',
					),
				),
			),  

    // УПРАВЛІННЯ КОМП'ЮТЕРИЗОВАНИХ СИСТЕМ
	'63'  => array(
				'name' => 'A417',
				'info' => array(
					'1' => array(
						'post'     => 'B418',
						'category' => 'C418',
						'count'    => 'D418',
					),
					'2' => array(
						'post'     => 'B419',
						'category' => 'C419',
						'count'    => 'D419',
					),
					'3' => array(
						'post'     => 'B420',
						'category' => 'C420',
						'count'    => 'D420',
					),
					'4' => array(
						'post'     => 'B421',
						'category' => 'C421',
						'count'    => 'D421',
					),
					'5' => array(
						'post'     => 'B422',
						'category' => 'C422',
						'count'    => 'D422',
					),
				),
			),  

    // Відділ розвитку інформаційних технологій і впровадження
	'64'  => array(
				'name' => 'A425',
				'info' => array(
					'1' => array(
						'post'     => 'B426',
						'category' => 'C426',
						'count'    => 'D426',
					),
					'2' => array(
						'post'     => 'B427',
						'category' => 'C427',
						'count'    => 'D427',
					),
				),
			),  

    // Сектор супроводження баз даних "Законопроект"
	'65'  => array(
				'name' => 'A430',
				'info' => array(
					'1' => array(
						'post'     => 'B431',
						'category' => 'C431',
						'count'    => 'D431',
					),
					'2' => array(
						'post'     => 'B432',
						'category' => 'C432',
						'count'    => 'D432',
					),
				),
			),  

    // Сектор супроводження прикладного програмного забезпечення
	'66'  => array(
				'name' => 'A435',
				'info' => array(
					'1' => array(
						'post'     => 'B436',
						'category' => 'C436',
						'count'    => 'D436',
					),
					'2' => array(
						'post'     => 'B437',
						'category' => 'C437',
						'count'    => 'D437',
					),
					'3' => array(
						'post'     => 'B438',
						'category' => 'C438',
						'count'    => 'D438',
					),
				),
			),  

    // Відділ досліджень та перспективних розробок
	'67'  => array(
				'name' => 'A442',
				'info' => array(
					'1' => array(
						'post'     => 'B443',
						'category' => 'C443',
						'count'    => 'D443',
					),
					'2' => array(
						'post'     => 'B444',
						'category' => 'C444',
						'count'    => 'D444',
					),
					'3' => array(
						'post'     => 'B445',
						'category' => 'C445',
						'count'    => 'D445',
					),
				),
			),  

    // Сектор адміністрування комп'ютерних мереж і передачі даних
	'68'  => array(
				'name' => 'A449',
				'info' => array(
					'1' => array(
						'post'     => 'B450',
						'category' => 'C450',
						'count'    => 'D450',
					),
					'2' => array(
						'post'     => 'B451',
						'category' => 'C451',
						'count'    => 'D451',
					),
					'3' => array(
						'post'     => 'B452',
						'category' => 'C452',
						'count'    => 'D452',
					),
				),
			),  

    // Сектор захисту інформації в автоматизованих системах
	'69'  => array(
				'name' => 'A455',
				'info' => array(
					'1' => array(
						'post'     => 'B456',
						'category' => 'C456',
						'count'    => 'D456',
					),
					'2' => array(
						'post'     => 'B457',
						'category' => 'C457',
						'count'    => 'D457',
					),
					'3' => array(
						'post'     => 'B458',
						'category' => 'C458',
						'count'    => 'D458',
					),
				),
			),  

    // Відділ баз даних нормативно-правової інформації
	'70'  => array(
				'name' => 'A460',
				'info' => array(
					'1' => array(
						'post'     => 'B461',
						'category' => 'C461',
						'count'    => 'D461',
					),
					'2' => array(
						'post'     => 'B462',
						'category' => 'C462',
						'count'    => 'D462',
					),
					'3' => array(
						'post'     => 'B463',
						'category' => 'C463',
						'count'    => 'D463',
					),
				),
			),  

    // Сектор розвитку та формування баз даних нормативно-правової інформації
	'71'  => array(
				'name' => 'A466',
				'info' => array(
					'1' => array(
						'post'     => 'B467',
						'category' => 'C467',
						'count'    => 'D467',
					),
					'2' => array(
						'post'     => 'B468',
						'category' => 'C468',
						'count'    => 'D468',
					),
				),
			),  

    // Сектор розробки програмного забезпечення та зв'язків з користувачами
	'72'  => array(
				'name' => 'A471',
				'info' => array(
					'1' => array(
						'post'     => 'B472',
						'category' => 'C472',
						'count'    => 'D472',
					),
					'2' => array(
						'post'     => 'B473',
						'category' => 'C473',
						'count'    => 'D473',
					),
					'3' => array(
						'post'     => 'B474',
						'category' => 'C474',
						'count'    => 'D474',
					),
				),
			),  

    // Сектор збору, класифікації та підготовки документів до введення у бази даних
	'73'  => array(
				'name' => 'A477',
				'info' => array(
					'1' => array(
						'post'     => 'B478',
						'category' => 'C478',
						'count'    => 'D478',
					),
					'2' => array(
						'post'     => 'B479',
						'category' => 'C479',
						'count'    => 'D479',
					),
					'3' => array(
						'post'     => 'B480',
						'category' => 'C480',
						'count'    => 'D480',
					),
				),
			),  

    // Системно-технологічний відділ  
	'74'  => array(
				'name' => 'A484',
				'info' => array(
					'1' => array(
						'post'     => 'B485',
						'category' => 'C485',
						'count'    => 'D485',
					),
					'2' => array(
						'post'     => 'B486',
						'category' => 'C486',
						'count'    => 'D486',
					),
					'3' => array(
						'post'     => 'B487',
						'category' => 'C487',
						'count'    => 'D487',
					),
					'4' => array(
						'post'     => 'B488',
						'category' => 'C488',
						'count'    => 'D488',
					),
				),
			),  

    // Відділ спеціалізованих інформаційно-технічних комплексів
	'75'  => array(
				'name' => 'A491',
				'info' => array(
					'1' => array(
						'post'     => 'B492',
						'category' => 'C492',
						'count'    => 'D492',
					),
					'2' => array(
						'post'     => 'B493',
						'category' => 'C493',
						'count'    => 'D493',
					),
					'3' => array(
						'post'     => 'B494',
						'category' => 'C494',
						'count'    => 'D494',
					),
				),
			),  

    // УПРАВЛІННЯ ПО ЗВ'ЯЗКАХ З МІСЦЕВИМИ ОРГАНАМИ ВЛАДИ І ОРГАНАМИ МІСЦЕВОГО САМОВРЯДУВАННЯ
	'76'  => array(
				'name' => 'A498',
				'info' => array(
					'1' => array(
						'post'     => 'B499',
						'category' => 'C499',
						'count'    => 'D499',
					),
					'2' => array(
						'post'     => 'B500',
						'category' => 'C500',
						'count'    => 'D500',
					),
					'3' => array(
						'post'     => 'B501',
						'category' => 'C501',
						'count'    => 'D501',
					),
				),
			),  

    // Відділ організаційної роботи з регіонами
	'77'  => array(
				'name' => 'A504',
				'info' => array(
					'1' => array(
						'post'     => 'B505',
						'category' => 'C505',
						'count'    => 'D505',
					),
					'2' => array(
						'post'     => 'B506',
						'category' => 'C506',
						'count'    => 'D506',
					),
					'3' => array(
						'post'     => 'B507',
						'category' => 'C507',
						'count'    => 'D507',
					),
				),
			),  

    // Сектор з питань методичної роботи
	'78'  => array(
				'name' => 'A510',
				'info' => array(
					'1' => array(
						'post'     => 'B511',
						'category' => 'C511',
						'count'    => 'D511',
					),
					'2' => array(
						'post'     => 'B512',
						'category' => 'C512',
						'count'    => 'D512',
					),
					'3' => array(
						'post'     => 'B513',
						'category' => 'C513',
						'count'    => 'D513',
					),
				),
			),  

    // Сектор з питань адміністративно-територіального устрою та виборчих систем 
	'79'  => array(
				'name' => 'A516',
				'info' => array(
					'1' => array(
						'post'     => 'B517',
						'category' => 'C517',
						'count'    => 'D517',
					),
					'2' => array(
						'post'     => 'B518',
						'category' => 'C518',
						'count'    => 'D518',
					),
				),
			),  

    // УПРАВЛІННЯ ЗАБЕЗПЕЧЕННЯ МІЖПАРЛАМЕНТСЬКИХ ЗВ'ЯЗКІВ
	'80'  => array(
				'name' => 'A523',
				'info' => array(
					'1' => array(
						'post'     => 'B524',
						'category' => 'C524',
						'count'    => 'D524',
					),
					'2' => array(
						'post'     => 'B525',
						'category' => 'C525',
						'count'    => 'D525',
					),
					'3' => array(
						'post'     => 'B526',
						'category' => 'C526',
						'count'    => 'D526',
					),
					'4' => array(
						'post'     => 'B527',
						'category' => 'C527',
						'count'    => 'D527',
					),
				),
			),  

    // Відділ організаційно-протокольного забезпечення
	'81'  => array(
				'name' => 'A530',
				'info' => array(
					'1' => array(
						'post'     => 'B531',
						'category' => 'C531',
						'count'    => 'D531',
					),
					'2' => array(
						'post'     => 'B532',
						'category' => 'C532',
						'count'    => 'D532',
					),
					'3' => array(
						'post'     => 'B533',
						'category' => 'C533',
						'count'    => 'D533',
					),
					'4' => array(
						'post'     => 'B534',
						'category' => 'C534',
						'count'    => 'D534',
					),
					'5' => array(
						'post'     => 'B535',
						'category' => 'C535',
						'count'    => 'D535',
					),
				),
			),  

    // Відділ забезпечення зв'язків з національними парламентами
	'82'  => array(
				'name' => 'A538',
				'info' => array(
					'1' => array(
						'post'     => 'B539',
						'category' => 'C539',
						'count'    => 'D539',
					),
					'2' => array(
						'post'     => 'B540',
						'category' => 'C540',
						'count'    => 'D540',
					),
					'3' => array(
						'post'     => 'B541',
						'category' => 'C541',
						'count'    => 'D541',
					),
					'4' => array(
						'post'     => 'B542',
						'category' => 'C542',
						'count'    => 'D542',
					),
				),
			),  

    // Відділ забезпечення зв'язків з міжнародними парламентськими організаціями
	'83'  => array(
				'name' => 'A545',
				'info' => array(
					'1' => array(
						'post'     => 'B546',
						'category' => 'C546',
						'count'    => 'D546',
					),
					'2' => array(
						'post'     => 'B547',
						'category' => 'C547',
						'count'    => 'D547',
					),
					'3' => array(
						'post'     => 'B548',
						'category' => 'C548',
						'count'    => 'D548',
					),
					'4' => array(
						'post'     => 'B549',
						'category' => 'C549',
						'count'    => 'D549',
					),
				),
			),  

    // УПРАВЛІННЯ КАДРІВ
	'84'  => array(
				'name' => 'A553',
				'info' => array(
					'1' => array(
						'post'     => 'B554',
						'category' => 'C554',
						'count'    => 'D554',
					),
					'2' => array(
						'post'     => 'B555',
						'category' => 'C555',
						'count'    => 'D555',
					),
				),
			),  

    // Відділ кадрового забезпечення народних депутатів України 
	'85'  => array(
				'name' => 'A558',
				'info' => array(
					'1' => array(
						'post'     => 'B559',
						'category' => 'C559',
						'count'    => 'D559',
					),
					'2' => array(
						'post'     => 'B560',
						'category' => 'C560',
						'count'    => 'D560',
					),
				),
			),  

    // Відділ  кадрового забезпечення помічників-консультантів народних депутатів України 
	'86'  => array(
				'name' => 'A564',
				'info' => array(
					'1' => array(
						'post'     => 'B565',
						'category' => 'C565',
						'count'    => 'D565',
					),
					'2' => array(
						'post'     => 'B566',
						'category' => 'C566',
						'count'    => 'D566',
					),
					'3' => array(
						'post'     => 'B567',
						'category' => 'C567',
						'count'    => 'D567',
					),
				),
			),  

    // Відділ кадрового забезпечення працівників Апарату Верховної Ради України
	'87'  => array(
				'name' => 'A570',
				'info' => array(
					'1' => array(
						'post'     => 'B571',
						'category' => 'C571',
						'count'    => 'D571',
					),
					'2' => array(
						'post'     => 'B572',
						'category' => 'C572',
						'count'    => 'D572',
					),
				),
			),  

    // Сектор з питань державної служби та підвищення кваліфікації працівників
	'88'  => array(
				'name' => 'A575',
				'info' => array(
					'1' => array(
						'post'     => 'B576',
						'category' => 'C576',
						'count'    => 'D576',
					),
					'2' => array(
						'post'     => 'B577',
						'category' => 'C577',
						'count'    => 'D577',
					),
				),
			),  

    // Сектор з питань нагородження
	'89'  => array(
				'name' => 'A581',
				'info' => array(
					'1' => array(
						'post'     => 'B582',
						'category' => 'C582',
						'count'    => 'D582',
					),
					'2' => array(
						'post'     => 'B583',
						'category' => 'C583',
						'count'    => 'D583',
					),
					'3' => array(
						'post'     => 'B584',
						'category' => 'C584',
						'count'    => 'D584',
					),
				),
			),  

    // Сектор реєстрації та опрацювання документів
	'90'  => array(
				'name' => 'A587',
				'info' => array(
					'1' => array(
						'post'     => 'B588',
						'category' => 'C588',
						'count'    => 'D588',
					),
					'2' => array(
						'post'     => 'B589',
						'category' => 'C589',
						'count'    => 'D589',
					),
					'3' => array(
						'post'     => 'B590',
						'category' => 'C590',
						'count'    => 'D590',
					),
					'4' => array(
						'post'     => 'B591',
						'category' => 'C591',
						'count'    => 'D591',
					),
				),
			),  

    // ВІДДІЛ ЗВ'ЯЗКІВ З ОРГАНАМИ ПРАВОСУДДЯ
	'91'  => array(
				'name' => 'A595',
				'info' => array(
					'1' => array(
						'post'     => 'B596',
						'category' => 'C596',
						'count'    => 'D596',
					),
					'2' => array(
						'post'     => 'B597',
						'category' => 'C597',
						'count'    => 'D597',
					),
					'3' => array(
						'post'     => 'B598',
						'category' => 'C598',
						'count'    => 'D598',
					),
					'4' => array(
						'post'     => 'B600',
						'category' => 'C600',
						'count'    => 'D600',
					),
				),
			),  

    // ВІДДІЛ КОНТРОЛЮ
	'92'  => array(
				'name' => 'A604',
				'info' => array(
					'1' => array(
						'post'     => 'B605',
						'category' => 'C605',
						'count'    => 'D605',
					),
					'2' => array(
						'post'     => 'B606',
						'category' => 'C606',
						'count'    => 'D606',
					),
					'3' => array(
						'post'     => 'B607',
						'category' => 'C607',
						'count'    => 'D607',
					),
					'4' => array(
						'post'     => 'B608',
						'category' => 'C608',
						'count'    => 'D608',
					),
				),
			),  

    // ВІДДІЛ З ПИТАНЬ ЗВЕРНЕНЬ ГРОМАДЯН
	'93'  => array(
				'name' => 'A611',
				'info' => array(
					'1' => array(
						'post'     => 'B612',
						'category' => 'C612',
						'count'    => 'D612',
					),
					'2' => array(
						'post'     => 'B613',
						'category' => 'C613',
						'count'    => 'D613',
					),
				),
			),  

    // Сектор прийому громадян
	'94'  => array(
				'name' => 'A616',
				'info' => array(
					'1' => array(
						'post'     => 'B617',
						'category' => 'C617',
						'count'    => 'D617',
					),
					'2' => array(
						'post'     => 'B618',
						'category' => 'C618',
						'count'    => 'D618',
					),
				),
			),  

    // Сектор розгляду листів
	'95'  => array(
				'name' => 'A621',
				'info' => array(
					'1' => array(
						'post'     => 'B622',
						'category' => 'C622',
						'count'    => 'D622',
					),
					'2' => array(
						'post'     => 'B623',
						'category' => 'C623',
						'count'    => 'D623',
					),
				),
			),  

    // Сектор обліку, аналізу та узагальнення звернень громадян
	'96'  => array(
				'name' => 'A626',
				'info' => array(
					'1' => array(
						'post'     => 'B627',
						'category' => 'C627',
						'count'    => 'D627',
					),
					'2' => array(
						'post'     => 'B628',
						'category' => 'C628',
						'count'    => 'D628',
					),
					'3' => array(
						'post'     => 'B629',
						'category' => 'C629',
						'count'    => 'D629',
					),
				),
			),  

    // Сектор контролю за зверненнями громадян
	'97'  => array(
				'name' => 'A632',
				'info' => array(
					'1' => array(
						'post'     => 'B633',
						'category' => 'C633',
						'count'    => 'D633',
					),
					'2' => array(
						'post'     => 'B634',
						'category' => 'C634',
						'count'    => 'D634',
					),
					'3' => array(
						'post'     => 'B635',
						'category' => 'C635',
						'count'    => 'D635',
					),
					'4' => array(
						'post'     => 'B636',
						'category' => 'C636',
						'count'    => 'D636',
					),
				),
			),  

    // ПЕРШИЙ ВІДДІЛ 
	'98'  => array(
				'name' => 'A640',
				'info' => array(
					'1' => array(
						'post'     => 'B641',
						'category' => 'C641',
						'count'    => 'D641',
					),
					'2' => array(
						'post'     => 'B643',
						'category' => 'C643',
						'count'    => 'D643',
					),
					'3' => array(
						'post'     => 'B644',
						'category' => 'C644',
						'count'    => 'D644',
					),
				),
			),  

    // ПРЕС-СЛУЖБА
	'99'  => array(
				'name' => 'A647',
				'info' => array(
					'1' => array(
						'post'     => 'B648',
						'category' => 'C648',
						'count'    => 'D648',
					),
					'2' => array(
						'post'     => 'B649',
						'category' => 'C649',
						'count'    => 'D649',
					),
					'3' => array(
						'post'     => 'B650',
						'category' => 'C650',
						'count'    => 'D650',
					),
				),
			),  

    // СЕКТОР МОБІЛІЗАЦІЙНОЇ РОБОТИ
	'100'  => array(
				'name' => 'A653',
				'info' => array(
					'1' => array(
						'post'     => 'B654',
						'category' => 'C654',
						'count'    => 'D654',
					),
					'2' => array(
						'post'     => 'B655',
						'category' => 'C655',
						'count'    => 'D655',
					),
				),
			),  

    // УПРАВЛІННЯ СПРАВАМИ
	// Керівництво Управління справами
	'101'  => array(
				'name' => 'A659',
				'info' => array(
					'1' => array(
						'post'     => 'B660',
						'category' => 'C660',
						'count'    => 'D660',
					),
					'2' => array(
						'post'     => 'B661',
						'category' => 'C661',
						'count'    => 'D661',
					),
					'3' => array(
						'post'     => 'B662',
						'category' => 'C662',
						'count'    => 'D662',
					),
					'4' => array(
						'post'     => 'B663',
						'category' => 'C663',
						'count'    => 'D663',
					),
				),
			),

    // Відділ бухгалтерського обліку, фінансів та планування 
	'102'  => array(
				'name' => 'A666',
				'info' => array(
					'1' => array(
						'post'     => 'B667',
						'category' => 'C667',
						'count'    => 'D667',
					),
					'2' => array(
						'post'     => 'B668',
						'category' => 'C668',
						'count'    => 'D668',
					),
				),
			),

    // Сектор автоматизації бухгалтерського обліку і звітності
	'103'  => array(
				'name' => 'A672',
				'info' => array(
					'1' => array(
						'post'     => 'B673',
						'category' => 'C673',
						'count'    => 'D673',
					),
					'2' => array(
						'post'     => 'B674',
						'category' => 'C674',
						'count'    => 'D674',
					),
				),
			),

    // Сектор розрахунків з народними депутатами України
	'104'  => array(
				'name' => 'A677',
				'info' => array(
					'1' => array(
						'post'     => 'B678',
						'category' => 'C678',
						'count'    => 'D678',
					),
					'2' => array(
						'post'     => 'B679',
						'category' => 'C679',
						'count'    => 'D679',
					),
				),
			),

    // Сектор розрахунків з помічниками-консультантами народних депутатів України
	'105'  => array(
				'name' => 'A682',
				'info' => array(
					'1' => array(
						'post'     => 'B683',
						'category' => 'C683',
						'count'    => 'D683',
					),
					'2' => array(
						'post'     => 'B684',
						'category' => 'C684',
						'count'    => 'D684',
					),
				),
			),

    // Сектор розрахунків з працівниками Апарату
	'106'  => array(
				'name' => 'A687',
				'info' => array(
					'1' => array(
						'post'     => 'B688',
						'category' => 'C688',
						'count'    => 'D688',
					),
					'2' => array(
						'post'     => 'B689',
						'category' => 'C689',
						'count'    => 'D689',
					),
				),
			),

    // Сектор розрахунків з установами і організаціями
	'107'  => array(
				'name' => 'A692',
				'info' => array(
					'1' => array(
						'post'     => 'B693',
						'category' => 'C693',
						'count'    => 'D693',
					),
					'2' => array(
						'post'     => 'B694',
						'category' => 'C694',
						'count'    => 'D694',
					),
				),
			),

    // Сектор матеріального обліку
	'108'  => array(
				'name' => 'A697',
				'info' => array(
					'1' => array(
						'post'     => 'B698',
						'category' => 'C698',
						'count'    => 'D698',
					),
					'2' => array(
						'post'     => 'B699',
						'category' => 'C699',
						'count'    => 'D699',
					),
				),
			),

    // Сектор планування, фінансів та аналізу
	'109'  => array(
				'name' => 'A702',
				'info' => array(
					'1' => array(
						'post'     => 'B703',
						'category' => 'C703',
						'count'    => 'D703',
					),
					'2' => array(
						'post'     => 'B704',
						'category' => 'C704',
						'count'    => 'D704',
					),
				),
			),

    // Загальний відділ 
	'110'  => array(
				'name' => 'A713',
				'info' => array(
					'1' => array(
						'post'     => 'B714',
						'category' => 'C714',
						'count'    => 'D714',
					),
					'2' => array(
						'post'     => 'B715',
						'category' => 'C715',
						'count'    => 'D715',
					),
					'3' => array(
						'post'     => 'B716',
						'category' => 'C716',
						'count'    => 'D716',
					),
					'4' => array(
						'post'     => 'B717',
						'category' => 'C717',
						'count'    => 'D717',
					),
				),
			),

    // Сектор реєстрації та відправлення кореспонденції
	'111'  => array(
				'name' => 'A720',
				'info' => array(
					'1' => array(
						'post'     => 'B721',
						'category' => 'C721',
						'count'    => 'D721',
					),
					'2' => array(
						'post'     => 'B722',
						'category' => 'C722',
						'count'    => 'D722',
					),
				),
			),

    // Відділ медичного, санаторного та побутового забезпечення
	'112'  => array(
				'name' => 'A726',
				'info' => array(
					'1' => array(
						'post'     => 'B727',
						'category' => 'C727',
						'count'    => 'D727',
					),
					'2' => array(
						'post'     => 'B729',
						'category' => 'C729',
						'count'    => 'D729',
					),
				),
			),			

    // Відділ господарського забезпечення
	'113'  => array(
				'name' => 'A733',
				'info' => array(
					'1' => array(
						'post'     => 'B734',
						'category' => 'C734',
						'count'    => 'D734',
					),
					'2' => array(
						'post'     => 'B735',
						'category' => 'C735',
						'count'    => 'D735',
					),
				),
			),  

    // Сектор обслуговування протокольних заходів
	'114'  => array(
				'name' => 'A738',
				'info' => array(
					'1' => array(
						'post'     => 'B739',
						'category' => 'C739',
						'count'    => 'D739',
					),
					'2' => array(
						'post'     => 'B740',
						'category' => 'C740',
						'count'    => 'D740',
					),
				),
			),  

    // Сектор господарського обслуговування
	'115'  => array(
				'name' => 'A743',
				'info' => array(
					'1' => array(
						'post'     => 'B744',
						'category' => 'C744',
						'count'    => 'D744',
					),
					'2' => array(
						'post'     => 'B745',
						'category' => 'C745',
						'count'    => 'D745',
					),
					'3' => array(
						'post'     => 'B746',
						'category' => 'C746',
						'count'    => 'D746',
					),
				),
			),  

    // Сектор забезпечення заходів
	'116'  => array(
				'name' => 'A749',
				'info' => array(
					'1' => array(
						'post'     => 'B750',
						'category' => 'C750',
						'count'    => 'D750',
					),
					'2' => array(
						'post'     => 'B751',
						'category' => 'C751',
						'count'    => 'D751',
					),
				),
			),  

    // Відділ будівництва та ремонту
	'117'  => array(
				'name' => 'A756',
				'info' => array(
					'1' => array(
						'post'     => 'B757',
						'category' => 'C757',
						'count'    => 'D757',
					),
					'2' => array(
						'post'     => 'B758',
						'category' => 'C758',
						'count'    => 'D758',
					),
				),
			),  

    // Відділ зв'язку і телерадіосистем
	'118'  => array(
				'name' => 'A760',
				'info' => array(
					'1' => array(
						'post'     => 'B761',
						'category' => 'C761',
						'count'    => 'D761',
					),
					'2' => array(
						'post'     => 'B762',
						'category' => 'C762',
						'count'    => 'D762',
					),
				),
			),  

    // Сектор систем звукопідсилення і звукозапису
	'119'  => array(
				'name' => 'A765',
				'info' => array(
					'1' => array(
						'post'     => 'B766',
						'category' => 'C766',
						'count'    => 'D766',
					),
					'2' => array(
						'post'     => 'B767',
						'category' => 'C767',
						'count'    => 'D767',
					),
				),
			),  

    // Сектор систем телебачення
	'120'  => array(
				'name' => 'A770',
				'info' => array(
					'1' => array(
						'post'     => 'B771',
						'category' => 'C771',
						'count'    => 'D771',
					),
					'2' => array(
						'post'     => 'B772',
						'category' => 'C772',
						'count'    => 'D772',
					),
				),
			),  

    // Сектор  систем зв'язку
	'121'  => array(
				'name' => 'A775',
				'info' => array(
					'1' => array(
						'post'     => 'B776',
						'category' => 'C776',
						'count'    => 'D776',
					),
					'2' => array(
						'post'     => 'B777',
						'category' => 'C777',
						'count'    => 'D777',
					),
				),
			),  

    // Довідково-інформаційний сектор
	'122'  => array(
				'name' => 'A780',
				'info' => array(
					'1' => array(
						'post'     => 'B781',
						'category' => 'C781',
						'count'    => 'D781',
					),
					'2' => array(
						'post'     => 'B782',
						'category' => 'C782',
						'count'    => 'D782',
					),
				),
			),  

    // Сектор технічного захисту інформації
	'123'  => array(
				'name' => 'A785',
				'info' => array(
					'1' => array(
						'post'     => 'B786',
						'category' => 'C786',
						'count'    => 'D786',
					),
					'2' => array(
						'post'     => 'B787',
						'category' => 'C787',
						'count'    => 'D787',
					),
				),
			),  

    // Відділ матеріально-технічного постачання
	'124'  => array(
				'name' => 'A792',
				'info' => array(
					'1' => array(
						'post'     => 'B793',
						'category' => 'C793',
						'count'    => 'D793',
					),
					'2' => array(
						'post'     => 'B794',
						'category' => 'C794',
						'count'    => 'D794',
					),
				),
			),  

    // Сектор забезпечення оргтехнікою та культтоварами
	'125'  => array(
				'name' => 'A797',
				'info' => array(
					'1' => array(
						'post'     => 'B798',
						'category' => 'C798',
						'count'    => 'D798',
					),
					'2' => array(
						'post'     => 'B799',
						'category' => 'C799',
						'count'    => 'D799',
					),
					'3' => array(
						'post'     => 'B800',
						'category' => 'C800',
						'count'    => 'D800',
					),
				),
			),  

    // Сектор господарського забезпечення 
	'126'  => array(
				'name' => 'A803',
				'info' => array(
					'1' => array(
						'post'     => 'B804',
						'category' => 'C804',
						'count'    => 'D804',
					),
					'2' => array(
						'post'     => 'B805',
						'category' => 'C805',
						'count'    => 'D805',
					),
					'3' => array(
						'post'     => 'B806',
						'category' => 'C806',
						'count'    => 'D806',
					),
				),
			),  

    // Відділ контролю та аудиту
	'127'  => array(
				'name' => 'A810',
				'info' => array(
					'1' => array(
						'post'     => 'B811',
						'category' => 'C811',
						'count'    => 'D811',
					),
					'2' => array(
						'post'     => 'B812',
						'category' => 'C812',
						'count'    => 'D812',
					),
					'3' => array(
						'post'     => 'B813',
						'category' => 'C813',
						'count'    => 'D813',
					),
				),
			),  

    // Відділ юридичного забезпечення
	'128'  => array(
				'name' => 'A816',
				'info' => array(
					'1' => array(
						'post'     => 'B817',
						'category' => 'C817',
						'count'    => 'D817',
					),
					'2' => array(
						'post'     => 'B818',
						'category' => 'C818',
						'count'    => 'D818',
					),
					'3' => array(
						'post'     => 'B819',
						'category' => 'C819',
						'count'    => 'D819',
					),
				),
			),  
);

?>