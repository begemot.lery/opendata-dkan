<?php

//************************************//
//*  Апарат(РГВРУ №164)              *//
//************************************//

$array_xls_sheet_4 = array(
	
	// СЕКРЕТАРІАТ ГОЛОВИ ВЕРХОВНОЇ РАДИ УКРАЇНИ
	'1'  => array(
				'name' => 'A16',
				'info' => array(
					'1' => array(
						'post'     => 'B17',
						'category' => 'C17',
						'count'    => 'D17',
					),
					'2' => array(
						'post'     => 'B18',
						'category' => 'C18',
						'count'    => 'D18',
					),
					'3' => array(
						'post'     => 'B19',
						'category' => 'C19',
						'count'    => 'D19',
					),
				),
			),  
			
    // Група радників, помічників Голови Верховної Ради України  
	'2'  => array(
				'name' => 'A21',
				'info' => array(
					'1' => array(
						'post'     => 'B22',
						'category' => 'C22',
						'count'    => 'D22',
					),
				),
			),  

    // Відділ документального забезпечення Голови Верховної Ради України  
	'3'  => array(
				'name' => 'A24',
				'info' => array(
					'1' => array(
						'post'     => 'B25',
						'category' => 'C25',
						'count'    => 'D25',
					),
					'2' => array(
						'post'     => 'B26',
						'category' => 'C26',
						'count'    => 'D26',
					),
					'3' => array(
						'post'     => 'B27',
						'category' => 'C27',
						'count'    => 'D27',
					),
				),
			),  

    // Приймальня Голови Верховної Ради України 
	'4'  => array(
				'name' => 'A31',
				'info' => array(
					'1' => array(
						'post'     => 'B32',
						'category' => 'C32',
						'count'    => 'D32',
					),
				),
			),  

    // СЕКРЕТАРІАТ ПЕРШОГО ЗАСТУПНИКА ГОЛОВИ ВЕРХОВНОЇ РАДИ УКРАЇНИ 
	'5'  => array(
				'name' => 'A36',
				'info' => array(
					'1' => array(
						'post'     => 'B37',
						'category' => 'C37',
						'count'    => 'D37',
					),
					'2' => array(
						'post'     => 'B38',
						'category' => 'C38',
						'count'    => 'D38',
					),
					'3' => array(
						'post'     => 'B40',
						'category' => 'C40',
						'count'    => 'D40',
					),
					'4' => array(
						'post'     => 'B41',
						'category' => 'C41',
						'count'    => 'D41',
					),
				),
			),  

    // СЕКРЕТАРІАТ ЗАСТУПНИКА ГОЛОВИ ВЕРХОВНОЇ РАДИ  УКРАЇНИ
	'6'  => array(
				'name' => 'A44',
				'info' => array(
					'1' => array(
						'post'     => 'B45',
						'category' => 'C45',
						'count'    => 'D45',
					),
					'2' => array(
						'post'     => 'B46',
						'category' => 'C46',
						'count'    => 'D46',
					),
					'3' => array(
						'post'     => 'B47',
						'category' => 'C47',
						'count'    => 'D47',
					),
					'4' => array(
						'post'     => 'B48',
						'category' => 'C48',
						'count'    => 'D48',
					),
					'5' => array(
						'post'     => 'B49',
						'category' => 'C49',
						'count'    => 'D49',
					),
					'6' => array(
						'post'     => 'B50',
						'category' => 'C50',
						'count'    => 'D50',
					),
				),
			),  

    // ??????????????
	'7'  => array(
				'name' => 'A53',
				'info' => array(
					'1' => array(
						'post'     => 'B54',
						'category' => 'C54',
						'count'    => 'D54',
					),
					'2' => array(
						'post'     => 'B55',
						'category' => 'C55',
						'count'    => 'D55',
					),
					'3' => array(
						'post'     => 'B56',
						'category' => 'C56',
						'count'    => 'D56',
					),
					'4' => array(
						'post'     => 'B58',
						'category' => 'C58',
						'count'    => 'D58',
					),
				),
			),  

    // ГОЛОВНЕ НАУКОВО-ЕКСПЕРТНЕ УПРАВЛІННЯ 
	'8'  => array(
				'name' => 'A61',
				'info' => array(
					'1' => array(
						'post'     => 'B62',
						'category' => 'C62',
						'count'    => 'D62',
					),
					'2' => array(
						'post'     => 'B63',
						'category' => 'C63',
						'count'    => 'D63',
					),
					'3' => array(
						'post'     => 'B64',
						'category' => 'C64',
						'count'    => 'D64',
					),
					'4' => array(
						'post'     => 'B65',
						'category' => 'C65',
						'count'    => 'D65',
					),
					'5' => array(
						'post'     => 'B66',
						'category' => 'C66',
						'count'    => 'D66',
					),
				),
			),  

    // Відділ з конституційних питань та державного будівництва 
	'9'  => array(
				'name' => 'A70',
				'info' => array(
					'1' => array(
						'post'     => 'B71',
						'category' => 'C71',
						'count'    => 'D71',
					),
					'2' => array(
						'post'     => 'B72',
						'category' => 'C72',
						'count'    => 'D72',
					),
				),
			),  

    // Відділ з гуманітарних і міжнародних питань, гармонізації законодавства з країнами ЄС та захисту прав людини
	'10'  => array(
				'name' => 'A75',
				'info' => array(
					'1' => array(
						'post'     => 'B76',
						'category' => 'C76',
						'count'    => 'D76',
					),
					'2' => array(
						'post'     => 'B77',
						'category' => 'C77',
						'count'    => 'D77',
					),
					'3' => array(
						'post'     => 'B78',
						'category' => 'C78',
						'count'    => 'D78',
					),
				),
			),  

    // Відділ з соціальних питань та праці
	'11'  => array(
				'name' => 'A81',
				'info' => array(
					'1' => array(
						'post'     => 'B82',
						'category' => 'C82',
						'count'    => 'D82',
					),
					'2' => array(
						'post'     => 'B83',
						'category' => 'C83',
						'count'    => 'D83',
					),
					'3' => array(
						'post'     => 'B84',
						'category' => 'C84',
						'count'    => 'D84',
					),
				),
			),  

    // Відділ з питань цивільних і господарських відносин та підприємництва
	'12'  => array(
				'name' => 'A87',
				'info' => array(
					'1' => array(
						'post'     => 'B88',
						'category' => 'C88',
						'count'    => 'D88',
					),
					'2' => array(
						'post'     => 'B89',
						'category' => 'C89',
						'count'    => 'D89',
					),
					'3' => array(
						'post'     => 'B90',
						'category' => 'C90',
						'count'    => 'D90',
					),
				),
			),  

    // Відділ з аграрних і екологічних питань та природокористування
	'13'  => array(
				'name' => 'A94',
				'info' => array(
					'1' => array(
						'post'     => 'B95',
						'category' => 'C95',
						'count'    => 'D95',
					),
					'2' => array(
						'post'     => 'B96',
						'category' => 'C96',
						'count'    => 'D96',
					),
					'3' => array(
						'post'     => 'B97',
						'category' => 'C97',
						'count'    => 'D97',
					),
				),
			),  

    // Відділ з питань бюджету, оподаткування та банківської діяльності
	'14'  => array(
				'name' => 'A100',
				'info' => array(
					'1' => array(
						'post'     => 'B101',
						'category' => 'C101',
						'count'    => 'D101',
					),
					'2' => array(
						'post'     => 'B102',
						'category' => 'C102',
						'count'    => 'D102',
					),
					'3' => array(
						'post'     => 'B103',
						'category' => 'C103',
						'count'    => 'D103',
					),
				),
			),  

    // Відділ з питань макроекономіки та промислового розвитку
	'15'  => array(
				'name' => 'A106',
				'info' => array(
					'1' => array(
						'post'     => 'B107',
						'category' => 'C107',
						'count'    => 'D107',
					),
					'2' => array(
						'post'     => 'B108',
						'category' => 'C108',
						'count'    => 'D108',
					),
				),
			),  

    // Відділ з питань національної безпеки, оборони, правоохоронної діяльності та боротьби із злочинністю
	'16'  => array(
				'name' => 'A111',
				'info' => array(
					'1' => array(
						'post'     => 'B112',
						'category' => 'C112',
						'count'    => 'D112',
					),
					'2' => array(
						'post'     => 'B113',
						'category' => 'C113',
						'count'    => 'D113',
					),
				),
			),  

    // ГОЛОВНЕ ЮРИДИЧНЕ УПРАВЛІННЯ
	'17'  => array(
				'name' => 'A117',
				'info' => array(
					'1' => array(
						'post'     => 'B118',
						'category' => 'C118',
						'count'    => 'D118',
					),
					'2' => array(
						'post'     => 'B119',
						'category' => 'C119',
						'count'    => 'D119',
					),
					'3' => array(
						'post'     => 'B120',
						'category' => 'C120',
						'count'    => 'D120',
					),
					'4' => array(
						'post'     => 'B121',
						'category' => 'C121',
						'count'    => 'D121',
					),
					'5' => array(
						'post'     => 'B122',
						'category' => 'C122',
						'count'    => 'D122',
					),
				),
			),  

    // УПРАВЛІННЯ КОНСТИТУЦІЙНОГО ЗАКОНОДАВСТВА ТА ДЕРЖАВНОГО БУДІВНИЦТВА
	'18'  => array(
				'name' => 'A125',
				'info' => array(
					'1' => array(
						'post'     => 'B126',
						'category' => 'C126',
						'count'    => 'D126',
					),
					'2' => array(
						'post'     => 'B127',
						'category' => 'C127',
						'count'    => 'D127',
					),
				),
			),  

    // Відділ з питань конституційного, кримінального, адміністративного законодавства та судово-правової реформи
	'19'  => array(
				'name' => 'A130',
				'info' => array(
					'1' => array(
						'post'     => 'B131',
						'category' => 'C131',
						'count'    => 'D131',
					),
					'2' => array(
						'post'     => 'B132',
						'category' => 'C132',
						'count'    => 'D132',
					),
					'3' => array(
						'post'     => 'B133',
						'category' => 'C133',
						'count'    => 'D133',
					),
				),
			),  			

    // Відділ з міжнародно-правових питань
	'20'  => array(
				'name' => 'A136',
				'info' => array(
					'1' => array(
						'post'     => 'B137',
						'category' => 'C137',
						'count'    => 'D137',
					),
					'2' => array(
						'post'     => 'B138',
						'category' => 'C138',
						'count'    => 'D138',
					),
					'3' => array(
						'post'     => 'B139',
						'category' => 'C139',
						'count'    => 'D139',
					),
					'4' => array(
						'post'     => 'B140',
						'category' => 'C140',
						'count'    => 'D140',
					),
				),
			),  

    // Відділ обліку та систематизації законодавства
	'21'  => array(
				'name' => 'A143',
				'info' => array(
					'1' => array(
						'post'     => 'B144',
						'category' => 'C144',
						'count'    => 'D144',
					),
					'2' => array(
						'post'     => 'B145',
						'category' => 'C145',
						'count'    => 'D145',
					),
					'3' => array(
						'post'     => 'B146',
						'category' => 'C146',
						'count'    => 'D146',
					),
					'4' => array(
						'post'     => 'B147',
						'category' => 'C147',
						'count'    => 'D147',
					),
				),
			),  

    // УПРАВЛІННЯ ЦИВІЛЬНОГО, ФІНАНСОВОГО ТА СОЦІАЛЬНОГО ЗАКОНОДАВСТВА
	'22'  => array(
				'name' => 'A151',
				'info' => array(
					'1' => array(
						'post'     => 'B152',
						'category' => 'C152',
						'count'    => 'D152',
					),
					'2' => array(
						'post'     => 'B153',
						'category' => 'C153',
						'count'    => 'D153',
					),
				),
			),  

    // Відділ з питань цивільного, податкового, бюджетного законодавства та підприємництва
	'23'  => array(
				'name' => 'A156',
				'info' => array(
					'1' => array(
						'post'     => 'B157',
						'category' => 'C157',
						'count'    => 'D157',
					),
					'2' => array(
						'post'     => 'B158',
						'category' => 'C158',
						'count'    => 'D158',
					),
					'3' => array(
						'post'     => 'B159',
						'category' => 'C159',
						'count'    => 'D159',
					),
					'4' => array(
						'post'     => 'B160',
						'category' => 'C160',
						'count'    => 'D160',
					),
				),
			),  

    // Відділ з питань екологічного, соціального та гуманітарного законодавства
	'24'  => array(
				'name' => 'A163',
				'info' => array(
					'1' => array(
						'post'     => 'B164',
						'category' => 'C164',
						'count'    => 'D164',
					),
					'2' => array(
						'post'     => 'B165',
						'category' => 'C165',
						'count'    => 'D165',
					),
					'3' => array(
						'post'     => 'B166',
						'category' => 'C166',
						'count'    => 'D166',
					),
				),
			),  

    // ГОЛОВНЕ УПРАВЛІННЯ ДОКУМЕНТАЛЬНОГО ЗАБЕЗПЕЧЕННЯ
	'25'  => array(
				'name' => 'A171',
				'info' => array(
					'1' => array(
						'post'     => 'B172',
						'category' => 'C172',
						'count'    => 'D172',
					),
					'2' => array(
						'post'     => 'B173',
						'category' => 'C173',
						'count'    => 'D173',
					),
					'3' => array(
						'post'     => 'B174',
						'category' => 'C174',
						'count'    => 'D174',
					),
				),
			),  

    // Відділ опрацювання та випуску законодавчих актів
	'26'  => array(
				'name' => 'A178',
				'info' => array(
					'1' => array(
						'post'     => 'B179',
						'category' => 'C179',
						'count'    => 'D179',
					),
					'2' => array(
						'post'     => 'B180',
						'category' => 'C180',
						'count'    => 'D180',
					),
				),
			),  

    // Сектор оформлення законодавчих актів
	'27'  => array(
				'name' => 'A183',
				'info' => array(
					'1' => array(
						'post'     => 'B184',
						'category' => 'C184',
						'count'    => 'D184',
					),
					'2' => array(
						'post'     => 'B185',
						'category' => 'C185',
						'count'    => 'D185',
					),
					'3' => array(
						'post'     => 'B186',
						'category' => 'C186',
						'count'    => 'D186',
					),
					'4' => array(
						'post'     => 'B187',
						'category' => 'C187',
						'count'    => 'D187',
					),
				),
			),  

    // Сектор випуску законодавчих актів
	'28'  => array(
				'name' => 'A190',
				'info' => array(
					'1' => array(
						'post'     => 'B191',
						'category' => 'C191',
						'count'    => 'D191',
					),
					'2' => array(
						'post'     => 'B192',
						'category' => 'C192',
						'count'    => 'D192',
					),
					'3' => array(
						'post'     => 'B193',
						'category' => 'C193',
						'count'    => 'D193',
					),
					'4' => array(
						'post'     => 'B194',
						'category' => 'C194',
						'count'    => 'D194',
					),
				),
			),  

    // Відділ організації  діловодства в Апараті
	'29'  => array(
				'name' => 'A198',
				'info' => array(
					'1' => array(
						'post'     => 'B199',
						'category' => 'C199',
						'count'    => 'D199',
					),
					'2' => array(
						'post'     => 'B200',
						'category' => 'C200',
						'count'    => 'D200',
					),
				),
			),  

    // Сектор організації діловодства та оформлення відряджень 
	'30'  => array(
				'name' => 'A203',
				'info' => array(
					'1' => array(
						'post'     => 'B204',
						'category' => 'C204',
						'count'    => 'D204',
					),
					'2' => array(
						'post'     => 'B205',
						'category' => 'C205',
						'count'    => 'D205',
					),
					'3' => array(
						'post'     => 'B206',
						'category' => 'C206',
						'count'    => 'D206',
					),
				),
			),  

    // Сектор з організаційних питань
	'31'  => array(
				'name' => 'A209',
				'info' => array(
					'1' => array(
						'post'     => 'B210',
						'category' => 'C210',
						'count'    => 'D210',
					),
					'2' => array(
						'post'     => 'B211',
						'category' => 'C211',
						'count'    => 'D211',
					),
					'3' => array(
						'post'     => 'B212',
						'category' => 'C212',
						'count'    => 'D212',
					),
				),
			),  

    // Сектор архівного діловодства
	'32'  => array(
				'name' => 'A215',
				'info' => array(
					'1' => array(
						'post'     => 'B216',
						'category' => 'C216',
						'count'    => 'D216',
					),
					'2' => array(
						'post'     => 'B217',
						'category' => 'C217',
						'count'    => 'D217',
					),
					'3' => array(
						'post'     => 'B218',
						'category' => 'C218',
						'count'    => 'D218',
					),
				),
			),  

    // Відділ службової кореспонденції
	'33'  => array(
				'name' => 'A222',
				'info' => array(
					'1' => array(
						'post'     => 'B223',
						'category' => 'C223',
						'count'    => 'D223',
					),
					'2' => array(
						'post'     => 'B224',
						'category' => 'C224',
						'count'    => 'D224',
					),
				),
			),  

    // Сектор прийому і відправлення кореспонденції
	'34'  => array(
				'name' => 'A227',
				'info' => array(
					'1' => array(
						'post'     => 'B228',
						'category' => 'C228',
						'count'    => 'D228',
					),
					'2' => array(
						'post'     => 'B229',
						'category' => 'C229',
						'count'    => 'D229',
					),
					'3' => array(
						'post'     => 'B230',
						'category' => 'C230',
						'count'    => 'D230',
					),
					'4' => array(
						'post'     => 'B231',
						'category' => 'C231',
						'count'    => 'D231',
					),
				),
			),  

    // Сектор аналізу кореспонденції
	'35'  => array(
				'name' => 'A234',
				'info' => array(
					'1' => array(
						'post'     => 'B235',
						'category' => 'C235',
						'count'    => 'D235',
					),
					'2' => array(
						'post'     => 'B236',
						'category' => 'C236',
						'count'    => 'D236',
					),
					'3' => array(
						'post'     => 'B237',
						'category' => 'C237',
						'count'    => 'D237',
					),
				),
			),  

    // Сектор реєстрації кореспонденції
	'36'  => array(
				'name' => 'A240',
				'info' => array(
					'1' => array(
						'post'     => 'B241',
						'category' => 'C241',
						'count'    => 'D241',
					),
					'2' => array(
						'post'     => 'B242',
						'category' => 'C242',
						'count'    => 'D242',
					),
					'3' => array(
						'post'     => 'B243',
						'category' => 'C243',
						'count'    => 'D243',
					),
					'4' => array(
						'post'     => 'B244',
						'category' => 'C244',
						'count'    => 'D244',
					),
				),
			),  

    // УПРАВЛІННЯ АВТОМАТИЗОВАНОЇ ОБРОБКИ ДОКУМЕНТІВ 
	'37'  => array(
				'name' => 'A248',
				'info' => array(
					'1' => array(
						'post'     => 'B249',
						'category' => 'C249',
						'count'    => 'D249',
					),
					'2' => array(
						'post'     => 'B250',
						'category' => 'C250',
						'count'    => 'D250',
					),
					'3' => array(
						'post'     => 'B251',
						'category' => 'C251',
						'count'    => 'D251',
					),
				),
			),  

    // Відділ автоматизованих комплексів
	'38'  => array(
				'name' => 'A254',
				'info' => array(
					'1' => array(
						'post'     => 'B255',
						'category' => 'C255',
						'count'    => 'D255',
					),
				),
			),  

    // Сектор супроводження автоматизованих комплексів та прикладного обладнання
	'39'  => array(
				'name' => 'A257',
				'info' => array(
					'1' => array(
						'post'     => 'B258',
						'category' => 'C258',
						'count'    => 'D258',
					),
					'2' => array(
						'post'     => 'B259',
						'category' => 'C259',
						'count'    => 'D259',
					),
					'3' => array(
						'post'     => 'B260',
						'category' => 'C260',
						'count'    => 'D260',
					),
				),
			),  

    // Сектор автоматизованої обробки законопроектів та супровідних матеріалів
	'40'  => array(
				'name' => 'A263',
				'info' => array(
					'1' => array(
						'post'     => 'B264',
						'category' => 'C264',
						'count'    => 'D264',
					),
					'2' => array(
						'post'     => 'B265',
						'category' => 'C265',
						'count'    => 'D265',
					),
					'3' => array(
						'post'     => 'B266',
						'category' => 'C266',
						'count'    => 'D266',
					),
					'4' => array(
						'post'     => 'B267',
						'category' => 'C267',
						'count'    => 'D267',
					),
					'5' => array(
						'post'     => 'B268',
						'category' => 'C268',
						'count'    => 'D268',
					),
				),
			),  

    // Відділ спеціалізованих інформаційних систем запису і обробки мовної інформації 
	'41'  => array(
				'name' => 'A272',
				'info' => array(
					'1' => array(
						'post'     => 'B273',
						'category' => 'C273',
						'count'    => 'D273',
					),
					'2' => array(
						'post'     => 'B274',
						'category' => 'C274',
						'count'    => 'D274',
					),
				),
			),  

    // Сектор комп'ютерної обробки мовної інформації пленарних засідань
	'42'  => array(
				'name' => 'A277',
				'info' => array(
					'1' => array(
						'post'     => 'B278',
						'category' => 'C278',
						'count'    => 'D278',
					),
					'2' => array(
						'post'     => 'B279',
						'category' => 'C279',
						'count'    => 'D279',
					),
					'3' => array(
						'post'     => 'B280',
						'category' => 'C280',
						'count'    => 'D280',
					),
					'4' => array(
						'post'     => 'B281',
						'category' => 'C281',
						'count'    => 'D281',
					),
					'5' => array(
						'post'     => 'B282',
						'category' => 'C282',
						'count'    => 'D282',
					),
				),
			),  

    // Сектор стенографічного запису і редагування текстових звітів
	'43'  => array(
				'name' => 'A285',
				'info' => array(
					'1' => array(
						'post'     => 'B286',
						'category' => 'C286',
						'count'    => 'D286',
					),
					'2' => array(
						'post'     => 'B287',
						'category' => 'C287',
						'count'    => 'D287',
					),
					'3' => array(
						'post'     => 'B288',
						'category' => 'C288',
						'count'    => 'D288',
					),
				),
			),  

    // Сектор комп'ютерної обробки мовної інформації парламентських заходів
	'44'  => array(
				'name' => 'A291',
				'info' => array(
					'1' => array(
						'post'     => 'B292',
						'category' => 'C292',
						'count'    => 'D292',
					),
					'2' => array(
						'post'     => 'B293',
						'category' => 'C293',
						'count'    => 'D293',
					),
					'3' => array(
						'post'     => 'B294',
						'category' => 'C294',
						'count'    => 'D294',
					),
					'4' => array(
						'post'     => 'B295',
						'category' => 'C295',
						'count'    => 'D295',
					),
					'5' => array(
						'post'     => 'B296',
						'category' => 'C296',
						'count'    => 'D296',
					),
				),
			),  

    // Відділ комп'ютерної обробки законодавчих актів
	'45'  => array(
				'name' => 'A300',
				'info' => array(
					'1' => array(
						'post'     => 'B301',
						'category' => 'C301',
						'count'    => 'D301',
					),
					'2' => array(
						'post'     => 'B302',
						'category' => 'C302',
						'count'    => 'D302',
					),
					'3' => array(
						'post'     => 'B303',
						'category' => 'C303',
						'count'    => 'D303',
					),
					'4' => array(
						'post'     => 'B304',
						'category' => 'C304',
						'count'    => 'D304',
					),
				),
			),  

    // Редакційний відділ
	'46'  => array(
				'name' => 'A308',
				'info' => array(
					'1' => array(
						'post'     => 'B309',
						'category' => 'C309',
						'count'    => 'D309',
					),
					'2' => array(
						'post'     => 'B310',
						'category' => 'C310',
						'count'    => 'D310',
					),
				),
			),  

    // Сектор редагування стенографічних матеріалів
	'47'  => array(
				'name' => 'A313',
				'info' => array(
					'1' => array(
						'post'     => 'B314',
						'category' => 'C314',
						'count'    => 'D314',
					),
					'2' => array(
						'post'     => 'B315',
						'category' => 'C315',
						'count'    => 'D315',
					),
					'3' => array(
						'post'     => 'B316',
						'category' => 'C316',
						'count'    => 'D316',
					),
					'4' => array(
						'post'     => 'B317',
						'category' => 'C317',
						'count'    => 'D317',
					),
				),
			),  

    // Сектор редагування законопроектів
	'48'  => array(
				'name' => 'A320',
				'info' => array(
					'1' => array(
						'post'     => 'B321',
						'category' => 'C321',
						'count'    => 'D321',
					),
					'2' => array(
						'post'     => 'B322',
						'category' => 'C322',
						'count'    => 'D322',
					),
					'3' => array(
						'post'     => 'B323',
						'category' => 'C323',
						'count'    => 'D323',
					),
					'4' => array(
						'post'     => 'B324',
						'category' => 'C324',
						'count'    => 'D324',
					),
				),
			),  

    // Сектор редакційного  опрацювання документів
	'49'  => array(
				'name' => 'A327',
				'info' => array(
					'1' => array(
						'post'     => 'B328',
						'category' => 'C328',
						'count'    => 'D328',
					),
					'2' => array(
						'post'     => 'B329',
						'category' => 'C329',
						'count'    => 'D329',
					),
				),
			),  

    // Сектор автоматизації редакційного процесу
	'50'  => array(
				'name' => 'A332',
				'info' => array(
					'1' => array(
						'post'     => 'B333',
						'category' => 'C333',
						'count'    => 'D333',
					),
					'2' => array(
						'post'     => 'B334',
						'category' => 'C334',
						'count'    => 'D334',
					),
					'3' => array(
						'post'     => 'B335',
						'category' => 'C335',
						'count'    => 'D335',
					),
					'4' => array(
						'post'     => 'B336',
						'category' => 'C336',
						'count'    => 'D336',
					),
				),
			),  

    // ГОЛОВНЕ ОРГАНІЗАЦІЙНЕ УПРАВЛІННЯ
	'51'  => array(
				'name' => 'A341',
				'info' => array(
					'1' => array(
						'post'     => 'B342',
						'category' => 'C342',
						'count'    => 'D342',
					),
					'2' => array(
						'post'     => 'B343',
						'category' => 'C343',
						'count'    => 'D343',
					),
					'3' => array(
						'post'     => 'B344',
						'category' => 'C344',
						'count'    => 'D344',
					),
				),
			),  

    // УПРАВЛІННЯ З ПИТАНЬ ПРОХОДЖЕННЯ ЗАКОНОПРОЕКТІВ ТА РОБОТИ З КОМІТЕТАМИ І ДЕПУТАТСЬКИМИ ФРАКЦІЯМИ
	'52'  => array(
				'name' => 'A347',
				'info' => array(
					'1' => array(
						'post'     => 'B348',
						'category' => 'C348',
						'count'    => 'D348',
					),
				),
			),  

    // Відділ по роботі з комітетами, депутатськими фракціями і групами
	'53'  => array(
				'name' => 'A350',
				'info' => array(
					'1' => array(
						'post'     => 'B351',
						'category' => 'C351',
						'count'    => 'D351',
					),
					'2' => array(
						'post'     => 'B352',
						'category' => 'C352',
						'count'    => 'D352',
					),
					'3' => array(
						'post'     => 'B353',
						'category' => 'C353',
						'count'    => 'D353',
					),
				),
			),  

    // Відділ реєстрації та  обліку проходження законопроектів
	'54'  => array(
				'name' => 'A359',
				'info' => array(
					'1' => array(
						'post'     => 'B360',
						'category' => 'C360',
						'count'    => 'D360',
					),
					'2' => array(
						'post'     => 'B361',
						'category' => 'C361',
						'count'    => 'D361',
					),
					'3' => array(
						'post'     => 'B362',
						'category' => 'C362',
						'count'    => 'D362',
					),
				),
			),  

    // Відділ організаційного забезпечення пленарних засідань Верховної Ради України 
	'55'  => array(
				'name' => 'A367',
				'info' => array(
					'1' => array(
						'post'     => 'B368',
						'category' => 'C368',
						'count'    => 'D368',
					),
				),
			),  

    // Сектор по забезпеченню пленарних засідань Верховної Ради України
	'56'  => array(
				'name' => 'A369',
				'info' => array(
					'1' => array(
						'post'     => 'B370',
						'category' => 'C370',
						'count'    => 'D370',
					),
					'2' => array(
						'post'     => 'B371',
						'category' => 'C371',
						'count'    => 'D371',
					),
					'3' => array(
						'post'     => 'B372',
						'category' => 'C372',
						'count'    => 'D372',
					),
				),
			),  

    // Сектор документального забезпечення та реєстрації учасників пленарних засідань
	'57'  => array(
				'name' => 'A376',
				'info' => array(
					'1' => array(
						'post'     => 'B377',
						'category' => 'C377',
						'count'    => 'D377',
					),
					'2' => array(
						'post'     => 'B378',
						'category' => 'C378',
						'count'    => 'D378',
					),
					'3' => array(
						'post'     => 'B379',
						'category' => 'C379',
						'count'    => 'D379',
					),
					'4' => array(
						'post'     => 'B380',
						'category' => 'C380',
						'count'    => 'D380',
					),
				),
			),  

    // ІНФОРМАЦІЙНЕ УПРАВЛІННЯ
	'58'  => array(
				'name' => 'A385',
				'info' => array(
					'1' => array(
						'post'     => 'B386',
						'category' => 'C386',
						'count'    => 'D386',
					),
					'2' => array(
						'post'     => 'B387',
						'category' => 'C387',
						'count'    => 'D387',
					),
					'3' => array(
						'post'     => 'B389',
						'category' => 'C389',
						'count'    => 'D389',
					),
				),
			),  

    // Відділ висвітлення діяльності Верховної Ради України 
	'59'  => array(
				'name' => 'A392',
				'info' => array(
					'1' => array(
						'post'     => 'B393',
						'category' => 'C393',
						'count'    => 'D393',
					),
					'2' => array(
						'post'     => 'B394',
						'category' => 'C394',
						'count'    => 'D394',
					),
					'3' => array(
						'post'     => 'B395',
						'category' => 'C395',
						'count'    => 'D395',
					),
				),
			),  

    // Відділ моніторингу
	'60'  => array(
				'name' => 'A398',
				'info' => array(
					'1' => array(
						'post'     => 'B399',
						'category' => 'C399',
						'count'    => 'D399',
					),
					'2' => array(
						'post'     => 'B400',
						'category' => 'C400',
						'count'    => 'D400',
					),
				),
			),  

    // Відділ роботи з публічною інформацією
	'61'  => array(
				'name' => 'A403',
				'info' => array(
					'1' => array(
						'post'     => 'B404',
						'category' => 'C404',
						'count'    => 'D404',
					),
					'2' => array(
						'post'     => 'B405',
						'category' => 'C405',
						'count'    => 'D405',
					),
					'3' => array(
						'post'     => 'B406',
						'category' => 'C406',
						'count'    => 'D406',
					),
					'4' => array(
						'post'     => 'B407',
						'category' => 'C407',
						'count'    => 'D407',
					),
				),
			),  

    // Відділ інформаційно-бібліотечного забезпечення
	'62'  => array(
				'name' => 'A410',
				'info' => array(
					'1' => array(
						'post'     => 'B411',
						'category' => 'C411',
						'count'    => 'D411',
					),
					'2' => array(
						'post'     => 'B412',
						'category' => 'C412',
						'count'    => 'D412',
					),
					'3' => array(
						'post'     => 'B413',
						'category' => 'C413',
						'count'    => 'D413',
					),
				),
			),  

    // УПРАВЛІННЯ КОМП'ЮТЕРИЗОВАНИХ СИСТЕМ
	'63'  => array(
				'name' => 'A418',
				'info' => array(
					'1' => array(
						'post'     => 'B419',
						'category' => 'C419',
						'count'    => 'D419',
					),
					'2' => array(
						'post'     => 'B420',
						'category' => 'C420',
						'count'    => 'D420',
					),
					'3' => array(
						'post'     => 'B421',
						'category' => 'C421',
						'count'    => 'D421',
					),
					'4' => array(
						'post'     => 'B422',
						'category' => 'C422',
						'count'    => 'D422',
					),
					'5' => array(
						'post'     => 'B423',
						'category' => 'C423',
						'count'    => 'D423',
					),
				),
			),  

    // Відділ розвитку інформаційних технологій і впровадження
	'64'  => array(
				'name' => 'A426',
				'info' => array(
					'1' => array(
						'post'     => 'B427',
						'category' => 'C427',
						'count'    => 'D427',
					),
					'2' => array(
						'post'     => 'B428',
						'category' => 'C428',
						'count'    => 'D428',
					),
				),
			),  

    // Сектор супроводження баз даних "Законопроект"
	'65'  => array(
				'name' => 'A431',
				'info' => array(
					'1' => array(
						'post'     => 'B432',
						'category' => 'C432',
						'count'    => 'D432',
					),
					'2' => array(
						'post'     => 'B433',
						'category' => 'C433',
						'count'    => 'D433',
					),
				),
			),  

    // Сектор супроводження прикладного програмного забезпечення
	'66'  => array(
				'name' => 'A436',
				'info' => array(
					'1' => array(
						'post'     => 'B437',
						'category' => 'C437',
						'count'    => 'D437',
					),
					'2' => array(
						'post'     => 'B438',
						'category' => 'C438',
						'count'    => 'D438',
					),
					'3' => array(
						'post'     => 'B439',
						'category' => 'C439',
						'count'    => 'D439',
					),
				),
			),  

    // Відділ досліджень та перспективних розробок
	'67'  => array(
				'name' => 'A443',
				'info' => array(
					'1' => array(
						'post'     => 'B444',
						'category' => 'C444',
						'count'    => 'D444',
					),
					'2' => array(
						'post'     => 'B445',
						'category' => 'C445',
						'count'    => 'D445',
					),
					'3' => array(
						'post'     => 'B446',
						'category' => 'C446',
						'count'    => 'D446',
					),
				),
			),  

    // Сектор адміністрування комп'ютерних мереж і передачі даних
	'68'  => array(
				'name' => 'A450',
				'info' => array(
					'1' => array(
						'post'     => 'B451',
						'category' => 'C451',
						'count'    => 'D451',
					),
					'2' => array(
						'post'     => 'B452',
						'category' => 'C452',
						'count'    => 'D452',
					),
					'3' => array(
						'post'     => 'B453',
						'category' => 'C453',
						'count'    => 'D453',
					),
				),
			),  

    // Сектор захисту інформації в автоматизованих системах
	'69'  => array(
				'name' => 'A456',
				'info' => array(
					'1' => array(
						'post'     => 'B457',
						'category' => 'C457',
						'count'    => 'D457',
					),
					'2' => array(
						'post'     => 'B458',
						'category' => 'C458',
						'count'    => 'D458',
					),
					'3' => array(
						'post'     => 'B459',
						'category' => 'C459',
						'count'    => 'D459',
					),
				),
			),  

    // Відділ баз даних нормативно-правової інформації
	'70'  => array(
				'name' => 'A461',
				'info' => array(
					'1' => array(
						'post'     => 'B462',
						'category' => 'C462',
						'count'    => 'D462',
					),
					'2' => array(
						'post'     => 'B463',
						'category' => 'C463',
						'count'    => 'D463',
					),
					'3' => array(
						'post'     => 'B464',
						'category' => 'C464',
						'count'    => 'D464',
					),
				),
			),  

    // Сектор розвитку та формування баз даних нормативно-правової інформації
	'71'  => array(
				'name' => 'A467',
				'info' => array(
					'1' => array(
						'post'     => 'B468',
						'category' => 'C468',
						'count'    => 'D468',
					),
					'2' => array(
						'post'     => 'B469',
						'category' => 'C469',
						'count'    => 'D469',
					),
				),
			),  

    // Сектор розробки програмного забезпечення та зв'язків з користувачами
	'72'  => array(
				'name' => 'A472',
				'info' => array(
					'1' => array(
						'post'     => 'B473',
						'category' => 'C473',
						'count'    => 'D473',
					),
					'2' => array(
						'post'     => 'B474',
						'category' => 'C474',
						'count'    => 'D474',
					),
					'3' => array(
						'post'     => 'B475',
						'category' => 'C475',
						'count'    => 'D475',
					),
				),
			),  

    // Сектор збору, класифікації та підготовки документів до введення у бази даних
	'73'  => array(
				'name' => 'A478',
				'info' => array(
					'1' => array(
						'post'     => 'B479',
						'category' => 'C479',
						'count'    => 'D479',
					),
					'2' => array(
						'post'     => 'B480',
						'category' => 'C480',
						'count'    => 'D480',
					),
					'3' => array(
						'post'     => 'B481',
						'category' => 'C481',
						'count'    => 'D481',
					),
				),
			),  

    // Системно-технологічний відділ  
	'74'  => array(
				'name' => 'A485',
				'info' => array(
					'1' => array(
						'post'     => 'B486',
						'category' => 'C486',
						'count'    => 'D486',
					),
					'2' => array(
						'post'     => 'B487',
						'category' => 'C487',
						'count'    => 'D487',
					),
					'3' => array(
						'post'     => 'B488',
						'category' => 'C488',
						'count'    => 'D488',
					),
					'4' => array(
						'post'     => 'B489',
						'category' => 'C489',
						'count'    => 'D489',
					),
				),
			),  

    // Відділ спеціалізованих інформаційно-технічних комплексів
	'75'  => array(
				'name' => 'A492',
				'info' => array(
					'1' => array(
						'post'     => 'B493',
						'category' => 'C493',
						'count'    => 'D493',
					),
					'2' => array(
						'post'     => 'B494',
						'category' => 'C494',
						'count'    => 'D494',
					),
					'3' => array(
						'post'     => 'B495',
						'category' => 'C495',
						'count'    => 'D495',
					),
				),
			),  

    // УПРАВЛІННЯ ПО ЗВ'ЯЗКАХ З МІСЦЕВИМИ ОРГАНАМИ ВЛАДИ І ОРГАНАМИ МІСЦЕВОГО САМОВРЯДУВАННЯ
	'76'  => array(
				'name' => 'A499',
				'info' => array(
					'1' => array(
						'post'     => 'B500',
						'category' => 'C500',
						'count'    => 'D500',
					),
					'2' => array(
						'post'     => 'B501',
						'category' => 'C501',
						'count'    => 'D501',
					),
					'3' => array(
						'post'     => 'B502',
						'category' => 'C502',
						'count'    => 'D502',
					),
				),
			),  

    // Відділ організаційної роботи з регіонами
	'77'  => array(
				'name' => 'A505',
				'info' => array(
					'1' => array(
						'post'     => 'B506',
						'category' => 'C506',
						'count'    => 'D506',
					),
					'2' => array(
						'post'     => 'B507',
						'category' => 'C507',
						'count'    => 'D507',
					),
					'3' => array(
						'post'     => 'B508',
						'category' => 'C508',
						'count'    => 'D508',
					),
				),
			),  

    // Сектор з питань методичної роботи
	'78'  => array(
				'name' => 'A511',
				'info' => array(
					'1' => array(
						'post'     => 'B512',
						'category' => 'C512',
						'count'    => 'D512',
					),
					'2' => array(
						'post'     => 'B513',
						'category' => 'C513',
						'count'    => 'D513',
					),
					'3' => array(
						'post'     => 'B514',
						'category' => 'C514',
						'count'    => 'D514',
					),
				),
			),  

    // Сектор з питань адміністративно-територіального устрою та виборчих систем 
	'79'  => array(
				'name' => 'A517',
				'info' => array(
					'1' => array(
						'post'     => 'B518',
						'category' => 'C518',
						'count'    => 'D518',
					),
					'2' => array(
						'post'     => 'B519',
						'category' => 'C519',
						'count'    => 'D519',
					),
				),
			),  

    // УПРАВЛІННЯ ЗАБЕЗПЕЧЕННЯ МІЖПАРЛАМЕНТСЬКИХ ЗВ'ЯЗКІВ
	'80'  => array(
				'name' => 'A524',
				'info' => array(
					'1' => array(
						'post'     => 'B525',
						'category' => 'C525',
						'count'    => 'D525',
					),
					'2' => array(
						'post'     => 'B526',
						'category' => 'C526',
						'count'    => 'D526',
					),
					'3' => array(
						'post'     => 'B527',
						'category' => 'C527',
						'count'    => 'D527',
					),
					'4' => array(
						'post'     => 'B528',
						'category' => 'C528',
						'count'    => 'D528',
					),
				),
			),  

    // Відділ організаційно-протокольного забезпечення
	'81'  => array(
				'name' => 'A531',
				'info' => array(
					'1' => array(
						'post'     => 'B532',
						'category' => 'C532',
						'count'    => 'D532',
					),
					'2' => array(
						'post'     => 'B533',
						'category' => 'C533',
						'count'    => 'D533',
					),
					'3' => array(
						'post'     => 'B534',
						'category' => 'C534',
						'count'    => 'D534',
					),
					'4' => array(
						'post'     => 'B535',
						'category' => 'C535',
						'count'    => 'D535',
					),
				),
			),  

    // Відділ забезпечення зв'язків з національними парламентами
	'82'  => array(
				'name' => 'A539',
				'info' => array(
					'1' => array(
						'post'     => 'B540',
						'category' => 'C540',
						'count'    => 'D540',
					),
					'2' => array(
						'post'     => 'B541',
						'category' => 'C541',
						'count'    => 'D541',
					),
					'3' => array(
						'post'     => 'B542',
						'category' => 'C542',
						'count'    => 'D542',
					),
					'4' => array(
						'post'     => 'B543',
						'category' => 'C543',
						'count'    => 'D543',
					),
				),
			),  

    // Відділ забезпечення зв'язків з міжнародними парламентськими організаціями
	'83'  => array(
				'name' => 'A546',
				'info' => array(
					'1' => array(
						'post'     => 'B547',
						'category' => 'C547',
						'count'    => 'D547',
					),
					'2' => array(
						'post'     => 'B548',
						'category' => 'C548',
						'count'    => 'D548',
					),
					'3' => array(
						'post'     => 'B549',
						'category' => 'C549',
						'count'    => 'D549',
					),
					'4' => array(
						'post'     => 'B550',
						'category' => 'C550',
						'count'    => 'D550',
					),
				),
			),  

    // УПРАВЛІННЯ КАДРІВ
	'84'  => array(
				'name' => 'A554',
				'info' => array(
					'1' => array(
						'post'     => 'B555',
						'category' => 'C555',
						'count'    => 'D555',
					),
					'2' => array(
						'post'     => 'B556',
						'category' => 'C556',
						'count'    => 'D556',
					),
				),
			),  

    // Відділ кадрового забезпечення народних депутатів України 
	'85'  => array(
				'name' => 'A559',
				'info' => array(
					'1' => array(
						'post'     => 'B560',
						'category' => 'C560',
						'count'    => 'D560',
					),
					'2' => array(
						'post'     => 'B561',
						'category' => 'C561',
						'count'    => 'D561',
					),
				),
			),  

    // Відділ  кадрового забезпечення помічників-консультантів народних депутатів України 
	'86'  => array(
				'name' => 'A565',
				'info' => array(
					'1' => array(
						'post'     => 'B566',
						'category' => 'C566',
						'count'    => 'D566',
					),
					'2' => array(
						'post'     => 'B567',
						'category' => 'C567',
						'count'    => 'D567',
					),
					'3' => array(
						'post'     => 'B568',
						'category' => 'C568',
						'count'    => 'D568',
					),
				),
			),  

    // Відділ кадрового забезпечення працівників Апарату Верховної Ради України
	'87'  => array(
				'name' => 'A571',
				'info' => array(
					'1' => array(
						'post'     => 'B572',
						'category' => 'C572',
						'count'    => 'D572',
					),
					'2' => array(
						'post'     => 'B573',
						'category' => 'C573',
						'count'    => 'D573',
					),
				),
			),  

    // Сектор з питань державної служби та підвищення кваліфікації працівників
	'88'  => array(
				'name' => 'A576',
				'info' => array(
					'1' => array(
						'post'     => 'B577',
						'category' => 'C577',
						'count'    => 'D577',
					),
					'2' => array(
						'post'     => 'B578',
						'category' => 'C578',
						'count'    => 'D578',
					),
				),
			),  

    // Сектор з питань нагородження
	'89'  => array(
				'name' => 'A582',
				'info' => array(
					'1' => array(
						'post'     => 'B583',
						'category' => 'C583',
						'count'    => 'D583',
					),
					'2' => array(
						'post'     => 'B584',
						'category' => 'C584',
						'count'    => 'D584',
					),
					'3' => array(
						'post'     => 'B585',
						'category' => 'C585',
						'count'    => 'D585',
					),
				),
			),  

    // Сектор реєстрації та опрацювання документів
	'90'  => array(
				'name' => 'A588',
				'info' => array(
					'1' => array(
						'post'     => 'B589',
						'category' => 'C589',
						'count'    => 'D589',
					),
					'2' => array(
						'post'     => 'B590',
						'category' => 'C590',
						'count'    => 'D590',
					),
					'3' => array(
						'post'     => 'B591',
						'category' => 'C591',
						'count'    => 'D591',
					),
					'4' => array(
						'post'     => 'B592',
						'category' => 'C592',
						'count'    => 'D592',
					),
				),
			),  

    // ВІДДІЛ ЗВ'ЯЗКІВ З ОРГАНАМИ ПРАВОСУДДЯ
	'91'  => array(
				'name' => 'A596',
				'info' => array(
					'1' => array(
						'post'     => 'B597',
						'category' => 'C597',
						'count'    => 'D597',
					),
					'2' => array(
						'post'     => 'B598',
						'category' => 'C598',
						'count'    => 'D598',
					),
					'3' => array(
						'post'     => 'B599',
						'category' => 'C599',
						'count'    => 'D599',
					),
					'4' => array(
						'post'     => 'B601',
						'category' => 'C601',
						'count'    => 'D601',
					),
				),
			),  

    // ВІДДІЛ КОНТРОЛЮ
	'92'  => array(
				'name' => 'A605',
				'info' => array(
					'1' => array(
						'post'     => 'B606',
						'category' => 'C606',
						'count'    => 'D606',
					),
					'2' => array(
						'post'     => 'B607',
						'category' => 'C607',
						'count'    => 'D607',
					),
					'3' => array(
						'post'     => 'B608',
						'category' => 'C608',
						'count'    => 'D608',
					),
					'4' => array(
						'post'     => 'B609',
						'category' => 'C609',
						'count'    => 'D609',
					),
				),
			),  

    // ВІДДІЛ З ПИТАНЬ ЗВЕРНЕНЬ ГРОМАДЯН
	'93'  => array(
				'name' => 'A612',
				'info' => array(
					'1' => array(
						'post'     => 'B613',
						'category' => 'C613',
						'count'    => 'D613',
					),
					'2' => array(
						'post'     => 'B614',
						'category' => 'C614',
						'count'    => 'D614',
					),
				),
			),  

    // Сектор прийому громадян
	'94'  => array(
				'name' => 'A617',
				'info' => array(
					'1' => array(
						'post'     => 'B618',
						'category' => 'C618',
						'count'    => 'D618',
					),
					'2' => array(
						'post'     => 'B619',
						'category' => 'C619',
						'count'    => 'D619',
					),
				),
			),  

    // Сектор розгляду листів
	'95'  => array(
				'name' => 'A622',
				'info' => array(
					'1' => array(
						'post'     => 'B623',
						'category' => 'C623',
						'count'    => 'D623',
					),
					'2' => array(
						'post'     => 'B624',
						'category' => 'C624',
						'count'    => 'D624',
					),
				),
			),  

    // Сектор обліку, аналізу та узагальнення звернень громадян
	'96'  => array(
				'name' => 'A627',
				'info' => array(
					'1' => array(
						'post'     => 'B628',
						'category' => 'C628',
						'count'    => 'D628',
					),
					'2' => array(
						'post'     => 'B629',
						'category' => 'C629',
						'count'    => 'D629',
					),
					'3' => array(
						'post'     => 'B630',
						'category' => 'C630',
						'count'    => 'D630',
					),
				),
			),  

    // Сектор контролю за зверненнями громадян
	'97'  => array(
				'name' => 'A633',
				'info' => array(
					'1' => array(
						'post'     => 'B634',
						'category' => 'C634',
						'count'    => 'D634',
					),
					'2' => array(
						'post'     => 'B635',
						'category' => 'C635',
						'count'    => 'D635',
					),
					'3' => array(
						'post'     => 'B636',
						'category' => 'C636',
						'count'    => 'D636',
					),
					'4' => array(
						'post'     => 'B637',
						'category' => 'C637',
						'count'    => 'D637',
					),
				),
			),  

    // ПЕРШИЙ ВІДДІЛ 
	'98'  => array(
				'name' => 'A641',
				'info' => array(
					'1' => array(
						'post'     => 'B642',
						'category' => 'C642',
						'count'    => 'D642',
					),
					'2' => array(
						'post'     => 'B644',
						'category' => 'C644',
						'count'    => 'D644',
					),
					'3' => array(
						'post'     => 'B645',
						'category' => 'C645',
						'count'    => 'D645',
					),
				),
			),  

    // ПРЕС-СЛУЖБА
	'99'  => array(
				'name' => 'A648',
				'info' => array(
					'1' => array(
						'post'     => 'B649',
						'category' => 'C649',
						'count'    => 'D649',
					),
					'2' => array(
						'post'     => 'B650',
						'category' => 'C650',
						'count'    => 'D650',
					),
					'3' => array(
						'post'     => 'B651',
						'category' => 'C651',
						'count'    => 'D651',
					),
				),
			),  

    // СЕКТОР МОБІЛІЗАЦІЙНОЇ РОБОТИ
	'100'  => array(
				'name' => 'A654',
				'info' => array(
					'1' => array(
						'post'     => 'B655',
						'category' => 'C655',
						'count'    => 'D655',
					),
					'2' => array(
						'post'     => 'B656',
						'category' => 'C656',
						'count'    => 'D656',
					),
				),
			),  

    // УПРАВЛІННЯ СПРАВАМИ
	// Керівництво Управління справами
	'101'  => array(
				'name' => 'A660',
				'info' => array(
					'1' => array(
						'post'     => 'B661',
						'category' => 'C661',
						'count'    => 'D661',
					),
					'2' => array(
						'post'     => 'B662',
						'category' => 'C662',
						'count'    => 'D662',
					),
					'3' => array(
						'post'     => 'B663',
						'category' => 'C663',
						'count'    => 'D663',
					),
					'4' => array(
						'post'     => 'B664',
						'category' => 'C664',
						'count'    => 'D664',
					),
				),
			),

    // Відділ бухгалтерського обліку, фінансів та планування 
	'102'  => array(
				'name' => 'A667',
				'info' => array(
					'1' => array(
						'post'     => 'B668',
						'category' => 'C668',
						'count'    => 'D668',
					),
					'2' => array(
						'post'     => 'B669',
						'category' => 'C669',
						'count'    => 'D669',
					),
				),
			),

    // Сектор автоматизації бухгалтерського обліку і звітності
	'103'  => array(
				'name' => 'A673',
				'info' => array(
					'1' => array(
						'post'     => 'B674',
						'category' => 'C674',
						'count'    => 'D674',
					),
					'2' => array(
						'post'     => 'B675',
						'category' => 'C675',
						'count'    => 'D675',
					),
				),
			),

    // Сектор розрахунків з народними депутатами України
	'104'  => array(
				'name' => 'A678',
				'info' => array(
					'1' => array(
						'post'     => 'B679',
						'category' => 'C679',
						'count'    => 'D679',
					),
					'2' => array(
						'post'     => 'B680',
						'category' => 'C680',
						'count'    => 'D680',
					),
				),
			),

    // Сектор розрахунків з помічниками-консультантами народних депутатів України
	'105'  => array(
				'name' => 'A683',
				'info' => array(
					'1' => array(
						'post'     => 'B684',
						'category' => 'C684',
						'count'    => 'D684',
					),
					'2' => array(
						'post'     => 'B685',
						'category' => 'C685',
						'count'    => 'D685',
					),
				),
			),

    // Сектор розрахунків з працівниками Апарату
	'106'  => array(
				'name' => 'A688',
				'info' => array(
					'1' => array(
						'post'     => 'B689',
						'category' => 'C689',
						'count'    => 'D689',
					),
					'2' => array(
						'post'     => 'B690',
						'category' => 'C690',
						'count'    => 'D690',
					),
				),
			),

    // Сектор розрахунків з установами і організаціями
	'107'  => array(
				'name' => 'A693',
				'info' => array(
					'1' => array(
						'post'     => 'B694',
						'category' => 'C694',
						'count'    => 'D694',
					),
					'2' => array(
						'post'     => 'B695',
						'category' => 'C695',
						'count'    => 'D695',
					),
				),
			),

    // Сектор матеріального обліку
	'108'  => array(
				'name' => 'A698',
				'info' => array(
					'1' => array(
						'post'     => 'B699',
						'category' => 'C699',
						'count'    => 'D699',
					),
					'2' => array(
						'post'     => 'B700',
						'category' => 'C700',
						'count'    => 'D700',
					),
				),
			),

    // Сектор планування, фінансів та аналізу
	'109'  => array(
				'name' => 'A703',
				'info' => array(
					'1' => array(
						'post'     => 'B704',
						'category' => 'C704',
						'count'    => 'D704',
					),
					'2' => array(
						'post'     => 'B705',
						'category' => 'C705',
						'count'    => 'D705',
					),
				),
			),

    // Загальний відділ 
	'110'  => array(
				'name' => 'A714',
				'info' => array(
					'1' => array(
						'post'     => 'B715',
						'category' => 'C715',
						'count'    => 'D715',
					),
					'2' => array(
						'post'     => 'B716',
						'category' => 'C716',
						'count'    => 'D716',
					),
					'3' => array(
						'post'     => 'B717',
						'category' => 'C717',
						'count'    => 'D717',
					),
					'4' => array(
						'post'     => 'B718',
						'category' => 'C718',
						'count'    => 'D718',
					),
				),
			),

    // Сектор реєстрації та відправлення кореспонденції
	'111'  => array(
				'name' => 'A721',
				'info' => array(
					'1' => array(
						'post'     => 'B722',
						'category' => 'C722',
						'count'    => 'D722',
					),
					'2' => array(
						'post'     => 'B723',
						'category' => 'C723',
						'count'    => 'D723',
					),
				),
			),

    // Відділ медичного, санаторного та побутового забезпечення
	'112'  => array(
				'name' => 'A727',
				'info' => array(
					'1' => array(
						'post'     => 'B728',
						'category' => 'C728',
						'count'    => 'D728',
					),
					'2' => array(
						'post'     => 'B730',
						'category' => 'C730',
						'count'    => 'D730',
					),
				),
			),			

    // Відділ господарського забезпечення
	'113'  => array(
				'name' => 'A734',
				'info' => array(
					'1' => array(
						'post'     => 'B735',
						'category' => 'C735',
						'count'    => 'D735',
					),
					'2' => array(
						'post'     => 'B736',
						'category' => 'C736',
						'count'    => 'D736',
					),
				),
			),  

    // Сектор обслуговування протокольних заходів
	'114'  => array(
				'name' => 'A739',
				'info' => array(
					'1' => array(
						'post'     => 'B740',
						'category' => 'C740',
						'count'    => 'D740',
					),
					'2' => array(
						'post'     => 'B741',
						'category' => 'C741',
						'count'    => 'D741',
					),
				),
			),  

    // Сектор господарського обслуговування
	'115'  => array(
				'name' => 'A744',
				'info' => array(
					'1' => array(
						'post'     => 'B745',
						'category' => 'C745',
						'count'    => 'D745',
					),
					'2' => array(
						'post'     => 'B746',
						'category' => 'C746',
						'count'    => 'D746',
					),
					'3' => array(
						'post'     => 'B747',
						'category' => 'C747',
						'count'    => 'D747',
					),
				),
			),  

    // Сектор забезпечення заходів
	'116'  => array(
				'name' => 'A750',
				'info' => array(
					'1' => array(
						'post'     => 'B751',
						'category' => 'C751',
						'count'    => 'D751',
					),
					'2' => array(
						'post'     => 'B752',
						'category' => 'C752',
						'count'    => 'D752',
					),
				),
			),  

    // Відділ будівництва та ремонту
	'117'  => array(
				'name' => 'A757',
				'info' => array(
					'1' => array(
						'post'     => 'B758',
						'category' => 'C758',
						'count'    => 'D758',
					),
					'2' => array(
						'post'     => 'B759',
						'category' => 'C759',
						'count'    => 'D759',
					),
				),
			),  

    // Відділ зв'язку і телерадіосистем
	'118'  => array(
				'name' => 'A761',
				'info' => array(
					'1' => array(
						'post'     => 'B762',
						'category' => 'C762',
						'count'    => 'D762',
					),
					'2' => array(
						'post'     => 'B763',
						'category' => 'C763',
						'count'    => 'D763',
					),
				),
			),  

    // Сектор систем звукопідсилення і звукозапису
	'119'  => array(
				'name' => 'A766',
				'info' => array(
					'1' => array(
						'post'     => 'B767',
						'category' => 'C767',
						'count'    => 'D767',
					),
					'2' => array(
						'post'     => 'B768',
						'category' => 'C768',
						'count'    => 'D768',
					),
				),
			),  

    // Сектор систем телебачення
	'120'  => array(
				'name' => 'A771',
				'info' => array(
					'1' => array(
						'post'     => 'B772',
						'category' => 'C772',
						'count'    => 'D772',
					),
					'2' => array(
						'post'     => 'B773',
						'category' => 'C773',
						'count'    => 'D773',
					),
				),
			),  

    // Сектор  систем зв'язку
	'121'  => array(
				'name' => 'A776',
				'info' => array(
					'1' => array(
						'post'     => 'B777',
						'category' => 'C777',
						'count'    => 'D777',
					),
					'2' => array(
						'post'     => 'B778',
						'category' => 'C778',
						'count'    => 'D778',
					),
				),
			),  

    // Довідково-інформаційний сектор
	'122'  => array(
				'name' => 'A781',
				'info' => array(
					'1' => array(
						'post'     => 'B782',
						'category' => 'C782',
						'count'    => 'D782',
					),
					'2' => array(
						'post'     => 'B783',
						'category' => 'C783',
						'count'    => 'D783',
					),
				),
			),  

    // Сектор технічного захисту інформації
	'123'  => array(
				'name' => 'A786',
				'info' => array(
					'1' => array(
						'post'     => 'B787',
						'category' => 'C787',
						'count'    => 'D787',
					),
					'2' => array(
						'post'     => 'B788',
						'category' => 'C788',
						'count'    => 'D788',
					),
				),
			),  

    // Відділ матеріально-технічного постачання
	'124'  => array(
				'name' => 'A793',
				'info' => array(
					'1' => array(
						'post'     => 'B794',
						'category' => 'C794',
						'count'    => 'D794',
					),
					'2' => array(
						'post'     => 'B795',
						'category' => 'C795',
						'count'    => 'D795',
					),
				),
			),  

    // Сектор забезпечення оргтехнікою та культтоварами
	'125'  => array(
				'name' => 'A798',
				'info' => array(
					'1' => array(
						'post'     => 'B799',
						'category' => 'C799',
						'count'    => 'D799',
					),
					'2' => array(
						'post'     => 'B800',
						'category' => 'C800',
						'count'    => 'D800',
					),
					'3' => array(
						'post'     => 'B801',
						'category' => 'C801',
						'count'    => 'D801',
					),
				),
			),  

    // Сектор господарського забезпечення 
	'126'  => array(
				'name' => 'A804',
				'info' => array(
					'1' => array(
						'post'     => 'B805',
						'category' => 'C805',
						'count'    => 'D805',
					),
					'2' => array(
						'post'     => 'B806',
						'category' => 'C806',
						'count'    => 'D806',
					),
					'3' => array(
						'post'     => 'B807',
						'category' => 'C807',
						'count'    => 'D807',
					),
				),
			),  

    // Відділ контролю та аудиту
	'127'  => array(
				'name' => 'A811',
				'info' => array(
					'1' => array(
						'post'     => 'B812',
						'category' => 'C812',
						'count'    => 'D812',
					),
					'2' => array(
						'post'     => 'B813',
						'category' => 'C813',
						'count'    => 'D813',
					),
					'3' => array(
						'post'     => 'B814',
						'category' => 'C814',
						'count'    => 'D814',
					),
				),
			),  

    // Відділ юридичного забезпечення
	'128'  => array(
				'name' => 'A817',
				'info' => array(
					'1' => array(
						'post'     => 'B818',
						'category' => 'C818',
						'count'    => 'D818',
					),
					'2' => array(
						'post'     => 'B819',
						'category' => 'C819',
						'count'    => 'D819',
					),
				),
			),  

    // Сектор з організації правової та претензійно-позовної роботи
	'129'  => array(
				'name' => 'A822',
				'info' => array(
					'1' => array(
						'post'     => 'B823',
						'category' => 'C823',
						'count'    => 'D823',
					),
					'2' => array(
						'post'     => 'B824',
						'category' => 'C824',
						'count'    => 'D824',
					),
				),
			),  

    // Сектор тдоговірної роботи та супроводження державних закупівель
	'130'  => array(
				'name' => 'A827',
				'info' => array(
					'1' => array(
						'post'     => 'B828',
						'category' => 'C828',
						'count'    => 'D828',
					),
					'2' => array(
						'post'     => 'B829',
						'category' => 'C829',
						'count'    => 'D829',
					),
				),
			),
);

?>