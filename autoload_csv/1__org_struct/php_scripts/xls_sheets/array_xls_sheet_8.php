<?php

//************************************//
//*       Комітети(РГВРУ №147)       *//
//************************************//

$array_xls_sheet_8 = array(
		
	// Секретаріат Комітету з питань аграрної політики та земельних відносин 
	'1'  => array(
				'name' => 'A15',
				'info' => array(
					'1' => array(
						'post'     => 'B16',
						'category' => 'C16',
						'count'    => 'D16',
					),
					'2' => array(
						'post'     => 'B17',
						'category' => 'C17',
						'count'    => 'D17',
					),
					'3' => array(
						'post'     => 'B19',
						'category' => 'C19',
						'count'    => 'D19',
					),
					'4' => array(
						'post'     => 'B20',
						'category' => 'C20',
						'count'    => 'D20',
					),
					'5' => array(
						'post'     => 'B21',
						'category' => 'C21',
						'count'    => 'D21',
					),
					'6' => array(
						'post'     => 'B22',
						'category' => 'C22',
						'count'    => 'D22',
					),
				),
			),  
			
	// Секретаріат Комітету з питань будівництва, містобудування і житлово-комунального господарства 
	'2'  => array(
				'name' => 'A24',
				'info' => array(
					'1' => array(
						'post'     => 'B25',
						'category' => 'C25',
						'count'    => 'D25',
					),
					'2' => array(
						'post'     => 'B26',
						'category' => 'C26',
						'count'    => 'D26',
					),
					'3' => array(
						'post'     => 'B27',
						'category' => 'C27',
						'count'    => 'D27',
					),
					'4' => array(
						'post'     => 'B28',
						'category' => 'C28',
						'count'    => 'D28',
					),
					'5' => array(
						'post'     => 'B29',
						'category' => 'C29',
						'count'    => 'D29',
					),
				),
			),

	// Секретаріат Комітету з питань бюджету
	'3'  => array(
				'name' => 'A32',
				'info' => array(
					'1' => array(
						'post'     => 'B33',
						'category' => 'C33',
						'count'    => 'D33',
					),
					'2' => array(
						'post'     => 'B34',
						'category' => 'C34',
						'count'    => 'D34',
					),
					'3' => array(
						'post'     => 'B35',
						'category' => 'C35',
						'count'    => 'D35',
					),
					'4' => array(
						'post'     => 'B36',
						'category' => 'C36',
						'count'    => 'D36',
					),
					'5' => array(
						'post'     => 'B38',
						'category' => 'C38',
						'count'    => 'D38',
					),
				),
			),  				

	// Секретаріат Комітету з питань державного будівництва, регіональної політики та місцевого самоврядування 
	'4'  => array(
				'name' => 'A40',
				'info' => array(
					'1' => array(
						'post'     => 'B41',
						'category' => 'C41',
						'count'    => 'D41',
					),
					'2' => array(
						'post'     => 'B42',
						'category' => 'C42',
						'count'    => 'D42',
					),
					'3' => array(
						'post'     => 'B43',
						'category' => 'C43',
						'count'    => 'D43',
					),
					'4' => array(
						'post'     => 'B44',
						'category' => 'C44',
						'count'    => 'D44',
					),
				),
			),  

	// Секретаріат Комітету з питань екологічної політики, природокористування та ліквідації наслідків Чорнобильської катастрофи 
	'5'  => array(
				'name' => 'A47',
				'info' => array(
					'1' => array(
						'post'     => 'B48',
						'category' => 'C48',
						'count'    => 'D48',
					),
					'2' => array(
						'post'     => 'B49',
						'category' => 'C49',
						'count'    => 'D49',
					),
					'3' => array(
						'post'     => 'B50',
						'category' => 'C50',
						'count'    => 'D50',
					),
					'4' => array(
						'post'     => 'B51',
						'category' => 'C51',
						'count'    => 'D51',
					),
					'5' => array(
						'post'     => 'B52',
						'category' => 'C52',
						'count'    => 'D52',
					),
					'6' => array(
						'post'     => 'B53',
						'category' => 'C53',
						'count'    => 'D53',
					),
				),
			),  

	// Секретаріат Комітету з питань економічної політики 
	'6'  => array(
				'name' => 'A55',
				'info' => array(
					'1' => array(
						'post'     => 'B56',
						'category' => 'C56',
						'count'    => 'D56',
					),
					'2' => array(
						'post'     => 'B57',
						'category' => 'C57',
						'count'    => 'D57',
					),
					'3' => array(
						'post'     => 'B58',
						'category' => 'C58',
						'count'    => 'D58',
					),
					'4' => array(
						'post'     => 'B59',
						'category' => 'C59',
						'count'    => 'D59',
					),
					'5' => array(
						'post'     => 'B60',
						'category' => 'C60',
						'count'    => 'D60',
					),
				),
			),  

	// Секретаріат Комітету з питань європейської інтеграції  
	'7'  => array(
				'name' => 'A63',
				'info' => array(
					'1' => array(
						'post'     => 'B64',
						'category' => 'C64',
						'count'    => 'D64',
					),
					'2' => array(
						'post'     => 'B65',
						'category' => 'C65',
						'count'    => 'D65',
					),
					'3' => array(
						'post'     => 'B66',
						'category' => 'C66',
						'count'    => 'D66',
					),
					'4' => array(
						'post'     => 'B67',
						'category' => 'C67',
						'count'    => 'D67',
					),
					'5' => array(
						'post'     => 'B68',
						'category' => 'C68',
						'count'    => 'D68',
					),
				),
			),  

	// Секретаріат Комітету з питань законодавчого забезпечення правоохоронної діяльності 
	'8'  => array(
				'name' => 'A70',
				'info' => array(
					'1' => array(
						'post'     => 'B71',
						'category' => 'C71',
						'count'    => 'D71',
					),
					'2' => array(
						'post'     => 'B72',
						'category' => 'C72',
						'count'    => 'D72',
					),
					'3' => array(
						'post'     => 'B73',
						'category' => 'C73',
						'count'    => 'D73',
					),
					'4' => array(
						'post'     => 'B74',
						'category' => 'C74',
						'count'    => 'D74',
					),
					'5' => array(
						'post'     => 'B75',
						'category' => 'C75',
						'count'    => 'D75',
					),
					'6' => array(
						'post'     => 'B76',
						'category' => 'C76',
						'count'    => 'D76',
					),
				),
			),  

	// Секретаріат Комітету з питань запобігання і протидії корупції 
	'9'  => array(
				'name' => 'A78',
				'info' => array(
					'1' => array(
						'post'     => 'B79',
						'category' => 'C79',
						'count'    => 'D79',
					),
					'2' => array(
						'post'     => 'B80',
						'category' => 'C80',
						'count'    => 'D80',
					),
					'3' => array(
						'post'     => 'B81',
						'category' => 'C81',
						'count'    => 'D81',
					),
					'4' => array(
						'post'     => 'B82',
						'category' => 'C82',
						'count'    => 'D82',
					),
					'5' => array(
						'post'     => 'B83',
						'category' => 'C83',
						'count'    => 'D83',
					),
				),
			),  

	// Секретаріат Комітету у закордонних справах 
	'10'  => array(
				'name' => 'A85',
				'info' => array(
					'1' => array(
						'post'     => 'B86',
						'category' => 'C86',
						'count'    => 'D86',
					),
					'2' => array(
						'post'     => 'B87',
						'category' => 'C87',
						'count'    => 'D87',
					),
					'3' => array(
						'post'     => 'B88',
						'category' => 'C88',
						'count'    => 'D88',
					),
					'4' => array(
						'post'     => 'B89',
						'category' => 'C89',
						'count'    => 'D89',
					),
				),
			),  			

	// Секретаріат Комітету з питань інформатизації та зв"язку
	'11'  => array(
				'name' => 'A91',
				'info' => array(
					'1' => array(
						'post'     => 'B92',
						'category' => 'C92',
						'count'    => 'D92',
					),
					'2' => array(
						'post'     => 'B93',
						'category' => 'C93',
						'count'    => 'D93',
					),
					'3' => array(
						'post'     => 'B94',
						'category' => 'C94',
						'count'    => 'D94',
					),
					'4' => array(
						'post'     => 'B95',
						'category' => 'C95',
						'count'    => 'D95',
					),
				),
			),  

	// Секретаріат Комітету з питань культури і духовності
	'12'  => array(
				'name' => 'A99',
				'info' => array(
					'1' => array(
						'post'     => 'B100',
						'category' => 'C100',
						'count'    => 'D100',
					),
					'2' => array(
						'post'     => 'B101',
						'category' => 'C101',
						'count'    => 'D101',
					),
					'3' => array(
						'post'     => 'B102',
						'category' => 'C102',
						'count'    => 'D102',
					),
					'4' => array(
						'post'     => 'B103',
						'category' => 'C103',
						'count'    => 'D103',
					),
					'5' => array(
						'post'     => 'B104',
						'category' => 'C104',
						'count'    => 'D104',
					),
				),
			),  

	// Секретаріат Комітету з питань науки і освіти
	'13'  => array(
				'name' => 'A106',
				'info' => array(
					'1' => array(
						'post'     => 'B107',
						'category' => 'C107',
						'count'    => 'D107',
					),
					'2' => array(
						'post'     => 'B108',
						'category' => 'C108',
						'count'    => 'D108',
					),
					'3' => array(
						'post'     => 'B109',
						'category' => 'C109',
						'count'    => 'D109',
					),
					'4' => array(
						'post'     => 'B111',
						'category' => 'C111',
						'count'    => 'D111',
					),
				),
			),  			

	// Секретаріат Комітету з питань національної безпеки і оборони
	'14'  => array(
				'name' => 'A114',
				'info' => array(
					'1' => array(
						'post'     => 'B115',
						'category' => 'C115',
						'count'    => 'D115',
					),
					'2' => array(
						'post'     => 'B116',
						'category' => 'C116',
						'count'    => 'D116',
					),
					'3' => array(
						'post'     => 'B117',
						'category' => 'C117',
						'count'    => 'D117',
					),
					'4' => array(
						'post'     => 'B118',
						'category' => 'C118',
						'count'    => 'D118',
					),
					'5' => array(
						'post'     => 'B119',
						'category' => 'C119',
						'count'    => 'D119',
					),
				),
			),  

	// Секретаріат Комітету з питань охорони здоров'я
	'15'  => array(
				'name' => 'A122',
				'info' => array(
					'1' => array(
						'post'     => 'B123',
						'category' => 'C123',
						'count'    => 'D123',
					),
					'2' => array(
						'post'     => 'B124',
						'category' => 'C124',
						'count'    => 'D124',
					),
					'3' => array(
						'post'     => 'B125',
						'category' => 'C125',
						'count'    => 'D125',
					),
					'4' => array(
						'post'     => 'B126',
						'category' => 'C126',
						'count'    => 'D126',
					),
				),
			),  

	// Секретаріат Комітету з питань паливно-енергетичного комплексу, ядерної політики та ядерної безпеки
	'16'  => array(
				'name' => 'A128',
				'info' => array(
					'1' => array(
						'post'     => 'B129',
						'category' => 'C129',
						'count'    => 'D129',
					),
					'2' => array(
						'post'     => 'B130',
						'category' => 'C130',
						'count'    => 'D130',
					),
					'3' => array(
						'post'     => 'B131',
						'category' => 'C131',
						'count'    => 'D131',
					),
					'4' => array(
						'post'     => 'B132',
						'category' => 'C132',
						'count'    => 'D132',
					),
					'5' => array(
						'post'     => 'B133',
						'category' => 'C133',
						'count'    => 'D133',
					),
					'6' => array(
						'post'     => 'B134',
						'category' => 'C134',
						'count'    => 'D134',
					),
				),
			),  


	// Секретаріат Комітету з питань податкової та митної політики
	'17'  => array(
				'name' => 'A136',
				'info' => array(
					'1' => array(
						'post'     => 'B137',
						'category' => 'C137',
						'count'    => 'D137',
					),
					'2' => array(
						'post'     => 'B138',
						'category' => 'C138',
						'count'    => 'D138',
					),
					'3' => array(
						'post'     => 'B139',
						'category' => 'C139',
						'count'    => 'D139',
					),
					'4' => array(
						'post'     => 'B140',
						'category' => 'C140',
						'count'    => 'D140',
					),
				),
			),  			

	// Секретаріат Комітету з питань прав людини, національних меншин і міжнаціональних відносин
	'18'  => array(
				'name' => 'A143',
				'info' => array(
					'1' => array(
						'post'     => 'B144',
						'category' => 'C144',
						'count'    => 'D144',
					),
					'2' => array(
						'post'     => 'B145',
						'category' => 'C145',
						'count'    => 'D145',
					),
					'3' => array(
						'post'     => 'B146',
						'category' => 'C146',
						'count'    => 'D146',
					),
					'4' => array(
						'post'     => 'B147',
						'category' => 'C147',
						'count'    => 'D147',
					),
					'5' => array(
						'post'     => 'B148',
						'category' => 'C148',
						'count'    => 'D148',
					),
					'6' => array(
						'post'     => 'B149',
						'category' => 'C149',
						'count'    => 'D149',
					),
				),
			),  			
			
	// Секретаріат Комітету з питань правової політики та правосуддя 
	'19'  => array(
				'name' => 'A151',
				'info' => array(
					'1' => array(
						'post'     => 'B152',
						'category' => 'C152',
						'count'    => 'D152',
					),
					'2' => array(
						'post'     => 'B153',
						'category' => 'C153',
						'count'    => 'D153',
					),
					'3' => array(
						'post'     => 'B154',
						'category' => 'C154',
						'count'    => 'D154',
					),
					'4' => array(
						'post'     => 'B155',
						'category' => 'C155',
						'count'    => 'D155',
					),
					'5' => array(
						'post'     => 'B156',
						'category' => 'C156',
						'count'    => 'D156',
					),
					'6' => array(
						'post'     => 'B157',
						'category' => 'C157',
						'count'    => 'D157',
					),
				),
			),  			

	// Секретаріат Комітету з питань промислової політики та підприємництва
	'20'  => array(
				'name' => 'A159',
				'info' => array(
					'1' => array(
						'post'     => 'B161',
						'category' => 'C161',
						'count'    => 'D161',
					),
					'2' => array(
						'post'     => 'B162',
						'category' => 'C162',
						'count'    => 'D162',
					),
					'3' => array(
						'post'     => 'B163',
						'category' => 'C163',
						'count'    => 'D163',
					),
					'4' => array(
						'post'     => 'B164',
						'category' => 'C164',
						'count'    => 'D164',
					),
					'5' => array(
						'post'     => 'B165',
						'category' => 'C165',
						'count'    => 'D165',
					),
				),
			),  			

	// Секретаріат Комітету з питань Регламенту та організації роботи Верховної Ради України
	'21'  => array(
				'name' => 'A167',
				'info' => array(
					'1' => array(
						'post'     => 'B168',
						'category' => 'C168',
						'count'    => 'D168',
					),
					'2' => array(
						'post'     => 'B169',
						'category' => 'C169',
						'count'    => 'D169',
					),
					'3' => array(
						'post'     => 'B170',
						'category' => 'C170',
						'count'    => 'D170',
					),
					'4' => array(
						'post'     => 'B171',
						'category' => 'C171',
						'count'    => 'D171',
					),
					'5' => array(
						'post'     => 'B172',
						'category' => 'C172',
						'count'    => 'D172',
					),
					'6' => array(
						'post'     => 'B173',
						'category' => 'C173',
						'count'    => 'D173',
					),
				),
			),  

	// Секретаріат Комітету з питань свободи слова та інформаційної політики
	'22'  => array(
				'name' => 'A176',
				'info' => array(
					'1' => array(
						'post'     => 'B177',
						'category' => 'C177',
						'count'    => 'D177',
					),
					'2' => array(
						'post'     => 'B178',
						'category' => 'C178',
						'count'    => 'D178',
					),
					'3' => array(
						'post'     => 'B179',
						'category' => 'C179',
						'count'    => 'D179',
					),
					'4' => array(
						'post'     => 'B180',
						'category' => 'C180',
						'count'    => 'D180',
					),
					'5' => array(
						'post'     => 'B181',
						'category' => 'C181',
						'count'    => 'D181',
					),
				),
			),  

	// Секретаріат Комітету з питань сім’ї, молодіжної політики, спорту та туризму
	'23'  => array(
				'name' => 'A183',
				'info' => array(
					'1' => array(
						'post'     => 'B184',
						'category' => 'C184',
						'count'    => 'D184',
					),
					'2' => array(
						'post'     => 'B185',
						'category' => 'C185',
						'count'    => 'D185',
					),
					'3' => array(
						'post'     => 'B186',
						'category' => 'C186',
						'count'    => 'D186',
					),
					'4' => array(
						'post'     => 'B187',
						'category' => 'C187',
						'count'    => 'D187',
					),
					'5' => array(
						'post'     => 'B188',
						'category' => 'C188',
						'count'    => 'D188',
					),
				),
			),  

	// Секретаріат Комітету з питань соціальної політики, зайнятості та пенсійного забезпечення 
	'24'  => array(
				'name' => 'A190',
				'info' => array(
					'1' => array(
						'post'     => 'B191',
						'category' => 'C191',
						'count'    => 'D191',
					),
					'2' => array(
						'post'     => 'B192',
						'category' => 'C192',
						'count'    => 'D192',
					),
					'3' => array(
						'post'     => 'B193',
						'category' => 'C193',
						'count'    => 'D193',
					),
					'4' => array(
						'post'     => 'B194',
						'category' => 'C194',
						'count'    => 'D194',
					),
					'5' => array(
						'post'     => 'B195',
						'category' => 'C195',
						'count'    => 'D195',
					),
				),
			),  

	// Секретаріат Комітету у справах ветеранів, учасників бойових дій, учасників антитерористичної операції та людей з інвалідністю
	'25'  => array(
				'name' => 'A197',
				'info' => array(
					'1' => array(
						'post'     => 'B198',
						'category' => 'C198',
						'count'    => 'D198',
					),
					'2' => array(
						'post'     => 'B199',
						'category' => 'C199',
						'count'    => 'D199',
					),
					'3' => array(
						'post'     => 'B200',
						'category' => 'C200',
						'count'    => 'D200',
					),
					'4' => array(
						'post'     => 'B201',
						'category' => 'C201',
						'count'    => 'D201',
					),
					'5' => array(
						'post'     => 'B202',
						'category' => 'C202',
						'count'    => 'D202',
					),
				),
			),  

	// Секретаріат Комітету з питань транспорту 
	'26'  => array(
				'name' => 'A204',
				'info' => array(
					'1' => array(
						'post'     => 'B205',
						'category' => 'C205',
						'count'    => 'D205',
					),
					'2' => array(
						'post'     => 'B206',
						'category' => 'C206',
						'count'    => 'D206',
					),
					'3' => array(
						'post'     => 'B207',
						'category' => 'C207',
						'count'    => 'D207',
					),
					'4' => array(
						'post'     => 'B208',
						'category' => 'C208',
						'count'    => 'D208',
					),
				),
			),  

	// Секретаріат Комітету з питань фінансової політики і банківської діяльності
	'27'  => array(
				'name' => 'A211',
				'info' => array(
					'1' => array(
						'post'     => 'B212',
						'category' => 'C212',
						'count'    => 'D212',
					),
					'2' => array(
						'post'     => 'B213',
						'category' => 'C213',
						'count'    => 'D213',
					),
					'3' => array(
						'post'     => 'B214',
						'category' => 'C214',
						'count'    => 'D214',
					),
					'4' => array(
						'post'     => 'B215',
						'category' => 'C215',
						'count'    => 'D215',
					),
				),
			),  

	// Спеціальна контрольна комісія з питань приватизації
	'28'  => array(
				'name' => 'A221',
				'info' => array(
					'1' => array(
						'post'     => 'B222',
						'category' => 'C222',
						'count'    => 'D222',
					),
					'2' => array(
						'post'     => 'B223',
						'category' => 'C223',
						'count'    => 'D223',
					),
					'3' => array(
						'post'     => 'B224',
						'category' => 'C224',
						'count'    => 'D224',
					),
					'4' => array(
						'post'     => 'B225',
						'category' => 'C225',
						'count'    => 'D225',
					),
					'5' => array(
						'post'     => 'B226',
						'category' => 'C226',
						'count'    => 'D226',
					),
				),
			),  
			
);

?>