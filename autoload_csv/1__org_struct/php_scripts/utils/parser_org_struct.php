<?php

require_once './php_scripts/utils/parser_xls2csv.php';

class ParserOrgStruct {

	private $dir_csv = './parsed_csv';

	private $xls_file;

	private $csv_file__organization;
	private $csv_file__departments;
	private $csv_file__positions;
	
	
	public function __construct( $xls_file ) {		
		$this->xls_file = $xls_file;		

		// CSV_FILE__ORGANIZATION
		$path_csv_file_organization   = $this->dir_csv . '/' . 'organization.csv';
		$this->csv_file__organization = fopen( $path_csv_file_organization, 'a' );

		// CSV_FILE__DEPARTMENTS
		$path_csv_file_departments   = $this->dir_csv . '/' . 'departments.csv';
		$this->csv_file__departments = fopen( $path_csv_file_departments, 'a' );

		// CSV_FILE__POSITIONS
		$path_csv_file_positions   = $this->dir_csv . '/' . 'positions.csv';
		$this->csv_file__positions = fopen( $path_csv_file_positions, 'a' );
	}
	
	public function __destruct() {
		fclose( $this->csv_file__organization );
		fclose( $this->csv_file__departments  );
		fclose( $this->csv_file__positions    );
	}
	
	public function parse() {

		ParserXls2Csv::initGlobalArrayXlsSheets();

		$xls2csvParer = new ParserXls2Csv(  $this->xls_file,
											$this->csv_file__organization,
											$this->csv_file__departments,
											$this->csv_file__positions );
		$xls2csvParer->parse();
	}
}


?>