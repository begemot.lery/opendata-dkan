<?php

class EvalValueCell {
	public static function get($valueCell) {
		$evalResult = $valueCell;
		if (EvalValueCell::isStartWithEquel($valueCell)) {
			eval("\$evalResult$evalResult;");
		}
		return $evalResult;
	}
	
	private static function isStartWithEquel($valueCell) {
		return ( (strpos($valueCell, '=') === false) ? false : true );
	}
}

?>