<?php

require_once './php_scripts/lib/PHPExcel.php';
require_once './php_scripts/utils/eval_value_cell.php';

$global_array_xls_sheets = array();

require_once './php_scripts/xls_sheets/array_xls_sheet_1.php';
require_once './php_scripts/xls_sheets/array_xls_sheet_2.php';
require_once './php_scripts/xls_sheets/array_xls_sheet_3.php';
require_once './php_scripts/xls_sheets/array_xls_sheet_4.php';
require_once './php_scripts/xls_sheets/array_xls_sheet_5.php';
require_once './php_scripts/xls_sheets/array_xls_sheet_6.php';
require_once './php_scripts/xls_sheets/array_xls_sheet_7.php';
require_once './php_scripts/xls_sheets/array_xls_sheet_8.php';
require_once './php_scripts/xls_sheets/array_xls_sheet_9.php';

class ParserXls2Csv {
	
	private $xls;
	private $array_xls_sheets;
	
	private $csv_file__organization;
	private $csv_file__departments;
	private $csv_file__positions;
	
	public static function initGlobalArrayXlsSheets() {
		global $global_array_xls_sheets;
		        
		global $array_xls_sheet_1;
		array_push( $global_array_xls_sheets, $array_xls_sheet_1 );

		global $array_xls_sheet_2;
		array_push( $global_array_xls_sheets, $array_xls_sheet_2 );

		global $array_xls_sheet_3;
		array_push( $global_array_xls_sheets, $array_xls_sheet_3 );

		global $array_xls_sheet_4;
		array_push( $global_array_xls_sheets, $array_xls_sheet_4 );

		global $array_xls_sheet_5;
		array_push( $global_array_xls_sheets, $array_xls_sheet_5 );

		global $array_xls_sheet_6;
		array_push( $global_array_xls_sheets, $array_xls_sheet_6 );

		global $array_xls_sheet_7;
		array_push( $global_array_xls_sheets, $array_xls_sheet_7 );

		global $array_xls_sheet_8;
		array_push( $global_array_xls_sheets, $array_xls_sheet_8 );

		global $array_xls_sheet_9;
		array_push( $global_array_xls_sheets, $array_xls_sheet_9 );
	}
	
	public function __construct( &$xls_file,
                                 &$csv_file__organization,
								 &$csv_file__departments,
								 &$csv_file__positions )
	{
		$this->xls = PHPExcel_IOFactory::load( $xls_file );
		
		$this->csv_file__organization = $csv_file__organization;
		$this->csv_file__departments  = $csv_file__departments;
		$this->csv_file__positions    = $csv_file__positions;
		
		global $global_array_xls_sheets;
		$this->array_xls_sheets = $global_array_xls_sheets;
	}
	
	public function parse() {
		$sheet_count = $this->xls->getSheetCount();

		for ( $sheet_index = 0; $sheet_index < $sheet_count; $sheet_index++ ) {			
			$this->writeXlsSheetInfoToCsvFiles( $sheet_index );
		}		
	}
	
	private function writeXlsSheetInfoToCsvFiles( $sheet_index ) {
		$this->xls->setActiveSheetIndex( $sheet_index );
		$xls_sheet = $this->xls->getActiveSheet();

		$id_org   = $sheet_index + 1;
		$name_org = $xls_sheet->getTitle();
		fputcsv( $this->csv_file__organization, [$id_org, $name_org], ',', '"' );

		foreach ( $this->array_xls_sheets[$sheet_index] as $id_dep => $array_xls_sheet ) {
	
			$name_dep = $xls_sheet->getCell( $array_xls_sheet['name'] );
			fputcsv( $this->csv_file__departments, [$id_org, $id_dep, $name_dep], ',', '"' );
		
			foreach ( $array_xls_sheet['info'] as $xls_sheet_info ) {

				$post      = $xls_sheet->getCell( $xls_sheet_info['post'] );
				
				$_category = $xls_sheet->getCell( $xls_sheet_info['category'] );
				$category  = EvalValueCell::get( $_category );
				
				$_count    = $xls_sheet->getCell( $xls_sheet_info['count'] );
				$count     = EvalValueCell::get( $_count );

				fputcsv( $this->csv_file__positions, [$id_org, $id_dep, $post, $category, $count], ',', '"' );
			}
		}
	}
}

?>