#!/bin/bash

cd /var/www/dkan/public_html/autoload_csv/1__org_struct


path_csv_dir=./parsed_csv

./bash_utils/init_csv_files.sh \
								"$path_csv_dir/organization.csv" \
								"$path_csv_dir/departments.csv" \
								"$path_csv_dir/positions.csv"


xml_file=./xls/2016.xls

php ./php_scripts/parser_org_struct_main.php $xml_file


DATASTORE_RESOURCE_ID__ORGANIZATION=6d4deb07-b7c2-45e7-8acc-91a78599c5fa
DATASTORE_RESOURCE_ID__DEPARTMENTS=5dc8ad60-cb18-4985-81e3-42173fba56ef
DATASTORE_RESOURCE_ID__POSITIONS=9ed13b1e-4968-4543-9041-b2091f3f44dd

path_csv_dir=./autoload_csv/1__org_struct/parsed_csv

/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__ORGANIZATION "$path_csv_dir/organization.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__DEPARTMENTS  "$path_csv_dir/departments.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__POSITIONS    "$path_csv_dir/positions.csv"
