#!/bin/bash

cd /var/www/dkan/public_html/autoload_csv/3__laws

./bash_utils/download_id_laws.sh
./bash_utils/init_csv_file.sh ./parsed_csv/list_laws.csv


array_id_laws=( $(cat ./id_laws/tt1001_utf-8.txt) )
length_array_id_laws=${#array_id_laws[@]}

max_connect=5000
num_mirror=-1
mod=-1

position=0
for id_law in ${array_id_laws[@]}
do
	(( mod = position % max_connect ))

	if [[ $mod == 0 ]]
	then
		(( num_mirror++ ))
	fi
	((position++))

	api_url="http://zakon$num_mirror.rada.gov.ua/laws/show/$id_law"
	echo
	echo "[$position from $length_array_id_laws]"
	echo "DOWNLOAD & PARSING: $api_url"
	result=$( php ./php_scripts/parser_list_laws_main.php $api_url $id_law )
done


DATASTORE_RESOURCE_ID___LIST_LAWS=aae05e7a-16ac-49b7-b39f-fa58924369e7

/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID___LIST_LAWS  ./autoload_csv/3__laws/parsed_csv/list_laws.csv