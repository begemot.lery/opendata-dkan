<?php

class ParseListLaws_Helper {

	private $domDocument;
	private $csv_file__list_laws;
	private $api_url;
	private $id_law;

	private $state_law;

	private $array_dom_nodes = array();
	
	public function __construct( &$domDocument,
								 &$csv_file__list_laws,
                                 &$api_url,
								 &$id_law )
	{
		$this->domDocument         = $domDocument;
		$this->csv_file__list_laws = $csv_file__list_laws;
        $this->api_url             = $api_url;
        $this->id_law              = $id_law;
	}
	
	public function parseHtmlToCsv() {
		$result = false;

		if ( $this->parseIsSuccess() ) {
			$this->appendInfoLawToCsv();
			$result = true;
		}

        return $result;  
	}
	
	private function parseIsSuccess() {
		$xpath    = new DOMXpath( $this->domDocument );
		$elements = $xpath->query( "//div[@class='lhd']" );

		if ( !is_null($elements) ) {
			foreach ( $elements as $element ) {
				$nodes = $element->childNodes;
				$i = 0;
				foreach ( $nodes as $node ) {
					$this->array_dom_nodes[$i] = $node->nodeValue;
					$i++;
				}
			}

			if ( count( $this->array_dom_nodes ) == 0 ) {
				return false;
			}

			$this->parseState();

		} else {
			return false;
		}
		return true;		
	}

	private function parseState() {
		$xpath    = new DOMXpath( $this->domDocument );
		$elements = $xpath->query("//font[@color='#0000CC']");

		if (!is_null($elements)) {
			foreach ($elements as $element) {
				$nodes = $element->childNodes;
				foreach ($nodes as $node) {
					$this->state_law = $node->nodeValue;
				}
			}
		}		
		
	}


	private function getNameLaw() {
		return trim( $this->array_dom_nodes['1'] );
	}

	private function getPublisherLaw() {
		$publisher = trim( $this->array_dom_nodes['3'] ) .
                     ' ' .
                     trim( $this->array_dom_nodes['4'] ) .
                     ' ' .
                     trim( $this->array_dom_nodes['5'] ) .
                     ' ' .
                     trim( $this->array_dom_nodes['6'] );

		return $publisher;
	}

	private function getDateLaw( $publisher ) {
		$date = '';
		if ( preg_match( "/.+(\d{2}.\d{2}.\d{4}).+/", $publisher, $matches ) == 1 ) {
			$date = $matches[1];
		}
		return $date;
	}

	private function getStateLaw() {		
		return trim( $this->state_law );
	}

	private function getIdLaw() {
		$text   = $this->array_dom_nodes['7'];
		$pos    = strpos( $text, ':' );
		$id_law = substr( $text, $pos + 1 );
		
		return trim( $id_law );
	}
	
	private function appendInfoLawToCsv() {
		$name_law      = $this->getNameLaw();
		$publisher_law = $this->getPublisherLaw();
		$date_law      = $this->getDateLaw($publisher_law) . " ";
		$state_law     = $this->getStateLaw();
		$id_law        = $this->id_law;
		$link_law_html = $this->api_url;

		$id_law_txt = str_replace( '/', '-', $this->id_law );
		$link_law_txt  = 'http://opendata.rada.gov.ua/sites/default/files/laws/' . $id_law_txt . '.txt';
		
		fputcsv( $this->csv_file__list_laws, [$name_law, $publisher_law, $date_law, $state_law, $id_law, $link_law_html, $link_law_txt] );
	}
}
?>