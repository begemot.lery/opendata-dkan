<?php

class ParseFromListLaws_Helper
{
//Разбирает html на txt по ссылкам из файла list_laws.csv

    public function parseHtmlToTxt()
    {
        $file_list_laws = fopen("./parsed_csv/list_laws.csv", "r");

        $first_row = true;
        while (($data = fgetcsv($file_list_laws, 1000, ",")) !== FALSE) {
            echo "Parsing ... $data[5] \n";
            if ($first_row == true) {
                $first_row = false;
                continue;
            } else {
                $findme = '/';
                $match_result = stripos($data[4], $findme);
                if ($match_result === false) {
                    //echo "Символ '$findme' не найдена в строке '$data[4]";
                    self::download($data[5], $data[4]);
                } else {
                    $result = explode('/', $data[4]);
                    //echo "Символ '$findme' найдена в строке '$result[1]+$result[0]";
                    $directory = $result[0];
                    $name = $result[1];
                    self::download($data[5], $name, $directory);
                }


            }
        }
        fclose($file_list_laws);

    }
    //Выкачивает по сслыке html код и сохраняет в txt
    public function download($url, $name, $directory = null)
    {
        $code = $url;
        $curl_object = curl_init();
        curl_setopt($curl_object, CURLOPT_URL, $code);
        curl_setopt($curl_object, CURLOPT_HEADER, 0);
        curl_setopt($curl_object, CURLOPT_RETURNTRANSFER, 1);
        $html = curl_exec($curl_object);
        $match_result = "";
        $pattern = '~<pre>(.*)</pre>~is';
        preg_match($pattern, $html, $match_result);
        if ($directory == null) {
         $name_res = $name;
        } else {
            $name_res = $directory."-".$name;


        }
        if(empty($match_result[1])){
            $file_error_logs = fopen("./parsed_csv/error_logs.txt", 'a') or die("can't open file");
            echo "Файл $name_res не записан,потому, что превышен лимит запросов по одному id, по адрессу $url \n";
            fwrite($file_error_logs, "Файл $name_res не записан,потому, что превышен лимит запросов по одному id, по адрессу $url \n");
            fclose($file_error_logs);
        }else{
            $file_parsed = fopen('./parsed_csv/logs.txt', 'a') or die("can't open file");
            fwrite($file_parsed, $url."\n");
            fclose($file_parsed);
            $match_result[1] = iconv("WINDOWS-1251", "UTF-8", $match_result[1]);
            $file_parsed_txt = fopen("./parsed_csv/" . $name_res . ".txt", 'w') or die("can't open file");
            fwrite($file_parsed_txt, strip_tags($match_result[1]));
            fclose($file_parsed_txt);
        }

        curl_close($curl_object);
    }


}

?>