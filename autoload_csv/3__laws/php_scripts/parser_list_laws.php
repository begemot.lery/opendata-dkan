<?php

require_once './php_scripts/utils/curl_helper.php';
require_once './php_scripts/utils/parse_list_laws_helper.php';

error_reporting(E_ALL & ~E_WARNING);

class ListLawsParser {

	private $api_url;
	private $id_law;
	private $csv_file__list_laws;

	private $domDocument;
	private $curlHelper;
		
	public function __construct( $api_url, $id_law ) {
		$this->api_url             = $api_url;
		$this->id_law              = $id_law;
		$this->csv_file__list_laws = fopen("./parsed_csv/list_laws.csv", "a");

		$this->domDocument = new DOMDocument();
		$this->curlHelper  = new CurlHelper();
	}

	public function __destruct() {
		fclose( $this->csv_file__list_laws );
	}

	public function parse() {
		$html_content = $this->curlHelper->getHtmlContent( $this->api_url );
		$this->domDocument->loadHTML( $html_content );

		$parseHelper = new ParseListLaws_Helper( $this->domDocument, $this->csv_file__list_laws, $this->api_url, $this->id_law );
		
		echo ( $parseHelper->parseHtmlToCsv() ? "1" : "0" );
	}
}

?>