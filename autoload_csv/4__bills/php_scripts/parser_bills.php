<?php

require_once './php_scripts/utils/parser_xmlbills2csv.php';

class ParserBills {	
	const DIR_CSV = './parsed_csv';
	const DIR_XML = './downloaded_xml';
	
	private $xml_obj;	
	private $convocation_number;	
	private $array_csv_files;
	
	public function __construct( $convocation_number ) {
		$this->convocation_number = $convocation_number;

		$this->array_csv_files['bills']               = $this->_fopen( 'bills'               );
		$this->array_csv_files['acts']                = $this->_fopen( 'acts'                );
		$this->array_csv_files['initiators_outer']    = $this->_fopen( 'initiators_outer'    );
		$this->array_csv_files['initiators_official'] = $this->_fopen( 'initiators_official' );
		$this->array_csv_files['authors_outer']       = $this->_fopen( 'authors_outer'       );
		$this->array_csv_files['authors_official']    = $this->_fopen( 'authors_official'    );
		$this->array_csv_files['main_executives']     = $this->_fopen( 'main_executives'     );
		$this->array_csv_files['executives']          = $this->_fopen( 'executives'          );
		$this->array_csv_files['documents']           = $this->_fopen( 'documents'           );
		$this->array_csv_files['passings']            = $this->_fopen( 'passings'            );
	}
	
	public function __destruct() {
		fclose( $this->array_csv_files['bills']               );
		fclose( $this->array_csv_files['acts']                );
		fclose( $this->array_csv_files['initiators_outer']    );
		fclose( $this->array_csv_files['initiators_official'] );
		fclose( $this->array_csv_files['authors_outer']       );
		fclose( $this->array_csv_files['authors_official']    );
		fclose( $this->array_csv_files['main_executives']     );
		fclose( $this->array_csv_files['executives']          );
		fclose( $this->array_csv_files['documents']           );
		fclose( $this->array_csv_files['passings']            );
	}
	
	public function parse() {
		$xml_file = self::DIR_XML . '/' . 'bills_' . $this->convocation_number . '.xml';

		if ( file_exists( $xml_file ) ) {
			$this->xml_obj = simplexml_load_file( $xml_file );
			
			$xmlbills2csvParser = new ParserXmlBills2Csv( $this->xml_obj, $this->array_csv_files );
			$xmlbills2csvParser->parseXmlToCsv();
		}	
	}
	
	private function _fopen( $file_name ) {
		$path_csv_file = self::DIR_CSV . '/' . $this->convocation_number . '/' . 'bills_' . $file_name . '_' . $this->convocation_number . '.csv';
		return fopen( $path_csv_file, 'a' );		
	}
}

?>