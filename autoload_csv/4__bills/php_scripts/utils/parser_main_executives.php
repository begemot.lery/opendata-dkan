<?php

class ParserMainExecutives {
	
	public static function parse( $csv_file__main_executives, $bill_attr_id, $main_executives ) {	
		if ( !empty( $main_executives ) ) {
			$array_main_executives[0] = $bill_attr_id;
			foreach ( $main_executives as $main_executive ) {
				$person = $main_executive->person;
				$array_main_executives[1] = $person->attributes()->id;
				$array_main_executives[2] = $person->surname . ' ' . $person->firstname . ' ' . $person->patronymic;
				$array_main_executives[3] = $main_executive->department;				
				
				ParserMainExecutives::writeToCsvFile( $csv_file__main_executives, $array_main_executives );
			}
		}
	}

	private static function writeToCsvFile( $csv_file__main_executives, $array_main_executives ) {
		fputcsv( $csv_file__main_executives, $array_main_executives, ',', '"' );
	}	
}

?>