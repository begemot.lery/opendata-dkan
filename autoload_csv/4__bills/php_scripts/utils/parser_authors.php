<?php

class ParserAuthors {

	public static function parse( $csv_file__authors_outer, $csv_file__authors_official, $bill_attr_id, $authors ) {
		if ( !empty( $authors ) ) {
			foreach ( $authors as $author ) {
				$outer = $author->outer;
				if ( !empty( $outer ) ) {
					ParserAuthors::parseAuthorsOuter( $csv_file__authors_outer, $bill_attr_id, $outer );
				}				
				$official = $author->official;
				if ( !empty( $official ) ) {
					ParserAuthors::parseAuthorsOfficial( $csv_file__authors_official, $bill_attr_id, $official );
				}
			}
		}
	}
	
	private static function parseAuthorsOuter( $csv_file__authors_outer, $bill_attr_id, $outer ) {
		$array_authors_outer[0] = $bill_attr_id;
		$array_authors_outer[1] = $outer->person->surname . ' ' . $outer->person->firstname . ' ' . $outer->person->patronymic;
		$array_authors_outer[2] = $outer->organization;
		$array_authors_outer[3] = $outer->post;
		
		ParserAuthors::writeToCsvFile( $csv_file__authors_outer, $array_authors_outer );
	}
	
	private static function parseAuthorsOfficial( $csv_file__authors_official, $bill_attr_id, $official ) {
		$array_authors_official[0] = $bill_attr_id;
		$array_authors_official[1] = $official->person->surname . ' ' . $official->person->firstname . ' ' . $official->person->patronymic;
		$array_authors_official[2] = $official->convocation;
		$array_authors_official[3] = $official->department;
		$array_authors_official[4] = $official->post;
	
		ParserAuthors::writeToCsvFile( $csv_file__authors_official, $array_authors_official );
	}

	private static function writeToCsvFile( $csv_file, $array ) {
		fputcsv( $csv_file, $array, ',', '"' );
	}
}

?>