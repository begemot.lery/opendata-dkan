<?php

class ParserInitiators {
	
	public static function parse( $csv_file__initiators_outer, $csv_file__initiators_official, $bill_attr_id, $initiators ) {
		
		if ( !empty( $initiators ) ) {

			foreach ( $initiators as $initiator ) {				
				$outer = $initiator->outer;				
				if ( !empty( $outer ) ) {
					ParserInitiators::parseInitiatorsOuter( $csv_file__initiators_outer, $bill_attr_id, $outer );
				}
				
				$official = $initiator->official;
				if ( !empty( $official ) ) {
					ParserInitiators::parseInitiatorsOfficial( $csv_file__initiators_official, $bill_attr_id, $official );
				}
			}
		}
	}

	private static function parseInitiatorsOuter( $csv_file__initiators_outer, $bill_attr_id, $outer ) {
		$array_initiators_outer[0] = $bill_attr_id;		
		$array_initiators_outer[1] = $outer->person->surname . ' ' . $outer->person->firstname . ' ' . $outer->person->patronymic;
		$array_initiators_outer[2] = $outer->organization;
		$array_initiators_outer[3] = $outer->post;

		ParserInitiators::writeToCsvFile( $csv_file__initiators_outer, $array_initiators_outer );
	}

	private static function parseInitiatorsOfficial( $csv_file__initiators_official, $bill_attr_id, $official ) {
		$array_initiators_official[0] = $bill_attr_id;
		$array_initiators_official[1] = $official->person->surname . ' ' . $official->person->firstname . ' ' . $official->person->patronymic;
		$array_initiators_official[2] = $official->convocation;
		$array_initiators_official[3] = $official->department;
		$array_initiators_official[4] = $official->post;

		ParserInitiators::writeToCsvFile( $csv_file__initiators_official, $array_initiators_official );	
	}
	
	private static function writeToCsvFile( $csv_file, $array ) {
		fputcsv( $csv_file, $array, ',', '"' );
	}
	
}

?>