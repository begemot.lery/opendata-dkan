<?php

require_once './php_scripts/utils/parser_acts.php';
require_once './php_scripts/utils/parser_initiators.php';
require_once './php_scripts/utils/parser_authors.php';
require_once './php_scripts/utils/parser_main_executives.php';
require_once './php_scripts/utils/parser_executives.php';
require_once './php_scripts/utils/parser_documents.php';
require_once './php_scripts/utils/parser_passings.php';

class ParserXmlBills2Csv {
	
	private $xml_obj;
	private $array_csv_files;
	
	public function __construct( &$xml_obj, &$array_csv_files ) {
		$this->xml_obj         = $xml_obj;
		$this->array_csv_files = $array_csv_files;
	}
	
	public function parseXmlToCsv() {
		foreach ( $this->xml_obj->bill as $bill ) {
			
			$bill_attr_id = $bill->attributes()->id;
			
			$array_bills[0]  = $bill_attr_id;
			$array_bills[1]  = $bill->type;
			$array_bills[2]  = $bill->title;
			$array_bills[3]  = $bill->uri;
			$array_bills[4]  = $bill->number;
			$array_bills[5]  = $bill->registrationDate;
			$array_bills[6]  = $bill->registrationSession;
			$array_bills[7]  = $bill->registrationConvocation;
			$array_bills[8]  = $bill->redaction;
			$array_bills[9]  = $bill->rubric;
			$array_bills[10] = $bill->subject;
			$array_bills[11] = $bill->currentPhase->date;
			$array_bills[12] = $bill->currentPhase->title;
			$array_bills[13] = $this->getBind( $bill );
			$array_bills[14] = $this->getAlternative( $bill );
			fputcsv ( $this->array_csv_files['bills'], $array_bills, ',', '"' );

			
			$acts = $bill->acts->act;
			ParserActs::parse( $this->array_csv_files['acts'],
							   $bill_attr_id, 
							   $acts );

			$initiators = $bill->initiators->initiator;
			ParserInitiators::parse( $this->array_csv_files['initiators_outer'],
									 $this->array_csv_files['initiators_official'], 
									 $bill_attr_id,
									 $initiators );

			$authors = $bill->authors->author;
			ParserAuthors::parse( $this->array_csv_files['authors_outer'],
								  $this->array_csv_files['authors_official'], 
								  $bill_attr_id,
								  $authors );
			
			$main_executives = $bill->mainExecutives->executive;
			ParserMainExecutives::parse( $this->array_csv_files['main_executives'],
										 $bill_attr_id,
										 $main_executives );
								  
			$executives = $bill->executives->executive;
			ParserExecutives::parse( $this->array_csv_files['executives'],
									 $bill_attr_id,
									 $executives );

			$documents = $bill->documents;
			ParserDocuments::parse( $this->array_csv_files['documents'],
									$bill_attr_id,
									$documents );
			
			$passings = $bill->passings->passing;
			ParserPassings::parse( $this->array_csv_files['passings'],
								   $bill_attr_id,
								   $passings );
		}
		
	}
	
	private function getBind( $bill ) {
		$bind = '';
		$refBills = $bill->bind->refBills->refBill;
		if ( !empty( $refBills ) ) {
			foreach ( $refBills as $refBill ) {
				$id    = $refBill->attributes()->id;
				$bind .= $id . ' ';
			}
		}
		
		return $bind;
	}
	
	private function getAlternative( $bill ) {
		$alternative = '';
		$refBills = $bill->alternative->refBills->refBill;
		if ( !empty( $refBills ) ) {
			foreach ( $refBills as $refBill ) {
				$id           = $refBill->attributes()->id;
				$alternative .= $id . ' ';
			}
		}
		
		return $alternative;
	}
}

?>