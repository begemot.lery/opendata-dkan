<?php

class ParserPassings {

	public static function parse( $csv_file__passings, $bill_attr_id, $passings ) {
		if ( !empty( $passings ) ) {
			$array_passings[0] = $bill_attr_id;
			foreach ( $passings as $passing ) {
				$array_passings[1] = $passing->date;
				$array_passings[2] = $passing->title;
				
				ParserPassings::writeToCsvFile( $csv_file__passings, $array_passings );
			}
		}
	}

	private static function writeToCsvFile( $csv_file__passings, $array_passings ) {
		fputcsv( $csv_file__passings, $array_passings, ',', '"' );
	}
}

?>