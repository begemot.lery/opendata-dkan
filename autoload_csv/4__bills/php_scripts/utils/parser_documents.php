<?php

class ParserDocuments {
	
	public static function parse( $csv_file__documents, $bill_attr_id, $documents ) {	
		if ( !empty( $documents ) ) {
			$source = $documents->source;
			if (!empty( $source )) {
				$array_documents_source[0] = $bill_attr_id;
				foreach ( $source->document as $document ) {
					$array_documents_source[1] = 'source'; 
					$array_documents_source[2] = $document->type;
					$array_documents_source[3] = $document->date;
					$array_documents_source[4] = $document->uri;
					
					ParserDocuments::writeToCsvFile( $csv_file__documents, $array_documents_source );
				}
			}
			$workflow = $documents->workflow;
			if (!empty( $workflow )) {
				$array_documents_workflow[0] = $bill_attr_id;
				foreach ( $workflow->document as $document ) {
					$array_documents_workflow[1] = 'workflow';
					$array_documents_workflow[2] = $document->type;
					$array_documents_workflow[3] = $document->date;
					$array_documents_workflow[4] = $document->uri;

					ParserDocuments::writeToCsvFile( $csv_file__documents, $array_documents_workflow );
				}
			}
		}
	}
	
	private static function writeToCsvFile( $csv_file__documents, $array_documents ) {
		fputcsv( $csv_file__documents, $array_documents, ',', '"' );
	}	
	
}

?>