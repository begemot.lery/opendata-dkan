<?php

class ParserActs {
	
	public static function parse( $csv_file__acts, $bill_attr_id, $acts ) {
		if ( !empty( $acts ) ) {
			$array_acts[0] = $bill_attr_id;			
			foreach ( $acts as $act ) {
				$array_acts[1] = $act->number;
				$array_acts[2] = $act->date;
				$array_acts[3] = $act->uri;
				
				ParserActs::writeToCsvFile( $csv_file__acts, $array_acts );
			}
		}
	}
	
	private static function writeToCsvFile( $csv_file__acts, $array_acts ) {
		fputcsv( $csv_file__acts, $array_acts, ',', '"' );
	}
}

?>