<?php

class ParserExecutives {
	
	public static function parse( $csv_file__executives, $bill_attr_id, $executives ) {	
		if ( !empty( $executives ) ) {
			$array_executives[0] = $bill_attr_id;
			foreach ( $executives as $executive ) {
				$person = $executive->person;
				$array_executives[1] = $person->attributes()->id;			
				$array_executives[2] = $person->surname . ' ' . $person->firstname . ' ' . $person->patronymic;
				$array_executives[3] = $executive->department;				
				
				ParserExecutives::writeToCsvFile( $csv_file__executives, $array_executives );
			}
		}
	}

	private static function writeToCsvFile( $csv_file__executives, $array_executives ) {
		fputcsv( $csv_file__executives, $array_executives, ',', '"' );
	}	
}

?>