#!/bin/bash

cd /var/www/dkan/public_html/autoload_csv/4__bills

CONVOCATION_NUMBER_5=5

DATASTORE_RESOURCE_ID__BILLS_5=2546fa42-f2cd-4301-b385-bb1ad7aa5c71
DATASTORE_RESOURCE_ID__ACTS_5=e185fca0-2df2-4293-8d59-92f899af33f1
DATASTORE_RESOURCE_ID__INITIATORS_OUTER_5=c0674062-d5ce-468d-8389-060b33a1583e
DATASTORE_RESOURCE_ID__INITIATORS_OFFICIAL_5=e56bbe97-c2ba-4dd9-a8ca-882faba46598
DATASTORE_RESOURCE_ID__AUTHORS_OUTER_5=1b1ea70e-345c-46d1-818b-f932a71b49a2
DATASTORE_RESOURCE_ID__AUTHORS_OFFICIAL_5=1747e621-0c39-4ae5-8da6-47d9331a818b
DATASTORE_RESOURCE_ID__MAIN_EXECUTIVES_5=2b74e7d3-1f2b-498c-913a-5d8a1da77c78
DATASTORE_RESOURCE_ID__EXECUTIVES_5=9e97c91e-1a16-4126-b490-fea1085906ed
DATASTORE_RESOURCE_ID__DOCUMENTS_5=8dbe0863-9cbd-4974-9014-591106590f19
DATASTORE_RESOURCE_ID__PASSINGS_5=c3d06c98-4ad6-41b1-beb2-3de202c0ccc8

./bash_utils/update_bills_main.sh \
					$CONVOCATION_NUMBER_5 \
					$DATASTORE_RESOURCE_ID__BILLS_5 \
					$DATASTORE_RESOURCE_ID__ACTS_5 \
					$DATASTORE_RESOURCE_ID__INITIATORS_OUTER_5 \
					$DATASTORE_RESOURCE_ID__INITIATORS_OFFICIAL_5 \
					$DATASTORE_RESOURCE_ID__AUTHORS_OUTER_5 \
					$DATASTORE_RESOURCE_ID__AUTHORS_OFFICIAL_5 \
					$DATASTORE_RESOURCE_ID__MAIN_EXECUTIVES_5 \
					$DATASTORE_RESOURCE_ID__EXECUTIVES_5 \
					$DATASTORE_RESOURCE_ID__DOCUMENTS_5 \
					$DATASTORE_RESOURCE_ID__PASSINGS_5