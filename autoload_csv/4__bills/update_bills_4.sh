#!/bin/bash

cd /var/www/dkan/public_html/autoload_csv/4__bills

CONVOCATION_NUMBER_4=4

DATASTORE_RESOURCE_ID__BILLS_4=0c87e0cc-03e3-4331-9759-f664fb04e3f1
DATASTORE_RESOURCE_ID__ACTS_4=e6fc5aff-b80e-4a92-968f-a6988f8557cd
DATASTORE_RESOURCE_ID__INITIATORS_OUTER_4=09730551-4e98-455d-9856-0d366a9b5bb7
DATASTORE_RESOURCE_ID__INITIATORS_OFFICIAL_4=c2f05a7a-97f1-4eeb-9b76-6dd1f1407960
DATASTORE_RESOURCE_ID__AUTHORS_OUTER_4=7f65f65e-cc30-424b-ab90-29c1f031b51c
DATASTORE_RESOURCE_ID__AUTHORS_OFFICIAL_4=3381d1bc-2105-4102-8dd8-f2889246dad8
DATASTORE_RESOURCE_ID__MAIN_EXECUTIVES_4=2d995fb0-ca27-46fd-822b-eda2875979c7
DATASTORE_RESOURCE_ID__EXECUTIVES_4=590161b0-bd2b-4f7a-9c90-bf2f5a075a58
DATASTORE_RESOURCE_ID__DOCUMENTS_4=113e323a-3944-4aa0-b028-3aa13fc44f0c
DATASTORE_RESOURCE_ID__PASSINGS_4=3bf78601-6ac4-4133-a67b-02b9b94a9747

./bash_utils/update_bills_main.sh \
					$CONVOCATION_NUMBER_4 \
					$DATASTORE_RESOURCE_ID__BILLS_4 \
					$DATASTORE_RESOURCE_ID__ACTS_4 \
					$DATASTORE_RESOURCE_ID__INITIATORS_OUTER_4 \
					$DATASTORE_RESOURCE_ID__INITIATORS_OFFICIAL_4 \
					$DATASTORE_RESOURCE_ID__AUTHORS_OUTER_4 \
					$DATASTORE_RESOURCE_ID__AUTHORS_OFFICIAL_4 \
					$DATASTORE_RESOURCE_ID__MAIN_EXECUTIVES_4 \
					$DATASTORE_RESOURCE_ID__EXECUTIVES_4 \
					$DATASTORE_RESOURCE_ID__DOCUMENTS_4 \
					$DATASTORE_RESOURCE_ID__PASSINGS_4