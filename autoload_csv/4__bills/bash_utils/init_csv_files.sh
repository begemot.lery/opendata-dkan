#!/bin/bash

PARAMETERS=( $@ )

CSV_FILE__BILLS=${PARAMETERS[0]}
CSV_FILE__ACTS=${PARAMETERS[1]}
CSV_FILE__INITIATORS_OUTER=${PARAMETERS[2]}
CSV_FILE__INITIATORS_OFFICIAL=${PARAMETERS[3]}
CSV_FILE__AUTHORS_OUTER=${PARAMETERS[4]}
CSV_FILE__AUTHORS_OFFICIAL=${PARAMETERS[5]}
CSV_FILE__MAIN_EXECUTIVES=${PARAMETERS[6]}
CSV_FILE__EXECUTIVES=${PARAMETERS[7]}
CSV_FILE__DOCUMENTS=${PARAMETERS[8]}
CSV_FILE__PASSINGS=${PARAMETERS[9]}


echo "bill_id, type, _title_, uri, number, registrationDate, registrationSession, registrationConvocation, redaction, rubric, subject, currentPhase_date, currentPhase_title, bind, alternative" > $CSV_FILE__BILLS

echo "bill_id, number, date, uri" > $CSV_FILE__ACTS

echo "bill_id, person, organization, post" > $CSV_FILE__INITIATORS_OUTER

echo "bill_id, person, convocation, department, post" > $CSV_FILE__INITIATORS_OFFICIAL

echo "bill_id, person, organization, post" > $CSV_FILE__AUTHORS_OUTER

echo "bill_id, person, convocation, department, post" > $CSV_FILE__AUTHORS_OFFICIAL

echo "bill_id, person_id, person, department" > $CSV_FILE__MAIN_EXECUTIVES

echo "bill_id, person_id, person, department" > $CSV_FILE__EXECUTIVES

echo "bill_id, document, type, date, uri" > $CSV_FILE__DOCUMENTS

echo "bill_id, date, _title_" > $CSV_FILE__PASSINGS

