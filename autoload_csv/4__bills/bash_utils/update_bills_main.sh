#!/bin/bash

PARAMETERS=( $@ )

CONVOCATION_NUMBER=${PARAMETERS[0]}

DATASTORE_RESOURCE_ID__BILLS=${PARAMETERS[1]}
DATASTORE_RESOURCE_ID__ACTS=${PARAMETERS[2]}
DATASTORE_RESOURCE_ID__INITIATORS_OUTER=${PARAMETERS[3]}
DATASTORE_RESOURCE_ID__INITIATORS_OFFICIAL=${PARAMETERS[4]}
DATASTORE_RESOURCE_ID__AUTHORS_OUTER=${PARAMETERS[5]}
DATASTORE_RESOURCE_ID__AUTHORS_OFFICIAL=${PARAMETERS[6]}
DATASTORE_RESOURCE_ID__MAIN_EXECUTIVES=${PARAMETERS[7]}
DATASTORE_RESOURCE_ID__EXECUTIVES=${PARAMETERS[8]}
DATASTORE_RESOURCE_ID__DOCUMENTS=${PARAMETERS[9]}
DATASTORE_RESOURCE_ID__PASSINGS=${PARAMETERS[10]}


path_parsed_csv="./parsed_csv/$CONVOCATION_NUMBER"

./bash_utils/init_csv_files.sh \
								"$path_parsed_csv/bills_bills_$CONVOCATION_NUMBER.csv" \
								"$path_parsed_csv/bills_acts_$CONVOCATION_NUMBER.csv" \
								"$path_parsed_csv/bills_initiators_outer_$CONVOCATION_NUMBER.csv" \
								"$path_parsed_csv/bills_initiators_official_$CONVOCATION_NUMBER.csv" \
								"$path_parsed_csv/bills_authors_outer_$CONVOCATION_NUMBER.csv" \
								"$path_parsed_csv/bills_authors_official_$CONVOCATION_NUMBER.csv" \
								"$path_parsed_csv/bills_main_executives_$CONVOCATION_NUMBER.csv" \
								"$path_parsed_csv/bills_executives_$CONVOCATION_NUMBER.csv" \
								"$path_parsed_csv/bills_documents_$CONVOCATION_NUMBER.csv" \
								"$path_parsed_csv/bills_passings_$CONVOCATION_NUMBER.csv"


curl "http://itdl.rada.gov.ua/apps_ad/opendata/Temp_files/bills_convocation_$CONVOCATION_NUMBER.xml" -o "./downloaded_xml/bills_$CONVOCATION_NUMBER.xml"

cp "./downloaded_xml/bills_$CONVOCATION_NUMBER.xml" "../../sites/default/files/links/bills/bills_$CONVOCATION_NUMBER.xml"


php ./php_scripts/parser_bills_main.php $CONVOCATION_NUMBER


path_parsed_csv="./autoload_csv/4__bills/parsed_csv/$CONVOCATION_NUMBER"

/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__BILLS               "$path_parsed_csv/bills_bills_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__ACTS                "$path_parsed_csv/bills_acts_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__INITIATORS_OUTER    "$path_parsed_csv/bills_initiators_outer_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__INITIATORS_OFFICIAL "$path_parsed_csv/bills_initiators_official_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__AUTHORS_OUTER       "$path_parsed_csv/bills_authors_outer_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__AUTHORS_OFFICIAL    "$path_parsed_csv/bills_authors_official_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__MAIN_EXECUTIVES     "$path_parsed_csv/bills_main_executives_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__EXECUTIVES          "$path_parsed_csv/bills_executives_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__DOCUMENTS           "$path_parsed_csv/bills_documents_$CONVOCATION_NUMBER.csv"
/usr/local/bin/drush dsu $DATASTORE_RESOURCE_ID__PASSINGS            "$path_parsed_csv/bills_passings_$CONVOCATION_NUMBER.csv"

